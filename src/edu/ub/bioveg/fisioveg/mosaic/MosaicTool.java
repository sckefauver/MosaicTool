package edu.ub.bioveg.fisioveg.mosaic;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import javax.swing.UIManager;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.UITool;
import edu.ub.bioveg.fisioveg.mosaic.ui.MosaicFrame;
import ij.IJ;
import ij.plugin.PlugIn;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 28, 2016
 *
 */
public class MosaicTool implements PlugIn {
        
        public static final String VERSION = "1.11-beta";
        private MosaicFrame frame = null;
        
        @Override
        public void run(String arg) {
                if (IJ.versionLessThan("1.51g")) {
                        IJ.showMessage("This plugin needs to run on ImageJ v1.51g and above");
                }
                else {
                        try {
                                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                        }
                        catch (Exception e) {
                                // Will use the standard Look&Feel
                        }
                        
                        frame = new MosaicFrame();
                        UITool.center(frame);
                        frame.setVisible(true);
                }
        }
        
        //For stand alone testing
        public static void main(String ...args) {
                new MosaicTool().run("");
        }
}
