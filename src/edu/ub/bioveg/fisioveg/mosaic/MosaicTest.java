package edu.ub.bioveg.fisioveg.mosaic;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.process.ImageProcessor;

public class MosaicTest {

        public MosaicTest() {
                System.out.println("Loading Image");
                ImagePlus imp = IJ.openImage("C:\\Users\\George\\Documents\\Scientific Research\\Maize Research\\Mosaic Tool\\Mosaic Images\\GX7\\AranjuezGX7_June8_25m_stitch.tiff");
                System.out.println("Loaded Image");
                IJ.run(imp, "Set Scale...", "distance=180 known=7 pixel=1 unit=m");
                
                int[] xpoints = {3334,3628,4834,4612};
                int[] ypoints = {258,15557,15617,294};
                Roi roi = new PolygonRoi(xpoints,ypoints,4,Roi.POLYGON);
                System.out.println("Created ROI");
                
                imp.setOverlay(roi, Color.YELLOW, 1, null);
                imp.getChannelProcessor().setMinAndMax(255, 255);
                ImagePlus flattened = imp.flatten();
                imp.close();
                imp = null;
                flattened.show();
                
                followYellowBrickRoad(flattened);
        }
        
        private void followYellowBrickRoad(ImagePlus imp) {
                IJ.run(imp, "Set Scale...", "distance=180 known=7 pixel=1 unit=m");
                ImageProcessor ip = imp.getChannelProcessor();
                int x = 3334;
                int y = 258;
                
                //Start at top right corner x,y
                int count = 0;
                
                while(true) {
                        if(!isBlack(ip.get(x, y))) {
                                y++;
                                count++;
                                //move 1 pixel down
                                
                                
                                if(count == 260) {
                                        //We traveled 260 pixels downwards
                                        System.out.println("makePoint("+x+","+y+");");
                                        System.out.println("roiManager(\"Add\");");
                                        
//                                        RoiManager roiManager = RoiManager.getInstance();
//                                        roiManager.addRoi(roi);
//                                        
//                                        Overlay over = new Overlay();
//                                        over.add(roi);
                                        
                                        count = 0;
                                }
                                
                                if(y >= 15557) {
                                        //We reach the bottom of the polygon
                                        System.out.println("We done following the yellow brick road");
                                        break;
                                }
                        }
                        else {
                                if(!isBlack(ip.get(x+1, y))) {
                                        //Check right
                                        x++;
                                        //go right
                                }
                                else if(!isBlack(ip.get(x-1, y))) {
                                        //Check left
                                        x--;
                                        //go left
                                }
                                else {
                                        System.out.println("Can't go left or right :(");
                                }
                        }
                }
        }
        
        private static final int BLACK = 0;
        private boolean isBlack(int pixel) {
                return BLACK == pixel;
        }
        
//        private static final int RED = 16;
//        private static final int GREEN = 8;
//        private static final int BLUE = 0;
//
//        public int[] getRGB(int pixel) {   
//            int[] color = new int[3];
//            color[0] = pixel >> RED & 0xff;
//            color[1] =pixel >> GREEN & 0xff;
//            color[2] =pixel >> BLUE & 0xff;
//            return color;
//        }
        
//        private void printRGB(int[] rgb) {
//                System.out.println("rgb("+rgb[0]+","+rgb[1]+","+rgb[2]+")");
//        }
        
        public static void main(String ... args) {
                new MosaicTest();
        }
}
