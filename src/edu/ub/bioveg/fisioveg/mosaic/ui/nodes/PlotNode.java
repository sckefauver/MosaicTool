package edu.ub.bioveg.fisioveg.mosaic.ui.nodes;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.Serializable;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 4, 2017
 * 
 */
public class PlotNode extends AbstractNode implements Comparable<PlotNode>, Serializable {

        private static final long serialVersionUID = 5498323540136640839L;
        
        private int sequence = 0;
        
        public PlotNode() {
                
        }
        
        public final int getSequence() {
                return sequence;
        }
        
        public final void setSequence(int sequance) {
                this.sequence = sequance;
        }

        @Override
        public int compareTo(PlotNode o) {
                return id.compareTo(o.getId());
        }
        
        @Override
        public String toString() {
                return id;
        }
}
