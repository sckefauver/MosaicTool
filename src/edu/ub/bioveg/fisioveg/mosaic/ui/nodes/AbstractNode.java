package edu.ub.bioveg.fisioveg.mosaic.ui.nodes;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import ij.gui.PolygonRoi;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 4, 2017
 * 
 */
public abstract class AbstractNode {

        public static final NumberFormat ANGLE_DECIMAL_FORMAT = new DecimalFormat("##0.00", DecimalFormatSymbols.getInstance(Locale.US));
        
        protected String id = null;
        protected String name = null;
        protected PolygonRoi polyRoi = null;
        protected double rotateAngle = 0.0d;
        
        public AbstractNode() {
                
        }
        
        public final String getId() {
                return id;
        }
        
        public final void setId(String id) {
                this.id = id;
        }
        
        public final String getName() {
                return name;
        }
        
        public final void setName(String name) {
                this.name = name;
        }
        
        public final PolygonRoi getPolyRoi() {
                return polyRoi;
        }
        
        public final void setPolyRoi(PolygonRoi polyRoi) {
                this.polyRoi = polyRoi;
        }
        
        public final double getRotateAngle() {
                return rotateAngle;
        }
        
        public final void setRotateAngle(double rotateAngle) {
                this.rotateAngle = rotateAngle;
        }
}
