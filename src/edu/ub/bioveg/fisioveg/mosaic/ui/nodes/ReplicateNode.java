package edu.ub.bioveg.fisioveg.mosaic.ui.nodes;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import edu.ub.bioveg.fisioveg.mosaic.tools.Corner;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 4, 2017
 * 
 */
public class ReplicateNode extends AbstractNode implements Comparable<ReplicateNode>, Serializable {

        private static final long serialVersionUID = -3672525746143790225L;
        
        private Corner upperLeftCorner = null;
        private Corner lowerLeftCorner = null;
        private Corner lowerRightCorner = null;
        private Corner upperRightCorner = null;
        
        private int sequence = 0;
        
        public ReplicateNode() {
                
        }
        
        public final Corner getUpperLeftCorner() {
                return upperLeftCorner;
        }
        
        public final void setUpperLeftCorner(Corner upperLeftCorner) {
                this.upperLeftCorner = upperLeftCorner;
        }
        
        public final Corner getLowerLeftCorner() {
                return lowerLeftCorner;
        }
        
        public final void setLowerLeftCorner(Corner lowerLeftCorner) {
                this.lowerLeftCorner = lowerLeftCorner;
        }
        
        public final Corner getLowerRightCorner() {
                return lowerRightCorner;
        }
        
        public final void setLowerRightCorner(Corner lowerRightCorner) {
                this.lowerRightCorner = lowerRightCorner;
        }
        
        public final Corner getUpperRightCorner() {
                return upperRightCorner;
        }
        
        public final void setUpperRightCorner(Corner upperRightCorner) {
                this.upperRightCorner = upperRightCorner;
        }
        
        public final int getSequence() {
                return sequence;
        }
        
        public final void setSequence(int sequance) {
                this.sequence = sequance;
        }
        
        @Override
        public int compareTo(ReplicateNode o) {
                return id.compareTo(o.getId());
        }
        
        @Override
        public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append(name).append(" (").append(ANGLE_DECIMAL_FORMAT.format(rotateAngle)).append('\u00B0').append(')');
                return sb.toString();
        }
}
