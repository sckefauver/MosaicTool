package edu.ub.bioveg.fisioveg.mosaic.ui;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import edu.ub.bioveg.fisioveg.mosaic.options.PlotOptions;
import edu.ub.bioveg.fisioveg.mosaic.tools.Corner;
import edu.ub.bioveg.fisioveg.mosaic.tools.PixelChaser;
import edu.ub.bioveg.fisioveg.mosaic.tools.Plot;
import edu.ub.bioveg.fisioveg.mosaic.tools.PolygonTool;
import edu.ub.bioveg.fisioveg.mosaic.tools.breedpix.BreedPixResult;
import edu.ub.bioveg.fisioveg.mosaic.tools.breedpix.BreedPixTool;
import edu.ub.bioveg.fisioveg.mosaic.tools.breedpix.PicVIOperation;
import edu.ub.bioveg.fisioveg.mosaic.tools.breedpix.PixelMask;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileOpen;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileSave;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.UITool;
import edu.ub.bioveg.fisioveg.mosaic.ui.nodes.PlotNode;
import edu.ub.bioveg.fisioveg.mosaic.ui.nodes.ReplicateNode;
import edu.ub.bioveg.fisioveg.mosaic.ui.panels.BreedPixOptionsPanel;
import edu.ub.bioveg.fisioveg.mosaic.ui.panels.CloneDirection;
import edu.ub.bioveg.fisioveg.mosaic.ui.panels.NamingSchemePanel;
import edu.ub.bioveg.fisioveg.mosaic.ui.panels.NamingStyle;
import edu.ub.bioveg.fisioveg.mosaic.ui.panels.ReplicateClonePanel;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.measure.Calibration;
import ij.plugin.Duplicator;
import ij.plugin.frame.RoiManager;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import ij.util.Tools;
import layout.TableLayout;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 1, 2016
 * 
 */
public class MosaicPanel extends JPanel implements Closeable {

        private static final long serialVersionUID = 1975632987668116827L;
        
        private static final Pattern XY_REGEX = Pattern.compile("^(\\-?\\d+(\\.\\d+)?),(\\-?\\d+(\\.\\d+)?)$");
        private static final Dimension LABEL_PREF_SIZE = new Dimension(61,16);
        
        private JLabel trialLabel = null;
        private JTextField trialField = null;
        private JLabel mosaicImageLabel = null;
        private JTextField mosaicField = null;
        private JButton mosaicButton = null;
        
        private JLabel trialScaleLabel = null;
        private JTextField trialScaleField = null;
        
        private JLabel repAreaLabel = null;
        private JTextField upperLeftCornerField = null;
        private JTextField lowerLeftCornerField = null;
        private JTextField lowerRightCornerField = null;
        private JTextField upperRightCornerField = null;
        private JTextField replicateAreaField = null;
        private JButton repAreaButton = null;
        private JButton deleteReplicateButton = null;
        private JButton cloneReplicateButton = null;
        
        private JLabel plotHeightLabel = null;
        private JTextField plotHeightField = null;
        private JLabel plotSpacingLabel = null;
        private JTextField plotSpacingField = null;
        private JLabel plotBufferXLabel = null;
        private JTextField plotBufferXField = null;
        private JLabel plotBufferYLabel = null;
        private JTextField plotBufferYField = null;
        private JButton segmentPlotsButton = null;
        private JButton deletePlotsButton = null;
        private JButton namingPlotsButton = null;
        
        private JLabel statusLabel = null;
        private JButton statusButton = null;
        private boolean isEdit = false;
        
        private String recentDir = null;
        private File mosaicFile = null;
        private ImagePlus mosaicImage = null;
        
        private JTree trialTree = null;
        private DefaultMutableTreeNode rootNode = null;
        private Map<String, DefaultMutableTreeNode> nodeMap = null;
        
        private int replicateId = 0;
        
        public MosaicPanel() {
                super();
                JPanel trialPanel = createTrialPanel();
                JPanel replicatePanel = createReplicatePanel();
                JPanel plotPanel = createPlotPanel();
                JPanel treePanel = createTreePanel();
                JPanel statusPanel = createStatusPanel();
                JPanel repButtonPanel = createReplicateButtonPanel();
                JPanel plotButtonPanel = createPlotButtonPanel();
                JPanel mainButtonPanel = createMainButtonPanel();
                
                trialPanel.setBorder(BorderFactory.createTitledBorder("Trial"));
                replicatePanel.setBorder(BorderFactory.createTitledBorder("Replicates"));
                plotPanel.setBorder(BorderFactory.createTitledBorder("Plots"));
                
                setStatus(StatusMode.VIEW);
                
                double[][] mainLayout = {
                                {
                                        TableLayout.PREFERRED, 10, TableLayout.PREFERRED
                                },
                                
                                {
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED,
                                        15,
                                        TableLayout.PREFERRED,
                                        15,
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.FILL
                                }
                };
                
                setLayout(new TableLayout(mainLayout));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(trialPanel,     "0, 0");
                add(replicatePanel, "0, 2");
                add(plotPanel,      "0, 4");
                add(statusPanel,    "0, 6");
                add(treePanel,      "0, 8");
                add(repButtonPanel, "2, 2");
                add(plotButtonPanel,"2, 4");
                add(mainButtonPanel,"2, 8");
                
                nodeMap = new TreeMap<String, DefaultMutableTreeNode>();
        }
        
        private final JPanel createReplicateButtonPanel() {
                deleteReplicateButton = new JButton("Delete");
                deleteReplicateButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                deleteReplicateNodes_actionPerformed();
                        }
                });
                
                cloneReplicateButton = new JButton("Clone");
                cloneReplicateButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                cloneReplicate_actionPerformed();
                        }
                });
                
                
                JPanel buttonPanel = new JPanel(new GridLayout(2, 1, 5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(cloneReplicateButton);
                buttonPanel.add(deleteReplicateButton);
                return buttonPanel;
        }
        
        private final JPanel createPlotButtonPanel() {
                segmentPlotsButton = new JButton("Segment");
                segmentPlotsButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                segmentPlots_actionPerformed();
                        }
                });
                
                deletePlotsButton = new JButton("Delete");
                deletePlotsButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                deletePlotNodes_actionPerformed();
                        }
                });
                
                namingPlotsButton = new JButton("Naming");
                namingPlotsButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                namingPlots_actionPerformed();
                        }
                });
                
                JPanel buttonPanel = new JPanel(new GridLayout(3, 1, 5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(segmentPlotsButton);
                buttonPanel.add(namingPlotsButton);
                buttonPanel.add(deletePlotsButton);
                return buttonPanel;
        }
        
        private final JPanel createMainButtonPanel() {
                JButton exportButton = new JButton("Export");
                exportButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                Thread t = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                                exportButton_actionPerformed();
                                        }
                                });
                                t.start();
                        }
                });
                
                JButton breedPixButton = new JButton("BreedPix");
                breedPixButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                Thread t = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                                breedPixButton_actionPerformed();
                                        }
                                });
                                t.start();
                        }
                });
                
                JPanel buttonPanel = new JPanel(new GridLayout(4, 1, 5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(exportButton);
                buttonPanel.add(breedPixButton);
                return buttonPanel;
        }
        
        private final void namingPlots_actionPerformed() {
                NamingSchemePanel panel = new NamingSchemePanel();
                int choice = JOptionPane.showConfirmDialog(null, panel, "Specify Naming Scheme", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if(JOptionPane.OK_OPTION == choice) {
                        performNamingScheme(panel);
                        DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                        treeModel.reload(rootNode);
                        trialTree.validate();
                        trialTree.repaint();
                }
        }
        
        private final void exportButton_actionPerformed() {
                File trialFile = FileSave.saveFile("Export Trial Plots", new File(recentDir+File.separator), "Location of saved plots for entire trial", trialField.getText());
                if(trialFile != null) {
                        if(!trialFile.exists()) {
                                trialFile.mkdirs();
                        }
                        
                        reloadMosaicImage();
                        
                        // Go through all plots
                        // get polyroi and crop
                        
                        @SuppressWarnings("unchecked")
                        Enumeration<DefaultMutableTreeNode> enu = rootNode.preorderEnumeration();
                        while(enu.hasMoreElements()) {
                                DefaultMutableTreeNode node = enu.nextElement();
                                Object obj = node.getUserObject();
                                if(obj instanceof PlotNode) {
                                        PlotNode plotNode = (PlotNode)obj;
                                        ImagePlus croppedPlot = cropPlot(plotNode);
                                        if(croppedPlot != null) {
                                                IJ.saveAs(croppedPlot, "Tiff", trialFile.getAbsolutePath()+File.separator+plotNode.getId()+".tif");
                                        }
                                }
                        }

                        JOptionPane.showMessageDialog(null, "Export Completed");
                }
        }
        
        private final void breedPixButton_actionPerformed() {
              BreedPixOptionsPanel panel = new BreedPixOptionsPanel(false);
              panel.setBatchInputDirVisible(false);
              int choice = JOptionPane.showConfirmDialog(null, panel, "BreedPix Options", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
              if(JOptionPane.OK_OPTION == choice) {
                      File saveResultsFile = panel.getSaveResultsFile();
                      File saveImageDir = panel.getSaveImageDir();
                      String csvDelim = panel.getCsvDelimiter();
                      boolean saveGaImage = panel.isSaveGaImage();
                      boolean saveGgaImage = panel.isSaveGgaImage();
                      
                      reloadMosaicImage();
                      processBreedPix(saveResultsFile, saveImageDir, csvDelim, saveGaImage, saveGgaImage);
                      System.gc();
              }
        }
        
        private final void processBreedPix(File saveResultsFile, File saveImageDir, String csvDelim, boolean saveGaImage, boolean saveGgaImage) {
                PicVIOperation picViOperation = new PicVIOperation();
                String imagePath = null;
                
                if(saveImageDir != null) {
                        imagePath = saveImageDir.getAbsolutePath();
                }
                
                char delim = '\0';
                switch(csvDelim) {
                        case "Comma": {
                                delim = ',';
                                break;
                        }
                        
                        case "Space": {
                                delim = ' ';
                                break;
                        }
                        
                        case "Tab": {
                                delim = '\t';
                                break;
                        }
                        
                        case "Pipe": {
                                delim = '|';
                                break;
                        }
                        
                        case "Semi-Colon": {
                                delim = ';';
                                break;
                        }
                        
                        default: {
                                delim = ',';
                                break;
                        }
                }
                
                StringBuilder csvBuilder = new StringBuilder();
                csvBuilder.append("Image Name");
                csvBuilder.append(delim);
                csvBuilder.append("Intensity");
                csvBuilder.append(delim);
                csvBuilder.append("Hue");
                csvBuilder.append(delim);
                csvBuilder.append("Saturation");
                csvBuilder.append(delim);
                csvBuilder.append("Lightness");
                csvBuilder.append(delim);
                csvBuilder.append("a*");
                csvBuilder.append(delim);
                csvBuilder.append("b*");
                csvBuilder.append(delim);
                csvBuilder.append("u*");
                csvBuilder.append(delim);
                csvBuilder.append("v*");
                csvBuilder.append(delim);
                csvBuilder.append("GA");
                csvBuilder.append(delim);
                csvBuilder.append("GGA");
                csvBuilder.append(delim);
                csvBuilder.append("CSI");
                csvBuilder.append(System.getProperty("line.separator"));
                
                @SuppressWarnings("unchecked")
                Enumeration<DefaultMutableTreeNode> enu = rootNode.preorderEnumeration();
                while(enu.hasMoreElements()) {
                        DefaultMutableTreeNode node = enu.nextElement();
                        Object obj = node.getUserObject();
                        if(obj instanceof PlotNode) {
                                PlotNode plotNode = (PlotNode)obj;
                                ImagePlus croppedPlot = cropPlot(plotNode);
                                if(croppedPlot != null) {
                                        BufferedImage image = croppedPlot.getBufferedImage();
                                        BufferedImage scaledRenderedImage = BreedPixTool.reduceToMaxSize(image, 1024*768);
                                        BreedPixResult result = picViOperation.execute(scaledRenderedImage);
                                        String plotId = plotNode.getId();
                                        if(result != null) {
                                                if(saveGaImage) {
                                                        PixelMask gaRoi = result.getGa_roi();
                                                        BufferedImage gaRoiImage = BreedPixTool.paintBWNotROI(scaledRenderedImage, gaRoi, plotId);
                                                        
                                                        try {
                                                                ImageIO.write(gaRoiImage, "jpg", new File(imagePath+File.separator+plotId+"_GA.JPG"));
                                                        }
                                                        catch(IOException ioe) {
                                                                IJ.log("Error saving GA ROI image: " + plotId);
                                                                IJ.log("Error: "+ioe.getMessage());
                                                        }
                                                        finally {
                                                                gaRoi = null;
                                                                gaRoiImage = null;
                                                        }
                                                }
                                                
                                                if(saveGgaImage) {
                                                        PixelMask ggaRoi = result.getGga_roi();
                                                        BufferedImage ggaRoiImage = BreedPixTool.paintBWNotROI(scaledRenderedImage, ggaRoi, plotId);
                                                        
                                                        try {
                                                                ImageIO.write(ggaRoiImage, "jpg", new File(imagePath+File.separator+plotId+"_GGA.JPG"));
                                                        }
                                                        catch(IOException ioe) {
                                                                IJ.log("Error saving GGA ROI image: " + plotId);
                                                                IJ.log("Error: "+ioe.getMessage());
                                                        }
                                                        finally {
                                                                ggaRoi = null;
                                                                ggaRoiImage = null;
                                                        }
                                                }
                                                
                                                csvBuilder.append(plotId);
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getIhs_i());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getIhs_h());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getIhs_s());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLab_l());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLab_a());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLab_b());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLuv_u());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getLuv_v());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getGa());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getGga());
                                                csvBuilder.append(delim);
                                                csvBuilder.append(result.getCsi());
                                                csvBuilder.append(System.getProperty("line.separator"));
                                        }
                                        
                                        scaledRenderedImage = null;
                                        image = null;
                                }
                        }
                }
                
                printResults(saveResultsFile, csvBuilder.toString());
                EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                                JOptionPane.showMessageDialog(null, "BreedPix Analysis Completed");
                        }
                });
        }
        
        private final void printResults(File saveResultsFile, String results) {
                try {
                        FileWriter fw = new FileWriter(saveResultsFile);
                        fw.write(results);
                        fw.flush();
                        fw.close();
                        fw = null;
                }
                catch(IOException ioe) {
                        IJ.log("Error saving csv file: " + saveResultsFile.getAbsoluteFile());
                        IJ.log("Error: "+ioe.getMessage());
                        IJ.error("I/O Error", "There was an error while saving the csv file.");
                }
        }
        
        public final ImagePlus cropPlot(PlotNode plotNode) {
                if(getMosaicImage().getStackSize() > 1) {
                        return cropImageStack(plotNode);
                }
                
                return cropRGB(plotNode);
        }

        private ImagePlus cropRGB(PlotNode plotNode) {
                ImagePlus croppedImage = null;
                
                if(plotNode != null) {
                        IJ.setBackgroundColor(0, 0, 0);
                        PolygonRoi polyRoi = plotNode.getPolyRoi();
                        double rotationAngle = plotNode.getRotateAngle();
                        ImagePlus mosaicImage = getMosaicImage();
                        mosaicImage.setRoi(polyRoi);
                        croppedImage = mosaicImage.crop();
                        //IJ.run(plotImage, "Clear Outside", "");
                        //IJ.log("Plot: "+plotNode.getId()+" : "+PlotNode.ANGLE_DECIMAL_FORMAT.format(rotationAngle));
                        IJ.run(croppedImage, "Rotate... ", "angle="+PlotNode.ANGLE_DECIMAL_FORMAT.format(rotationAngle)+" grid=1 interpolation=Bilinear enlarge");    
                }

                return croppedImage;
        }

        private final ImagePlus cropImageStack(PlotNode plotNode) {
                ImagePlus croppedImage = null;
                
                if(plotNode != null) {
                        PolygonRoi polyRoi = plotNode.getPolyRoi();
                        double rotationAngle = plotNode.getRotateAngle();
                        ImagePlus mosaicImage = getMosaicImage();
                        mosaicImage.setRoi(polyRoi);
                        // First C  (first channel)
                        // Last C   (last channel) = 12
                        // First Z  (first slice position)
                        // Last Z   (last slice position)
                        // First T  (first frame position)
                        // Last T   (last frame position)

                        //IJ.log(imagePlus.getNChannels());
                        //IJ.log(imagePlus.getNSlices());
                        //IJ.log(imagePlus.getNFrames());
                        
                        croppedImage = new Duplicator().run(mosaicImage, 1, mosaicImage.getNChannels(), 1, mosaicImage.getNSlices(), 1, mosaicImage.getNFrames());
                        IJ.run(croppedImage, "Rotate... ", "angle="+PlotNode.ANGLE_DECIMAL_FORMAT.format(rotationAngle)+" grid=1 interpolation=Bilinear enlarge");
                }
                
                return croppedImage;
        }
        
        public final void reloadMosaicImage() {
                close();
                ImagePlus imagePlus = openImage(mosaicFile);
                setMosaicImage(imagePlus);
        }
        
        private final JPanel createReplicatePanel() {
                repAreaLabel = new JLabel("Area: ");
                repAreaLabel.setHorizontalAlignment(JLabel.RIGHT);
                repAreaLabel.setToolTipText("Capture replicate or enter the 4 point polygon points manually");
                repAreaLabel.setPreferredSize(LABEL_PREF_SIZE);
                
                upperLeftCornerField = new JTextField("", 5);
                upperLeftCornerField.setToolTipText("Upper left corner point (x,y)");
                upperLeftCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                lowerLeftCornerField = new JTextField("", 5);
                lowerLeftCornerField.setToolTipText("Lower left corner point (x,y)");
                lowerLeftCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                lowerRightCornerField = new JTextField("",5);
                lowerRightCornerField.setToolTipText("Lower right corner point (x,y)");
                lowerRightCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                upperRightCornerField = new JTextField("",5);
                upperRightCornerField.setToolTipText("Upper right corner point (x,y)");
                upperRightCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                JLabel replicateAreaLabel = new JLabel("Selected:");
                replicateAreaLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                replicateAreaField = new JTextField("", 10);
                replicateAreaField.setBackground(new Color(211,211,211));
                
                JPanel repAreaPanel = new JPanel(new GridLayout(1, 4, 5, 5));
                repAreaPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                repAreaPanel.add(upperLeftCornerField);
                repAreaPanel.add(lowerLeftCornerField);
                repAreaPanel.add(lowerRightCornerField);
                repAreaPanel.add(upperRightCornerField);
                
                repAreaButton = new JButton("Capture");
                repAreaButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                repAreaButton_actionPerformed();
                        }
                });
                
                double[][] panelLayout = {
                                {
                                        TableLayout.PREFERRED, 5, TableLayout.FILL, 5, TableLayout.PREFERRED
                                },
                                
                                {
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED
                                }
                };
                
                JPanel panel = new JPanel(new TableLayout(panelLayout));
                panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                panel.add(repAreaLabel,       "0, 0");
                panel.add(repAreaPanel,       "2, 0");
                panel.add(repAreaButton,      "4, 0");
                panel.add(replicateAreaLabel, "0, 2");
                panel.add(replicateAreaField, "2, 2, 4");
                return panel;
        }
        
        private final JPanel createPlotPanel() {
                plotHeightLabel = new JLabel("Height:");
                plotHeightLabel.setHorizontalAlignment(JLabel.RIGHT);
                plotHeightLabel.setPreferredSize(LABEL_PREF_SIZE);
                plotHeightField = new JTextField(15);
                
                plotSpacingLabel = new JLabel("Spacing:");
                plotSpacingLabel.setHorizontalAlignment(JLabel.RIGHT);
                plotSpacingLabel.setPreferredSize(LABEL_PREF_SIZE);
                plotSpacingField = new JTextField(15);
                
                plotBufferXLabel = new JLabel("Buffer X:");
                plotBufferXLabel.setHorizontalAlignment(JLabel.RIGHT);
                plotBufferXField = new JTextField(11);
                
                plotBufferYLabel = new JLabel("Buffer Y:");
                plotBufferYLabel.setHorizontalAlignment(JLabel.RIGHT);
                plotBufferYField = new JTextField(15);
                
                double[][] panelLayout = {
                                {
                                        TableLayout.PREFERRED, 5, TableLayout.FILL, 5, TableLayout.PREFERRED, 5, TableLayout.FILL
                                },
                                
                                {
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED,
                                }
                };
                
                JPanel panel = new JPanel(new TableLayout(panelLayout));
                panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                panel.add(plotHeightLabel,  "0, 0");
                panel.add(plotHeightField,  "2, 0");
                panel.add(plotBufferXLabel, "4, 0");
                panel.add(plotBufferXField, "6, 0");
                panel.add(plotSpacingLabel, "0, 2");
                panel.add(plotSpacingField, "2, 2");
                panel.add(plotBufferYLabel, "4, 2");
                panel.add(plotBufferYField, "6, 2");
                return panel;
        }
        
        private final JPanel createTrialPanel() {
                mosaicImageLabel = new JLabel("Mosaic:");
                mosaicImageLabel.setHorizontalAlignment(JLabel.RIGHT);
                mosaicImageLabel.setPreferredSize(LABEL_PREF_SIZE);

                mosaicField = new JTextField(30);
                mosaicField.setEditable(false);
                mosaicField.setBackground(Color.WHITE);

                mosaicButton = new JButton("...");
                mosaicButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                mosaicButton_actionPerformed();
                        }
                });
                
                trialLabel = new JLabel("Name:");
                trialLabel.setHorizontalAlignment(JLabel.RIGHT);
                trialLabel.setPreferredSize(LABEL_PREF_SIZE);
                trialField = new JTextField(15);
                
                trialScaleLabel = new JLabel("Scale:");
                trialScaleLabel.setHorizontalAlignment(JLabel.RIGHT);
                trialScaleField = new JTextField(15);
                trialScaleField.setEditable(false);
                trialScaleField.setEnabled(false);
                
                double[][] panelLayout = {
                                {
                                        TableLayout.PREFERRED, 5, TableLayout.PREFERRED, 5, TableLayout.PREFERRED, 5, TableLayout.PREFERRED, 5, TableLayout.PREFERRED
                                },
                                
                                {
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED,
                                }
                };
                
                JPanel panel = new JPanel(new TableLayout(panelLayout));
                panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                panel.add(trialLabel,       "0, 0");
                panel.add(trialField,       "2, 0");
                panel.add(trialScaleLabel,  "4, 0");
                panel.add(trialScaleField,  "6, 0");
                panel.add(mosaicImageLabel, "0, 2");
                panel.add(mosaicField,      "2, 2, 6");
                panel.add(mosaicButton,     "8, 2");
                return panel;
        }
        
        private final void validateField(JTextField textField) {
                String txt = textField.getText();
                if(txt.isEmpty() || XY_REGEX.matcher(txt).matches()) {
                        textField.setBackground(Color.WHITE);
                        textField.setForeground(Color.BLACK);
                }
                else {
                        textField.setBackground(Color.RED);
                        textField.setForeground(Color.WHITE);
                }
        }
        
        private final void repAreaButton_actionPerformed() {
                if(upperLeftCornerField.getText().isEmpty() &&
                   lowerLeftCornerField.getText().isEmpty() &&
                   upperRightCornerField.getText().isEmpty() &&
                   lowerRightCornerField.getText().isEmpty()) {
                        captureReplicate_actionPerformed();
                }
                else {
                        addReplicate_actionPerformed();
                }
                
                resetReplicateAreaFields();
        }
        
        private final JPanel createStatusPanel() {
                statusLabel = new JLabel();
                statusLabel.setHorizontalAlignment(JLabel.LEFT);
                statusLabel.setHorizontalTextPosition(JLabel.TRAILING);
                statusLabel.setOpaque(true);
                
                statusButton = new JButton();
                statusButton.setFocusable(false);
                statusButton.setFocusPainted(false);
                statusButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                statusButton_actionPerformed();
                        }
                });
                
                
                double[][] panelLayout = {
                                {
                                        TableLayout.FILL, 5, TableLayout.PREFERRED
                                },
                                
                                {
                                        TableLayout.PREFERRED,
                                }
                };
                
                JPanel panel = new JPanel(new TableLayout(panelLayout));
                panel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
                panel.add(statusLabel, "0, 0");
                panel.add(statusButton, "2, 0");
                return panel;
        }
        
        private final void setStatus(StatusMode statusMode) {
                Border border1 = BorderFactory.createLineBorder(statusMode.getStatusBorder(), 1, true);
                Border border2 = BorderFactory.createEmptyBorder(5, 10, 5, 10);
                statusLabel.setBackground(statusMode.getStatusBackground());
                statusLabel.setForeground(statusMode.getStatusForeground());
                statusLabel.setBorder(BorderFactory.createCompoundBorder(border1, border2));
                
                switch(statusMode) {
                        case EDIT: {
                                statusLabel.setIcon(UITool.getImageIcon("/edu/ub/bioveg/fisioveg/mosaic/ui/icons/edit_22.png"));
                                statusLabel.setText("<html><strong>"+statusMode.getStatus()+"</strong></html>");
                                statusButton.setText("<html><strong>Save</strong></html>");
                                break;
                        }
                        
                        case VIEW: {
                                statusLabel.setIcon(UITool.getImageIcon("/edu/ub/bioveg/fisioveg/mosaic/ui/icons/apply_22.png"));
                                statusLabel.setText("<html><strong>"+statusMode.getStatus()+"</strong></html>");
                                statusButton.setText("<html><strong>"+StatusMode.EDIT.getStatus()+"</strong></html>");
                                break;
                        }
                        
                        default: {
                                statusLabel.setIcon(null);
                                break;
                        }
                }
        }
        
        private final void mosaicButton_actionPerformed() {
                mosaicFile = FileOpen.getFile("Select Mosaic Image", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.FILES_ONLY, "Mosaic Images", "jpg", "jpeg", "tiff", "tif", "png");
                if (mosaicFile != null) {
                        recentDir = mosaicFile.getParent();
                        mosaicField.setText(mosaicFile.getAbsolutePath());
                        ImagePlus imagePlus = openImage(mosaicFile);
                        setMosaicImage(imagePlus);
                        if(imagePlus != null) {
                                setMosaicScale();
                                createTrial_actionPerformed();
                        }
                        else {
                                JOptionPane.showMessageDialog(this, "There was an error while trying to open the mosaic file", "File Open Error", JOptionPane.ERROR_MESSAGE);
                        }
                }
        }
        
        private final void setMosaicImage(ImagePlus mosaicImage) throws NullPointerException {
                if(mosaicImage == null) {
                        throw new NullPointerException("mosaicImage cannot be set to null");
                }
                
                this.mosaicImage = mosaicImage;
        }
        
        private final ImagePlus getMosaicImage() {
                return mosaicImage;
        }
      
        private final ImagePlus openImage(File mosaicFile) {
                ImagePlus imagePlus = IJ.openImage(mosaicFile.getAbsolutePath());
                if (imagePlus != null) {
                        imagePlus.show();
                }
                
                return imagePlus;
        }
      
        private final void setMosaicScale() {
                StringBuilder sb = new StringBuilder();
                ImagePlus imagePlus = getMosaicImage();
                
                IJ.run(imagePlus, "Set Scale...", "distance=1 known=1 pixel=1.0 unit=pixel");
                Calibration cal = imagePlus.getCalibration();
                if (cal.scaled()) {
                        boolean unitsMatch = cal.getXUnit().equalsIgnoreCase(cal.getYUnit());
                        double cwidth = imagePlus.getWidth() * cal.pixelWidth;
                        double cheight = imagePlus.getHeight() * cal.pixelHeight;
                        int digits = Tools.getDecimalPlaces(cwidth, cheight);
                        if (digits > 2) {
                                digits = 2;
                        }

                        if (unitsMatch) {
                                sb.append(1);
                                sb.append(" pixel");
                                sb.append('/');
                                sb.append("pixel");
                                sb.append(' ');
                                sb.append(IJ.d2s(cwidth, digits));
                                sb.append('x');
                                sb.append(IJ.d2s(cheight, digits));
                                sb.append(' ');
                                sb.append(cal.getUnits());
                                sb.append(' ');
                                sb.append('(');
                                sb.append(imagePlus.getWidth());
                                sb.append('x');
                                sb.append(imagePlus.getHeight());
                                sb.append(')');
                        }
                        else {
                                sb.append(IJ.d2s(cwidth, digits));
                                sb.append(' ');
                                sb.append(cal.getXUnit());
                                sb.append('x');
                                sb.append(IJ.d2s(cheight));
                                sb.append(' ');
                                sb.append(cal.getYUnit());
                                sb.append(' ');
                                sb.append('(');
                                sb.append(imagePlus.getWidth());
                                sb.append('x');
                                sb.append(imagePlus.getHeight());
                                sb.append(')');
                        }
                }
                else {
                        sb.append(imagePlus.getWidth());
                        sb.append('x');
                        sb.append(imagePlus.getHeight());
                        sb.append(' ');
                        sb.append("pixels");
                }

                trialScaleField.setText(sb.toString());
        }
        
        private final JPanel createTreePanel() {
                rootNode = new DefaultMutableTreeNode("");
                rootNode.setAllowsChildren(true);
                
                trialTree = new JTree(rootNode);
                trialTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
                trialTree.setRootVisible(false);
                trialTree.addTreeSelectionListener(new TreeSelectionListener() {
                        @Override
                        public void valueChanged(TreeSelectionEvent e) {
                                trialTree_valueChanged(e);
                        }
                });
                
                trialTree.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseReleased(MouseEvent e) {
                                DefaultMutableTreeNode node = (DefaultMutableTreeNode)trialTree.getLastSelectedPathComponent();
                                if(node == null) {
                                        resetReplicateAreaFields();
                                }
                        }
                });
                
                JScrollPane treeScrollPane = new JScrollPane(trialTree);
                
                JPanel treePanel = new JPanel(new BorderLayout(5,5));
                treePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                treePanel.add(treeScrollPane, BorderLayout.CENTER);
                
                return treePanel;
        }
        
        private final void trialTree_valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)trialTree.getLastSelectedPathComponent();
                if(node != null) {
                        Object obj = node.getUserObject();
                        if (node.isRoot() && node.getAllowsChildren()) {
                                //IJ.log("Root Node:" + obj);
                        }
                        else if (node.isLeaf() && !node.isRoot() && node.getAllowsChildren()) {
                                if(obj instanceof ReplicateNode) {
                                        ReplicateNode replicateNode = (ReplicateNode)node.getUserObject();
                                        getMosaicImage().setRoi(replicateNode.getPolyRoi());
                                        fillReplicateAreaField(replicateNode);
                                        
                                        //IJ.log("Replicate Node: " + replicateNode.toString());
                                }
                                else {
                                        //IJ.log("5. What is this: "+obj);
                                }
                        }
                        else if (!node.isLeaf() && !node.isRoot() && node.getAllowsChildren()) {
                                if(obj instanceof ReplicateNode) {
                                        ReplicateNode replicateNode = (ReplicateNode)node.getUserObject();
                                        getMosaicImage().setRoi(replicateNode.getPolyRoi());
                                        fillReplicateAreaField(replicateNode);
                                        
                                        //IJ.log("Replicate Node: " + replicateNode.toString());
                                }
                                else {
                                        //IJ.log("4. What is this: "+obj);
                                }
                        }
                        else if (node.isLeaf() && !node.isRoot() && !node.getAllowsChildren()) {
                                if(obj instanceof PlotNode) {
                                        PlotNode plotNode = (PlotNode)node.getUserObject();
                                        getMosaicImage().setRoi(plotNode.getPolyRoi());
                                        
                                        //IJ.log("Plot Node: " + plotNode.toString()+" "+plotNode.getRotateAngle());
                                }
                                else {
                                        //IJ.log("1. What is this: "+obj);
                                }
                        }
                        else {
                                //IJ.log("2. What is this: "+obj);
                        }
                }
        }
        
        private final void fillReplicateAreaField(ReplicateNode replicateNode) {
                if(replicateNode != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(replicateNode.getId())
                        .append(" = ")
                        .append('(')
                        .append(replicateNode.getUpperLeftCorner().toString())
                        .append(')')
                        .append(' ')
                        .append('(')
                        .append(replicateNode.getUpperRightCorner().toString())
                        .append(')')
                        .append(' ')
                        .append('(')
                        .append(replicateNode.getLowerLeftCorner().toString())
                        .append(')')
                        .append(' ')
                        .append('(')
                        .append(replicateNode.getLowerRightCorner().toString())
                        .append(')')
                        .append(' ');
                        replicateAreaField.setText(sb.toString());
                }
                else {
                        replicateAreaField.setText("");
                }
        }
        
        private final void resetReplicateAreaFields() {
                upperLeftCornerField.setText("");
                lowerLeftCornerField.setText("");
                upperRightCornerField.setText("");
                lowerRightCornerField.setText("");
                validateField(upperLeftCornerField);
                validateField(lowerLeftCornerField);
                validateField(upperRightCornerField);
                validateField(lowerRightCornerField);
        }
        
//        private final void trialTree_rightClickMouseReleased(MouseEvent me) {
//                JMenuItem createTrialMenuItem = new JMenuItem();
////                createTrialMenuItem.setText("Create Trial");
////                createTrialMenuItem.addActionListener(new ActionListener() {
////                        @Override
////                        public void actionPerformed(ActionEvent e) {
////                                createTrialMenuItem_actionPerformed();
////                        }
////                });
////                
////                JMenuItem addReplicateMenuItem = new JMenuItem();
////                addReplicateMenuItem.setText("Add Replicate");
////                addReplicateMenuItem.addActionListener(new ActionListener() {
////                        @Override
////                        public void actionPerformed(ActionEvent e) {
////                                captureReplicate_actionPerformed();
////                        }
////                });
////                
////                JMenuItem addReplicateManuallyMenuItem = new JMenuItem();
////                addReplicateManuallyMenuItem.setText("Add Replicate Manually");
////                addReplicateManuallyMenuItem.addActionListener(new ActionListener() {
////                        @Override
////                        public void actionPerformed(ActionEvent e) {
////                                addReplicate_actionPerformed();
////                        }
////                });
//                
//                JMenuItem segmentMenuItem = new JMenuItem();
//                segmentMenuItem.setText("Auto-Segment plots");
//                segmentMenuItem.addActionListener(new ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent e) {
//                                segmentPlots_actionPerformed();
//                        }
//                });
//                
//                JMenuItem deleteReplicateMenuItem = new JMenuItem();
//                deleteReplicateMenuItem.setText("Delete Replicate");
//                deleteReplicateMenuItem.addActionListener(new ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent e) {
//                                deleteNodes_actionPerformed();
//                        }
//                });
//                
//                JMenuItem deletePlotMenuItem = new JMenuItem();
//                deletePlotMenuItem.setText("Delete Plot(s)");
//                deletePlotMenuItem.addActionListener(new ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent e) {
//                                deleteNodes_actionPerformed();
//                        }
//                });
//                
//                JMenuItem cloneReplicateMenuItem = new JMenuItem();
//                cloneReplicateMenuItem.setText("Clone");
//                cloneReplicateMenuItem.addActionListener(new ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent e) {
//                                cloneReplicate_actionPerformed();
//                        }
//                });
//                
//                JMenuItem namingSchemeMenuItem = new JMenuItem();
//                namingSchemeMenuItem.setText("Apply Naming Scheme");
//                namingSchemeMenuItem.addActionListener(new ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent e) {
//                                namingSchemeMenuItem_actionPerformed();
//                        }
//                });
//                
//                JPopupMenu treePopup = new JPopupMenu();
//                
//                DefaultMutableTreeNode node = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
//                if (node != null) {
//                        if (node.isRoot() && node.getAllowsChildren() && trialTree.isRootVisible()) {
//                                //Trial with or without replicates
////                                if(node.getChildCount() > 0) {
////                                        treePopup.add(namingSchemeMenuItem);
////                                }
////                                else {
////                                        treePopup.add(addReplicateMenuItem);
////                                        treePopup.add(addReplicateManuallyMenuItem);
////                                }
//                        }
//                        else if (node.isLeaf() && !node.isRoot() && node.getAllowsChildren()) {
//                                //Replicate with no plots
//                                treePopup.add(segmentMenuItem);
//                                treePopup.add(deleteReplicateMenuItem);
//                        }
//                        else if (!node.isLeaf() && !node.isRoot() && node.getAllowsChildren()) {
//                                //Replicate with plots
//                                treePopup.add(cloneReplicateMenuItem);
//                                treePopup.add(deleteReplicateMenuItem);
//                        }
//                        else if (node.isLeaf() && !node.isRoot() && !node.getAllowsChildren()) {
//                                //Plot
//                                treePopup.add(deletePlotMenuItem);
//                        }
//                }
//                else {
//                        treePopup.add(createTrialMenuItem);
//                }
//                
//                treePopup.show(trialTree, me.getX(), me.getY());
//        }
        
        private final void createTrial_actionPerformed() {
                //Check if root is leaf for extra safety
                if(rootNode.isLeaf()) {
                        String rootNodeName = trialField.getText();
                        if(rootNodeName.isEmpty()) {
                                rootNodeName = "trial";
                        }
                        
                        rootNode = new DefaultMutableTreeNode(rootNodeName);
                        rootNode.setAllowsChildren(true);
                        
                        DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                        treeModel.setRoot(rootNode);
                        treeModel.reload();
                        trialTree.setRootVisible(true);
                }
        }
        
        private final void captureReplicate_actionPerformed() {
                ImagePlus imagePlus = getMosaicImage();
                Roi roi = imagePlus.getRoi();
                if(roi instanceof PolygonRoi) {
                        PolygonRoi polyRoi = (PolygonRoi)roi;
                        ReplicateNode replicateNode = createReplicateNode(polyRoi);
                        getMosaicImage().setRoi(polyRoi);
                        addReplicateNodeToTree(replicateNode);
                        fillReplicateAreaField(replicateNode);
                }
                else {
                        JOptionPane.showMessageDialog(this, "Replicate area must be a 4 point polygon", "Replicate Polygon", JOptionPane.INFORMATION_MESSAGE);
                }
        }
        
        private final void addReplicate_actionPerformed() {
                if(!upperLeftCornerField.getText().isEmpty() &&
                   !lowerLeftCornerField.getText().isEmpty() &&
                   !upperRightCornerField.getText().isEmpty() &&
                   !lowerRightCornerField.getText().isEmpty()) {
                        Corner cr1 = Corner.parse(upperLeftCornerField.getText());
                        Corner cr2 = Corner.parse(lowerLeftCornerField.getText());
                        Corner cr3 = Corner.parse(lowerRightCornerField.getText());
                        Corner cr4 = Corner.parse(upperRightCornerField.getText());

                        int[] xPoints = new int[] { cr1.x, cr2.x, cr3.x, cr4.x };
                        int[] yPoints = new int[] { cr1.y, cr2.y, cr3.y, cr4.y };
                        PolygonRoi polyRoi = new PolygonRoi(xPoints, yPoints, 4, Roi.POLYGON);
                        ReplicateNode replicateNode = createReplicateNode(polyRoi);
                        getMosaicImage().setRoi(polyRoi);
                        addReplicateNodeToTree(replicateNode);
                }
        }
        
        private final void segmentPlots_actionPerformed() {
                if(plotHeightField.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Plot height must be defined.", "Missing Plot Height", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(plotSpacingField.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Plot spacing must be defined.", "Missing Plot Spacing", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                DefaultMutableTreeNode repTreeNode = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
                if(repTreeNode != null) {
                        Object obj = repTreeNode.getUserObject();
                        if(obj instanceof ReplicateNode) {
                                ReplicateNode repNode = (ReplicateNode)obj;
                                
                                if(repTreeNode.getChildCount() > 0) {
                                        repTreeNode.removeAllChildren();
                                }
                                
                                PlotOptions plotOpts = new PlotOptions();
                                plotOpts.setPlotHeight(plotHeightField.getText());
                                plotOpts.setPlotVerticalSpacing(plotSpacingField.getText());
                                plotOpts.setBufferX(plotBufferXField.getText());
                                plotOpts.setBufferY(plotBufferYField.getText());
                                
                                PixelChaser pixelChaser = new PixelChaser();
                                pixelChaser.setPlotOptions(plotOpts);
                                
                                ImagePlus imagePlus = getMosaicImage();
                                
                                ImageProcessor ip = null;
                                if(imagePlus.getStackSize() > 1) {
                                        ImageStack stack = imagePlus.getStack();
                                        ImageProcessor _ip = stack.getProcessor(1);
                                        ImagePlus tempImagePlus = new ImagePlus("tmp_1", _ip);
                                        ImageConverter ic = new ImageConverter(tempImagePlus);
                                        ic.convertToRGB();
                                        ip = tempImagePlus.getProcessor();
                                }
                                else {
                                        //Some images are not RGB and need to be converted.
                                        ImageConverter ic = new ImageConverter(imagePlus);
                                        ic.convertToRGB();
                                        ip = imagePlus.getProcessor();
                                }
                                
                                PolygonRoi polyRoi = repNode.getPolyRoi();
                                
                                try {
                                        List<Plot> plots = pixelChaser.drawPlots(ip, polyRoi);
                                        imagePlus.updateAndDraw();
                                        
                                        int plotNumber = 1;
                                        for(int i=0;  i < plots.size(); i++) {
                                                Plot plot = plots.get(i);
                                                PlotNode plotNode = createPlotNode(plot, plotNumber, repNode.getId(), repNode.getRotateAngle());
                                                plotNumber++;
                                                
                                                DefaultMutableTreeNode plotTreeNode = new DefaultMutableTreeNode(plotNode);
                                                plotTreeNode.setAllowsChildren(false);
                                                repTreeNode.add(plotTreeNode);
                                        }

                                        DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                                        treeModel.reload(repTreeNode);
                                        nodeMap.put(repNode.getId(), repTreeNode);
                                        
                                        trialTree.expandPath(new TreePath(repTreeNode.getPath()));
                                        trialTree.validate();
                                        trialTree.repaint();
                                }
                                catch(IndexOutOfBoundsException ex) {
                                        JOptionPane.showMessageDialog(null, "There was an error while chasing the pixels around the replicate, try to adjust the plot height and try again.", "Pixel Chaser Error", JOptionPane.ERROR_MESSAGE);
                                        RoiManager.getRoiManager().deselect();
                                        RoiManager.getRoiManager().reset();
                                        
                                        setStatus(StatusMode.ERROR);
                                }
                                finally {
                                        ip = null;
                                        pixelChaser = null;
                                }
                        }
                }
        }
        
        private final void cloneReplicate_actionPerformed() {
                ReplicateClonePanel repClonePanel = new ReplicateClonePanel();
                NamingSchemePanel namingSchemePanel = new NamingSchemePanel();
                
                repClonePanel.setBorder(BorderFactory.createTitledBorder("Clone"));
                namingSchemePanel.setBorder(BorderFactory.createTitledBorder("Naming"));
                
                Box box = Box.createVerticalBox();
                box.add(repClonePanel);
                box.add(Box.createVerticalStrut(5));
                box.add(namingSchemePanel);
                
                int choice = JOptionPane.showConfirmDialog(null, box, "Enter Clone Properties", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if(JOptionPane.OK_OPTION == choice) {
                        performReplicateCloning(repClonePanel);
                        performNamingScheme(namingSchemePanel);
                }
        }
        
        private final void performNamingScheme(NamingSchemePanel namingSchemePanel) {
                int startAt = namingSchemePanel.getStartAt();
                int plotIncBy = namingSchemePanel.getPlotIncrementBy();
                int repIncBy = namingSchemePanel.getRepIncrementBy();
                NamingStyle namingStlye = namingSchemePanel.getNamingStyle();
                
                String repId = null;
                int gCounter = startAt;
                int repCounter = 0;
                int childCount = 0;
                
                @SuppressWarnings("unchecked")
                Enumeration<DefaultMutableTreeNode> enu = rootNode.preorderEnumeration();
                while(enu.hasMoreElements()) {
                        DefaultMutableTreeNode replicateTreeNode = enu.nextElement();
                        Object obj = replicateTreeNode.getUserObject();
                        if(obj instanceof ReplicateNode) {
                                ReplicateNode replicateNode = (ReplicateNode)obj;
                                repId = replicateNode.getId();
                                childCount = replicateTreeNode.getChildCount();
                                
                                //IJ.log("Rep Node "+repId);
                                
                                if(childCount > 0) {
                                        repCounter++;
                                        //IJ.log("repCounter = "+repCounter);
                                        startAt = startAt + repIncBy;
                                        gCounter = gCounter + repIncBy;
                                        
                                        if(NamingStyle.SERPENTINE.equals(namingStlye)) {
                                                if((repCounter & 1) == 0) {
                                                        //Even plot number so start at max count and go downwards
                                                        startAt = startAt + (childCount * plotIncBy);
                                                        //IJ.log("Even Rep");
                                                }
                                                else {
                                                        //Odd plot number start at min count and go upwards
                                                        startAt = gCounter;
                                                        //IJ.log("Odd Rep");
                                                }
                                        }
                                        
                                        //IJ.log("startAt = "+startAt+" , gCounter = "+gCounter);
                                        
                                        int i = 0;
                                        @SuppressWarnings("unchecked")
                                        Enumeration<DefaultMutableTreeNode> childEnum = replicateTreeNode.preorderEnumeration();
                                        while(childEnum.hasMoreElements()) {
                                                DefaultMutableTreeNode plotTreeNode = childEnum.nextElement();
                                                Object childObj = plotTreeNode.getUserObject();
                                                if(childObj instanceof PlotNode) {
                                                        i++;
                                                        //IJ.log("      startAt = "+startAt+" , gCounter = "+gCounter+", i = "+i);
                                                        
                                                        String name = "Plot-"+String.format("%03d", Integer.valueOf(startAt));
                                                        String id =  repId+"_"+name;
                                                        
                                                        PlotNode plotNode = (PlotNode)childObj;
                                                        plotNode.setName(id);
                                                        plotNode.setId(id);
                                                        plotNode.setSequence(startAt);
                                                        plotNode.getPolyRoi().setName(id);
                                                        
                                                        if(NamingStyle.SERPENTINE.equals(namingStlye)) {
                                                                if(i < childCount) {
                                                                        if((repCounter & 1) == 0) {
                                                                                startAt = startAt - plotIncBy;
                                                                        }
                                                                        else {
                                                                                startAt = startAt + plotIncBy;
                                                                        }
                                                                }
                                                        }
                                                        else {
                                                                startAt = startAt + plotIncBy;
                                                        }
                                                        
                                                        gCounter = gCounter + plotIncBy;
                                                }
                                        }
                                }
                        }
                }
                
                //IJ.log("-----------------------------");
                //DebugTool.printTree(rootNode);
        }
        
        private final void performReplicateCloning(ReplicateClonePanel repClonePanel) {
                int count = repClonePanel.getReplicateCount();
                int offset = repClonePanel.getReplicateSpacing();
                CloneDirection direction = repClonePanel.getCloneDirection();
                
                DefaultMutableTreeNode repTreeNode = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
                if(repTreeNode != null) {
                        ReplicateNode repNode = (ReplicateNode)repTreeNode.getUserObject();
                        PolygonRoi polyRoi = repNode.getPolyRoi();
                        
                        PolygonRoi[] plotPolyRois = null;
                        PolygonRoi[] plotPolyRoisClone = null;
                        PlotNode[] plotNodes = null;
                        boolean clonePlots = false;
                        if(repTreeNode.getChildCount() > 0) {
                                plotPolyRois = getPlotPolygonRois(repTreeNode);
                                plotPolyRoisClone = plotPolyRois;
                                plotNodes = new PlotNode[plotPolyRois.length];
                                clonePlots = true;
                        }
                        
                        PolygonRoi polyRoiClone = polyRoi;
                        
                        for(int i=0; i < count; i++) {
                                polyRoiClone = PolygonTool.clonePolygonRoi(polyRoiClone, offset, direction);
                                ReplicateNode repNodeClone = createReplicateNode(polyRoiClone);
                                
                                if(clonePlots) {
                                        Arrays.fill(plotNodes, null);
                                        for(int j=0; j < plotPolyRois.length; j++) {
                                                PolygonRoi plotPolyRoi = plotPolyRoisClone[j];
                                                PolygonRoi plotPolyRoiClone = PolygonTool.clonePolygonRoi(plotPolyRoi, offset, direction);
                                                plotPolyRoisClone[j] = plotPolyRoiClone;
                                                
                                                PlotNode plotNode = createPlotNode(plotPolyRoiClone, j, repNodeClone.getId(), repNodeClone.getRotateAngle());
                                                plotNodes[j] = plotNode;
                                        }
                                }
                                
                                addReplicateNodeToTree(repNodeClone, plotNodes);
                                getMosaicImage().setRoi(polyRoiClone);
                        }
                }
        }
        
        private final PolygonRoi[] getPlotPolygonRois(DefaultMutableTreeNode replicateTreeNode) {
                PolygonRoi[] polyRois = null;
                
                if(replicateTreeNode.getChildCount() > 0) {
                        int i=0;
                        polyRois = new PolygonRoi[replicateTreeNode.getChildCount()];
                        
                        @SuppressWarnings("unchecked")
                        Enumeration<DefaultMutableTreeNode> enu = replicateTreeNode.preorderEnumeration();
                        while(enu.hasMoreElements()) {
                                DefaultMutableTreeNode node = enu.nextElement();
                                Object obj = node.getUserObject();
                                if(obj instanceof PlotNode) {
                                        PlotNode plotNode = (PlotNode)obj;
                                        polyRois[i] = plotNode.getPolyRoi();
                                        i++;
                                }
                        }
                }
                
                return polyRois;
        }
        
        private final ReplicateNode createReplicateNode(PolygonRoi polyRoi) {
                replicateId++;
                String id = "R-"+String.format("%03d", Integer.valueOf(replicateId));

                polyRoi.setName(id);
                Polygon polygon = polyRoi.getPolygon();
                
                ReplicateNode replicateNode = new ReplicateNode();
                replicateNode.setId(id);
                replicateNode.setSequence(replicateId);
                replicateNode.setPolyRoi(polyRoi);
                replicateNode.setName(id);
                replicateNode.setUpperLeftCorner(Corner.valueOf(polygon.xpoints[0], polygon.ypoints[0]));
                replicateNode.setLowerLeftCorner(Corner.valueOf(polygon.xpoints[1], polygon.ypoints[1]));
                replicateNode.setLowerRightCorner(Corner.valueOf(polygon.xpoints[2], polygon.ypoints[2]));
                replicateNode.setUpperRightCorner(Corner.valueOf(polygon.xpoints[3], polygon.ypoints[3]));
                replicateNode.setRotateAngle(PolygonTool.getAngleRotation(polyRoi.getPolygon()));
                return replicateNode;
        }
        
        private final void statusButton_actionPerformed() {
                isEdit = !isEdit;
                
                if(isEdit) {
                        editNodes();
                }
                else {
                        saveNodes();
                }
        }
        
        private final void editNodes() {
              //Transfer all plot nodes on the tree to the ROI Manager
                setStatus(StatusMode.EDIT);
                RoiManager.getRoiManager().reset();
                
                @SuppressWarnings("unchecked")
                Enumeration<DefaultMutableTreeNode> enu = rootNode.preorderEnumeration();
                while(enu.hasMoreElements()) {
                        DefaultMutableTreeNode node = enu.nextElement();
                        Object obj = node.getUserObject();
                        if(obj instanceof PlotNode) {
                                PlotNode plotNode = (PlotNode)obj;
                                RoiManager.getInstance().addRoi(plotNode.getPolyRoi());
                        }
                }
                
                RoiManager.getRoiManager().runCommand("Associate", "false");
                RoiManager.getRoiManager().runCommand("Centered", "false");
                RoiManager.getRoiManager().runCommand("UseNames", "true");
                RoiManager.getRoiManager().runCommand(getMosaicImage(), "Show All with labels");
        }
        
        private final void saveNodes() {
                setStatus(StatusMode.VIEW);
                
                //Transfer all ROIs from the ROI Manager back into the tree
                if(RoiManager.getRoiManager().getCount() > 0) {
                        Iterator<DefaultMutableTreeNode> iter = nodeMap.values().iterator();
                        while(iter.hasNext()) {
                                DefaultMutableTreeNode treeNode = iter.next();
                                treeNode.removeAllChildren();
                        }
                        
                        DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                        rootNode.removeAllChildren();
                        treeModel.reload(rootNode);
                        getMosaicImage().deleteRoi();
                        RoiManager.getRoiManager().deselect();
                        
                        Roi[] rois = RoiManager.getRoiManager().getRoisAsArray();
                        for(int i = 0; i < rois.length; i++) {
                                PolygonRoi polyRoi = (PolygonRoi)rois[i];
                                String id = polyRoi.getName();
                                String replicateId = id.substring(0, id.indexOf('_'));
                                if(nodeMap.containsKey(replicateId)) {
                                        DefaultMutableTreeNode replicateTreeNode = nodeMap.get(replicateId);
                                        
                                        int plotNumber = Integer.parseInt(polyRoi.getProperty("xseq"));
                                        String name = "Plot-"+String.format("%03d", Integer.valueOf(plotNumber));
                                        
                                        PlotNode plotNode = new PlotNode();
                                        plotNode.setId(id);
                                        plotNode.setSequence(plotNumber);
                                        plotNode.setName(name);
                                        plotNode.setPolyRoi(polyRoi);
                                        plotNode.setRotateAngle(((ReplicateNode)replicateTreeNode.getUserObject()).getRotateAngle());
                                        
                                        DefaultMutableTreeNode plotTreeNode = new DefaultMutableTreeNode(plotNode);
                                        plotTreeNode.setAllowsChildren(false);
                                        
                                        replicateTreeNode.add(plotTreeNode);
                                }
                        }
                        
                        trialTree.expandPath(new TreePath(rootNode.getPath()));
                        
                        iter = nodeMap.values().iterator();
                        while(iter.hasNext()) {
                                DefaultMutableTreeNode replicateNode = iter.next();
                                rootNode.add(replicateNode);
                                trialTree.expandPath(new TreePath(replicateNode.getPath()));
                        }
                        
                        RoiManager.getRoiManager().reset();
                        RoiManager.getRoiManager().close();
                        
                        treeModel.reload(rootNode);
                        trialTree.validate();
                        trialTree.repaint();
                }
        }
        
        private final void deleteReplicateNodes_actionPerformed() {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
                if(node != null) {
                        Object obj = node.getUserObject();
                        if(obj instanceof ReplicateNode) {
                                if(trialTree.getSelectionCount() > 1) {
                                        int choice = JOptionPane.showConfirmDialog(null, "Really delete all "+trialTree.getSelectionCount()+" replicates ?", "Confirm Delete", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                        if(JOptionPane.YES_OPTION == choice) {
                                                Thread t = new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                                deleteReplicateNode(true);
                                                        }
                                                });
                                                t.start();
                                        }
                                }
                                else {
                                        ReplicateNode replicateNode = (ReplicateNode)node.getUserObject();
                                        int choice = JOptionPane.showConfirmDialog(null, "Really delete replicate " + replicateNode.getName() + "?", "Confirm Delete", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                        if (JOptionPane.YES_OPTION == choice) {
                                                Thread t = new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                                deleteReplicateNode(false);
                                                        }
                                                });
                                                t.start();
                                        }
                                }
                        } 
                }
        }
        
        private final void deletePlotNodes_actionPerformed() {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
                if(node != null) {
                        Object obj = node.getUserObject();
                        if(obj instanceof PlotNode) {
                                if(trialTree.getSelectionCount() > 1) {
                                        int choice = JOptionPane.showConfirmDialog(null, "Really delete all "+trialTree.getSelectionCount()+" plots ?", "Confirm Delete", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                        if(JOptionPane.YES_OPTION == choice) {
                                                Thread t = new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                                deletePlotNode(true);
                                                        }
                                                });
                                                t.start();
                                        }
                                }
                                else {
                                        PlotNode plotNode = (PlotNode)node.getUserObject();
                                        int choice = JOptionPane.showConfirmDialog(null, "Really delete plot " + plotNode.getName() + "?", "Confirm Delete", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                        if (JOptionPane.YES_OPTION == choice) {
                                                Thread t = new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                                deletePlotNode(false);
                                                        }
                                                });
                                                t.start();
                                        }
                                }
                        }
                }
        }
        
        private final void deleteReplicateNode(boolean isMulti) {
                if(isMulti) {
                        TreePath[] treePaths = trialTree.getSelectionPaths();
                        for (int i = 0; i < treePaths.length; i++) {
                                DefaultMutableTreeNode node = (DefaultMutableTreeNode)treePaths[i].getLastPathComponent();
                                ReplicateNode replicateNode = (ReplicateNode)node.getUserObject();
                                PolygonRoi polyRoi = replicateNode.getPolyRoi();
                                getMosaicImage().setRoi(polyRoi);
                                getMosaicImage().deleteRoi();

                                DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                                treeModel.removeNodeFromParent(node);
                                nodeMap.remove(replicateNode.getId());
                                trialTree.clearSelection();
                        }
                }
                else {
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
                        if(node != null) {
                                ReplicateNode replicateNode = (ReplicateNode)node.getUserObject();
                                PolygonRoi polyRoi = replicateNode.getPolyRoi();
                                getMosaicImage().setRoi(polyRoi);
                                getMosaicImage().deleteRoi();

                                DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                                treeModel.removeNodeFromParent(node);
                                nodeMap.remove(replicateNode.getId());
                                trialTree.clearSelection();
                        }
                }
        }
        
        private final void deletePlotNode(boolean isMulti) {
                if(isMulti) {
                        TreePath[] treePaths = trialTree.getSelectionPaths();
                        for (int i = 0; i < treePaths.length; i++) {
                                DefaultMutableTreeNode node = (DefaultMutableTreeNode)treePaths[i].getLastPathComponent();
                                if (node != null && node.isLeaf() && !node.isRoot() && !node.getAllowsChildren()) {
                                        PlotNode plotNode = (PlotNode)node.getUserObject();
                                        PolygonRoi polyRoi = plotNode.getPolyRoi();
                                        DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                                        treeModel.removeNodeFromParent(node);
                                        getMosaicImage().setRoi(polyRoi);
                                        getMosaicImage().deleteRoi();
                                }
                        }

                        trialTree.clearSelection();
                }
                else {
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode) trialTree.getLastSelectedPathComponent();
                        if(node != null) {
                                PlotNode plotNode = (PlotNode)node.getUserObject();
                                PolygonRoi polyRoi = plotNode.getPolyRoi();
                                DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                                treeModel.removeNodeFromParent(node);
                                getMosaicImage().setRoi(polyRoi);
                                getMosaicImage().deleteRoi();
                                trialTree.clearSelection();
                        }
                }
        }
        
        private final PlotNode createPlotNode(Plot plot, int plotNumber, String replicateId, double rotationAngle) {
                int[] x = {plot.getUpperLeft().x, plot.getLowerLeft().x, plot.getLowerRight().x, plot.getUpperRight().x};
                int[] y = {plot.getUpperLeft().y, plot.getLowerLeft().y, plot.getLowerRight().y, plot.getUpperRight().y};
                
                String name = "Plot-"+String.format("%03d", Integer.valueOf(plotNumber));
                String id =  replicateId+"_"+name;
                
                PolygonRoi polyRoi = new PolygonRoi(x, y, 4, Roi.POLYGON);
                polyRoi.setName(id);
                polyRoi.setProperty("xseq", Integer.toString(plotNumber));
                
                PlotNode plotNode = new PlotNode();
                plotNode.setId(id);
                plotNode.setSequence(plotNumber);
                plotNode.setName(name);
                plotNode.setPolyRoi(polyRoi);
                plotNode.setRotateAngle(rotationAngle);
                
                return plotNode;
        }
        
        private final PlotNode createPlotNode(PolygonRoi polyRoi, int plotNumber, String replicateId, double rotationAngle) {
                String name = "Plot-"+String.format("%03d", Integer.valueOf(plotNumber));
                String id =  replicateId+"_"+name;
                
                polyRoi.setName(id);
                polyRoi.setProperty("xseq", Integer.toString(plotNumber));
                
                PlotNode plotNode = new PlotNode();
                plotNode.setId(id);
                plotNode.setSequence(plotNumber);
                plotNode.setName(name);
                plotNode.setPolyRoi(polyRoi);
                plotNode.setRotateAngle(rotationAngle);
                
                return plotNode;
        }
        
        private final void addReplicateNodeToTree(ReplicateNode repNode) {
                addReplicateNodeToTree(repNode, null);
        }
        
        private final void addReplicateNodeToTree(ReplicateNode repNode, PlotNode[] plotNodes) {
                DefaultMutableTreeNode replicateTreeNode = new DefaultMutableTreeNode(repNode);
                replicateTreeNode.setAllowsChildren(true);
                
                if(plotNodes != null) {
                        for(int i=0; i < plotNodes.length; i++) {
                                PlotNode plotNode = plotNodes[i];
                                DefaultMutableTreeNode plotTreeNode = new DefaultMutableTreeNode(plotNode);
                                plotTreeNode.setAllowsChildren(false);
                                replicateTreeNode.add(plotTreeNode);
                        }
                }
                
                rootNode.add(replicateTreeNode);
                nodeMap.put(repNode.getId(), replicateTreeNode);
                
                DefaultTreeModel treeModel = (DefaultTreeModel) trialTree.getModel();
                treeModel.reload(rootNode);
                
                trialTree.expandPath(new TreePath(rootNode.getPath()));
                trialTree.validate();
                trialTree.repaint();
        }
        
//        private final void cropPlotsButton_actionPerformed() {
//                if(!replicateListModel.isEmpty()) {
//                        //----- This is a hack
//                        replicateList.setSelectedIndex(0);
//                        replicateList_mouseReleased();
//                        reloadMosaicImage();
//                        
//                        /* 
//                         * The above hack is a temporary work around to get the
//                         * crops to export. It wasn't like this before, but once
//                         * the rotate angle bug was fixed, we kept getting NPE
//                         * every time this method was called. But if the replicate
//                         * was clicked on the list and then this was called then
//                         * everything would work. Which doesn't make sense because
//                         * this method and clicking on the replicate both execute
//                         * the same method
//                         * 
//                         * ReplicateData repData = replicateListModel.get(i);
//                         * replicatePanel.setReplicateData(repData);
//                         * 
//                         * There seems to be some link between that and reloading the mosaic
//                         * image. Too late to figure out, but must resolve this.
//                         * 
//                         */
//                        
//                        //----- This is a hack
//                        
//                        isCropping = true;
//                        setButtonPanelEnabled(false);
//                        
//                        for(int i=0; i < replicateListModel.getSize(); i++) {
//                                ReplicateData repData = replicateListModel.get(i);
//                                replicatePanel.setReplicateData(repData);
//                                plotPanel.setPreviewPlotsButton(false);
//                                plotPanel.cropPlots();
//                        }
//                        
//                        isCropping = false;
//                        setButtonPanelEnabled(true);
//                        exportButton.setEnabled(true);
//                }
//        }
//        
        @Override
        public void close() {
                if(mosaicImage != null) {
                        mosaicImage.close();
                }
        }
}
