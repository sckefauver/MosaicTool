package edu.ub.bioveg.fisioveg.mosaic.ui.panels;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileOpen;
import layout.TableLayout;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 15, 2017
 * 
 */
public class BreedPixOptionsPanel extends JPanel {
        
        private static final long serialVersionUID = 6765707098979993875L;
        
        private JLabel batchInputLabel = null;
        private JTextField batchInputField = null;
        private JButton batchInputButton = null;
        
        private JCheckBox saveGaImageCheckBox = null;
        private JCheckBox saveGgaImageCheckBox = null;
        private JTextField saveImageField = null;
        private JButton saveImageButton = null;
        
        private JLabel resultsFileLabel = null;
        private JTextField resultsFileField = null;
        private JButton resultsFileButton = null;
        
        private JLabel csvDelimiterLabel = null;
        private JComboBox<String> csvDelimiterList = null;
        
        private String recentDir = null;
        private File batchInputDir = null;
        private File saveImageDir = null;
        private File saveResultsFile = null;
        
        public BreedPixOptionsPanel() {
                this(false);
        }
        
        public BreedPixOptionsPanel(boolean showTitleBorder) {
                batchInputLabel = new JLabel("Batch Inputs:");
                batchInputLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                batchInputField = new JTextField(20);
                batchInputField.setEditable(false);
                batchInputField.setBackground(Color.WHITE);
                
                batchInputButton = new JButton("...");
                batchInputButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                batchInputButton_actionPerformed();
                        }
                });
                
              //----------------------------------------------------------------
                
                saveGaImageCheckBox = new JCheckBox("Save GA", false);
                saveGaImageCheckBox.setFocusable(false);
                saveGaImageCheckBox.setSelected(false);
                saveGaImageCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveGaImageCheckBox_actionPerformed();
                        }
                });
                
                saveGgaImageCheckBox = new JCheckBox("GGA Images?", false);
                saveGgaImageCheckBox.setFocusable(false);
                saveGgaImageCheckBox.setSelected(false);
                saveGgaImageCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveGgaImageCheckBox_actionPerformed();
                        }
                });
                
                saveImageField = new JTextField(20);
                saveImageField.setEditable(false);
                saveImageField.setBackground(null);
                
                saveImageButton = new JButton("...");
                saveImageButton.setEnabled(false);
                saveImageButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveImageButton_actionPerformed();
                        }
                });
                
              //----------------------------------------------------------------
                
                resultsFileLabel = new JLabel("CSV Results:");
                resultsFileLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                resultsFileField = new JTextField(20);
                resultsFileField.setEditable(false);
                resultsFileField.setBackground(Color.WHITE);
                
                resultsFileButton = new JButton("...");
                resultsFileButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                resultsFileButton_actionPerformed();  
                        }
                });
                
              //----------------------------------------------------------------
                
                csvDelimiterLabel = new JLabel("CSV Delimiter");
                csvDelimiterLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                csvDelimiterList = new JComboBox<>(new String[] {"Comma", "Space", "Tab", "Pipe", "Semi-Colon"});
                
              //----------------------------------------------------------------
                
                double spacer = 5;
                double[][] layoutSize = {
                                //                   0,                             2,                        4,                             6
                                {TableLayout.PREFERRED, spacer, TableLayout.PREFERRED, spacer, TableLayout.FILL, spacer, TableLayout.PREFERRED},
                                {TableLayout.PREFERRED, //0
                                 spacer,
                                 TableLayout.PREFERRED, //2
                                 spacer,
                                 TableLayout.PREFERRED, //4
                                 spacer,
                                 TableLayout.PREFERRED  //6
                                }
                };
                
                Border border = null;
                if(showTitleBorder) {
                        border = BorderFactory.createTitledBorder("Options");
                }
                else {
                        border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
                }
                
                setBorder(border);
                setLayout(new TableLayout(layoutSize));
                add(batchInputLabel,      "0, 0");
                add(batchInputField,      "2, 0, 4");
                add(batchInputButton,     "6, 0");
                add(resultsFileLabel,     "0, 2");
                add(resultsFileField,     "2, 2, 4");
                add(resultsFileButton,    "6, 2");
                add(csvDelimiterLabel,    "0, 4");
                add(csvDelimiterList,     "2, 4");
                add(saveGaImageCheckBox,  "0, 6");
                add(saveGgaImageCheckBox, "2, 6");
                add(saveImageField,       "4, 6");
                add(saveImageButton,      "6, 6");
        }
        
        private final void batchInputButton_actionPerformed() {
                batchInputDir = FileOpen.getFile("Select batch input folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY, "Batch Image Folder", (String[]) null);
                if (batchInputDir != null) {
                        recentDir = batchInputDir.getAbsolutePath();
                        batchInputField.setText(batchInputDir.getAbsolutePath());
                }
        }
        
        private final void saveGaImageCheckBox_actionPerformed() {
                if(saveGaImageCheckBox.isSelected()) {
                        saveImageButton.setEnabled(true);
                        saveImageField.setBackground(Color.WHITE);
                }
                else {
                        if(!saveGgaImageCheckBox.isSelected()) {
                                saveImageButton.setEnabled(false);
                                saveImageField.setBackground(null);
                        }
                }
        }
        
        private final void saveGgaImageCheckBox_actionPerformed() {
                if(saveGgaImageCheckBox.isSelected()) {
                        saveImageButton.setEnabled(true);
                        saveImageField.setBackground(Color.WHITE);
                }
                else {
                        if(!saveGaImageCheckBox.isSelected()) {
                                saveImageButton.setEnabled(false);
                                saveImageField.setBackground(null);
                        }
                }
        }
        
        private final void saveImageButton_actionPerformed() {
                saveImageDir = FileOpen.getFile("Select GA/GGA images folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY , "GA/GGA Images Folder", (String[])null);
                if(saveImageDir != null) {
                        recentDir = saveImageDir.getAbsolutePath();
                        saveImageField.setText(saveImageDir.getAbsolutePath());
                }
        }
        
        private final void resultsFileButton_actionPerformed() {
                saveResultsFile = FileOpen.getFile("Name Results File", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.FILES_AND_DIRECTORIES , "Results File (*.csv)", "csv");
                if(saveResultsFile != null) {
                        recentDir = saveResultsFile.getParentFile().getAbsolutePath();
                        
                        if(!saveResultsFile.getName().endsWith(".csv")) {
                                String tmp = saveResultsFile.getAbsolutePath();
                                saveResultsFile = new File(tmp+".csv");
                        }
                        
                        resultsFileField.setText(saveResultsFile.getAbsolutePath());
                }
        }
        
        public final File getBtachInputDir() {
                return batchInputDir;
        }
        
        public final File getSaveResultsFile() {
                return saveResultsFile;
        }
        
        public final File getSaveImageDir() {
                return saveImageDir;
        }
        
        public final String getCsvDelimiter() {
                return (String)csvDelimiterList.getSelectedItem();
        }
        
        public final boolean isSaveGaImage() {
                return saveGaImageCheckBox.isSelected();
        }
        
        public final boolean isSaveGgaImage() {
                return saveGgaImageCheckBox.isSelected();
        }
        
        public final void setBatchInputDirVisible(boolean visible) {
                batchInputLabel.setVisible(false);
                batchInputField.setVisible(false);
                batchInputButton.setVisible(false);
        }
        
        public static void main(String ...strings) {
                BreedPixOptionsPanel panel = new BreedPixOptionsPanel(false);
                panel.setBatchInputDirVisible(false);
                int choice = JOptionPane.showConfirmDialog(null, panel, "BreedPix Options", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(JOptionPane.OK_OPTION == choice) {
                        System.out.println(panel.getBtachInputDir());
                        System.out.println(panel.getSaveResultsFile());
                        System.out.println(panel.getSaveImageDir());
                        System.out.println(panel.getCsvDelimiter());
                        System.out.println(panel.isSaveGaImage());
                        System.out.println(panel.isSaveGgaImage());
                }
        }
}
