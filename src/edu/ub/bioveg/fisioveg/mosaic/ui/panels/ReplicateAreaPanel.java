package edu.ub.bioveg.fisioveg.mosaic.ui.panels;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import edu.ub.bioveg.fisioveg.mosaic.tools.Corner;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: April 27, 2017
 * 
 */
public class ReplicateAreaPanel extends JPanel {

        private static final long serialVersionUID = 3988174476234713243L;
        private static final Pattern XY_REGEX = Pattern.compile("^(\\-?\\d+(\\.\\d+)?),(\\-?\\d+(\\.\\d+)?)$");
        
        private JLabel repAreaLabel = null;
        private JTextField upperLeftCornerField = null;
        private JTextField lowerLeftCornerField = null;
        private JTextField lowerRightCornerField = null;
        private JTextField upperRightCornerField = null;
        
        public ReplicateAreaPanel() {
                repAreaLabel = new JLabel("Replicate Area: ");
                repAreaLabel.setHorizontalAlignment(JLabel.RIGHT);
                repAreaLabel.setToolTipText("Enter the 4 point polygon points manually");
                
                upperLeftCornerField = new JTextField("", 10);
                upperLeftCornerField.setToolTipText("Upper left corner point (x,y)");
                upperLeftCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                lowerLeftCornerField = new JTextField("", 10);
                lowerLeftCornerField.setToolTipText("Lower left corner point (x,y)");
                lowerLeftCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                lowerRightCornerField = new JTextField("",10);
                lowerRightCornerField.setToolTipText("Lower right corner point (x,y)");
                lowerRightCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                upperRightCornerField = new JTextField("",10);
                upperRightCornerField.setToolTipText("Upper right corner point (x,y)");
                upperRightCornerField.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                                validateField((JTextField)e.getComponent());
                        }
                });
                
                
                setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(repAreaLabel);
                add(upperLeftCornerField);
                add(lowerLeftCornerField);
                add(lowerRightCornerField);
                add(upperRightCornerField);
        }
        
        public final Corner getUpperLeftCorner() {
                String xyStr = upperLeftCornerField.getText();
                return Corner.parse(xyStr);
        }
        
        public final Corner getLowerLeftCorner() {
                String xyStr = lowerLeftCornerField.getText();
                return Corner.parse(xyStr);
        }
        
        public final Corner getLowerRightCorner() {
                String xyStr = lowerRightCornerField.getText();
                return Corner.parse(xyStr);
        }
        
        public final Corner getUpperRightCorner() {
                String xyStr = upperRightCornerField.getText();
                return Corner.parse(xyStr);
        }
        
        private final void validateField(JTextField textField) {
                String txt = textField.getText();
                if(!XY_REGEX.matcher(txt).matches()) {
                        textField.setBackground(Color.RED);
                        textField.setForeground(Color.WHITE);
                }
                else {
                        textField.setBackground(Color.WHITE);
                        textField.setForeground(Color.BLACK);
                }
        }
        
//        public static void main(String ...strings) {
//                ReplicateAreaPanel repAreaPanel = new ReplicateAreaPanel();
//                int choice = JOptionPane.showConfirmDialog(null, repAreaPanel, "Enter Points", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
//                if(JOptionPane.OK_OPTION == choice) {
//                        System.out.println(repAreaPanel.getUpperLeftCorner());
//                        System.out.println(repAreaPanel.getLowerLeftCorner());
//                        System.out.println(repAreaPanel.getLowerRightCorner());
//                        System.out.println(repAreaPanel.getUpperRightCorner());
//                }
//        }
}
