package edu.ub.bioveg.fisioveg.mosaic.ui.panels;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.RegexDocumentFilter;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 8, 2017
 * 
 */
public class ReplicateClonePanel extends JPanel {

        private static final long serialVersionUID = 6583103161865664077L;
        private static final RegexDocumentFilter DIGIT_DOC_FILTER_10 = new RegexDocumentFilter("\\d", 10);

        private JLabel replicateCountLabel = null;
        private JTextField replicateCountField = null;
        private JLabel replicateSpacingLabel = null;
        private JTextField replicateSpacingField = null;
        private JRadioButton cloneRightRadio = null;
        private JRadioButton cloneLeftRadio = null;
        private ButtonGroup radioButtonGroup = null;

        public ReplicateClonePanel() {
                replicateCountLabel = new JLabel("Count:");
                replicateCountLabel.setHorizontalAlignment(JLabel.RIGHT);
                replicateCountField = new JTextField(5);
                replicateCountField.setText("0");
                ((AbstractDocument) replicateCountField.getDocument()).setDocumentFilter(DIGIT_DOC_FILTER_10);

                replicateSpacingLabel = new JLabel("Spacing:");
                replicateSpacingLabel.setHorizontalAlignment(JLabel.RIGHT);
                replicateSpacingField = new JTextField(5);
                replicateSpacingField.setText("0");
                ((AbstractDocument) replicateSpacingField.getDocument()).setDocumentFilter(DIGIT_DOC_FILTER_10);

                cloneLeftRadio = new JRadioButton("Left");
                cloneRightRadio = new JRadioButton("Right");
                cloneRightRadio.setSelected(true);
                radioButtonGroup = new ButtonGroup();
                radioButtonGroup.add(cloneLeftRadio);
                radioButtonGroup.add(cloneRightRadio);

                setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(replicateCountLabel);
                add(replicateCountField);
                add(replicateSpacingLabel);
                add(replicateSpacingField);
                add(cloneLeftRadio);
                add(cloneRightRadio);
        }

        public final int getReplicateCount() {
                return Integer.parseInt(replicateCountField.getText());
        }

        public final int getReplicateSpacing() {
                return Integer.parseInt(replicateSpacingField.getText());
        }
        
        public final CloneDirection getCloneDirection() {
                if(cloneRightRadio.isSelected()) {
                        return CloneDirection.RIGHT;
                }
                else if(cloneLeftRadio.isSelected()) {
                        return CloneDirection.LEFT;
                }
                else {
                        return CloneDirection.RIGHT;
                }
        }

//        public static void main(String ...strings) {
//                ReplicateClonePanel panel = new ReplicateClonePanel();
//                int choice = JOptionPane.showConfirmDialog(null, panel, "Enter Replicate Properties", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
//                if(JOptionPane.OK_OPTION == choice) {
//                        System.out.println(panel.getReplicateCount());
//                        System.out.println(panel.getReplicateSpacing());
//                        System.out.println(panel.getCloneDirection());
//                }
//        }
}
