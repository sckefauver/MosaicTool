package edu.ub.bioveg.fisioveg.mosaic.ui.panels;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.UITool;
import layout.TableLayout;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 11, 2017
 * 
 */
public class NamingSchemePanel extends JPanel {
        
        private static final long serialVersionUID = -4804654683114755852L;
        
        private JSpinner startSpinner = null;
        private JSpinner plotIncrementSpinner = null;
        private JSpinner repIncrementSpinner = null;
        private JLabel startAtLabel = null;
        private JLabel plotIncrementByLabel = null;
        private JLabel repIncrementByLabel = null;
        private JLabel schemeStyleLabel = null;
        private ButtonGroup buttonGroup = null;
        private JToggleButton serpentineButton = null;
        private JToggleButton zigzagButton = null;
        
        public NamingSchemePanel() {
                startAtLabel = new JLabel("Start at:");
                startAtLabel.setHorizontalAlignment(JLabel.RIGHT);
                plotIncrementByLabel = new JLabel("Increment Plots by:");
                plotIncrementByLabel.setHorizontalAlignment(JLabel.RIGHT);
                repIncrementByLabel = new JLabel("Increment Rep by:");
                repIncrementByLabel.setHorizontalAlignment(JLabel.RIGHT);
                schemeStyleLabel = new JLabel("Scheme Style:");
                schemeStyleLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                startSpinner = new JSpinner(new SpinnerNumberModel(1, 0, 10000000, 1));
                plotIncrementSpinner = new JSpinner(new SpinnerNumberModel(1, 0, 100000, 1));
                repIncrementSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 100000, 100));
                
                serpentineButton = new JToggleButton(UITool.getImageIcon("/edu/ub/bioveg/fisioveg/mosaic/ui/icons/serpentine_22.png"));
                serpentineButton.setOpaque(false);
                serpentineButton.setFocusPainted(false);
                serpentineButton.setToolTipText("Serpentine");
                serpentineButton.setSelected(true);
                zigzagButton = new JToggleButton(UITool.getImageIcon("/edu/ub/bioveg/fisioveg/mosaic/ui/icons/zigzag_22.png"));
                zigzagButton.setOpaque(false);
                zigzagButton.setFocusPainted(false);
                zigzagButton.setToolTipText("ZigZag");
                buttonGroup = new ButtonGroup();
                buttonGroup.add(serpentineButton);
                buttonGroup.add(zigzagButton);
                
                double[][] layout = {
                                {
                                        TableLayout.PREFERRED, 5, TableLayout.PREFERRED, 5, TableLayout.PREFERRED
                                },

                                {
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED,
                                        5,
                                        TableLayout.PREFERRED
                                }
                };
                
                setLayout(new TableLayout(layout));
                setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
                add(startAtLabel,         "0, 0");
                add(startSpinner,         "2, 0, 4");
                add(plotIncrementByLabel, "0, 2");
                add(plotIncrementSpinner, "2, 2, 4");
                add(repIncrementByLabel,  "0, 4");
                add(repIncrementSpinner,  "2, 4, 4");
                add(schemeStyleLabel,     "0, 6");
                add(serpentineButton,     "2, 6");
                add(zigzagButton,         "4, 6");
        }
        
        public final int getStartAt() {
                return ((Integer)startSpinner.getModel().getValue()).intValue();
        }
        
        public final int getPlotIncrementBy() {
                return ((Integer)plotIncrementSpinner.getModel().getValue()).intValue();
        }
        
        public final int getRepIncrementBy() {
                return ((Integer)repIncrementSpinner.getModel().getValue()).intValue();
        }
        
        public final NamingStyle getNamingStyle() {
                if(serpentineButton.isSelected()) {
                        return NamingStyle.SERPENTINE;
                }
                
                return NamingStyle.ZIGZAG;
        }
        
        public static void main(String ...strings) {
                NamingSchemePanel panel = new NamingSchemePanel();
                int choice = JOptionPane.showConfirmDialog(null, panel, "Specify Naming Scheme", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if(JOptionPane.OK_OPTION == choice) {
                        System.out.println(panel.getStartAt());
                        System.out.println(panel.getPlotIncrementBy());
                        System.out.println(panel.getRepIncrementBy());
                        System.out.println(panel.getNamingStyle().toString());
                }
        }
}
