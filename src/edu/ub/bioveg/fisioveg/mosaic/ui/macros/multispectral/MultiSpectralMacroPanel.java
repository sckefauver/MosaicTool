package edu.ub.bioveg.fisioveg.mosaic.ui.macros.multispectral;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import edu.ub.bioveg.fisioveg.mosaic.tools.exporters.CsvExporter;
import edu.ub.bioveg.fisioveg.mosaic.tools.exporters.ExcelExporter;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileOpen;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileSave;
import edu.ub.bioveg.fisioveg.mosaic.ui.MosaicFrame;
import ij.IJ;
import layout.TableLayout;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Dec 4, 2016
 */
public class MultiSpectralMacroPanel extends JPanel {
        
        private static final long serialVersionUID = -250113643620001119L;
        
        private JLabel batchInputLabel = null;
        private JTextField batchInputField = null;
        private JButton batchInputButton = null;
        
        private JLabel saveExcelLabel = null;
        private JCheckBox saveExcelCheckBox = null;
        
        private JLabel saveCsvLabel = null;
        private JCheckBox saveCsvCheckBox = null;
        
        private JLabel csvDelimiterLabel = null;
        private JComboBox<String> csvDelimiterList = null;
        
        private JLabel resultsFileLabel = null;
        private JTextField resultsFileField = null;
        private JButton resultsFileButton = null;
        
        private JPanel optionsPanel = null;
        
        private JPanel buttonPanel = null;
        private JButton runButton = null;
        private JProgressBar progressBar = null;
        
        private String recentDir = null;
        private File batchInputDir = null;
        private File saveResultsFile = null;
        
        private MultiSpectralMacroVars macroVars = null;
        
        public MultiSpectralMacroPanel() {
                macroVars = new MultiSpectralMacroVars();
                
                batchInputLabel = new JLabel("Batch Inputs:");
                batchInputLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                batchInputField = new JTextField(20);
                batchInputField.setEditable(false);
                batchInputField.setBackground(Color.WHITE);
                
                batchInputButton = new JButton("...");
                batchInputButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                batchInputButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                saveExcelLabel = new JLabel("Save as Excel?");
                saveExcelLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                saveExcelCheckBox = new JCheckBox("", false);
                saveExcelCheckBox.setFocusable(false);
                saveExcelCheckBox.setSelected(false);
                saveExcelCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveExcelCheckBox_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                saveCsvLabel = new JLabel("Save as CSV?");
                saveExcelLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                saveCsvCheckBox = new JCheckBox("", false);
                saveCsvCheckBox.setFocusable(false);
                saveCsvCheckBox.setSelected(false);
                saveCsvCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveCsvCheckBox_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                csvDelimiterLabel = new JLabel("CSV Delimiter");
                csvDelimiterLabel.setHorizontalAlignment(JLabel.RIGHT);
                csvDelimiterList = new JComboBox<>(new String[] {"Comma", "Space", "Tab", "Pipe", "Semi-Colon"});
                
                //----------------------------------------------------------------
                
                resultsFileLabel = new JLabel("Results File:");
                resultsFileLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                resultsFileField = new JTextField(20);
                resultsFileField.setEditable(false);
                resultsFileField.setBackground(Color.WHITE);
                
                resultsFileButton = new JButton("...");
                resultsFileButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                resultsFileButton_actionPerformed();  
                        }
                });
                
                //----------------------------------------------------------------
                
                double spacer = 5;
                double[][] layoutSize = {
                                //                   0,                             2,                             4,                        6,                             8
                                {TableLayout.PREFERRED, spacer, TableLayout.PREFERRED, spacer, TableLayout.PREFERRED, spacer, TableLayout.FILL, spacer, TableLayout.PREFERRED},
                                {TableLayout.PREFERRED, //0
                                 spacer,
                                 TableLayout.PREFERRED, //2
                                 spacer,
                                 TableLayout.PREFERRED, //4
                                 spacer,
                                 TableLayout.PREFERRED, //6
                                 spacer,
                                 TableLayout.PREFERRED  //8
                                }
                };
                
                optionsPanel = new JPanel();
                optionsPanel.setBorder(BorderFactory.createTitledBorder("Macro Options"));
                optionsPanel.setLayout(new TableLayout(layoutSize));
                optionsPanel.add(batchInputLabel,     "0, 0");
                optionsPanel.add(batchInputField,     "2, 0, 6");
                optionsPanel.add(batchInputButton,    "8, 0");
                
                optionsPanel.add(resultsFileLabel,    "0, 2");
                optionsPanel.add(resultsFileField,    "2, 2, 6");
                optionsPanel.add(resultsFileButton,   "8, 2");
                
                optionsPanel.add(saveExcelLabel,      "0, 4");
                optionsPanel.add(saveExcelCheckBox,   "2, 4");
                
                optionsPanel.add(saveCsvLabel,        "0, 6");
                optionsPanel.add(saveCsvCheckBox,     "2, 6");
                optionsPanel.add(csvDelimiterList,    "4, 6");
                
                
                //----------------------------------------------------------------
                
                runButton = new JButton("Run Macro");
                runButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                runButton_actionPerformed();
                        }
                });
                
                progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
                progressBar.setStringPainted(true);
                progressBar.setString("Ready");
                progressBar.setIndeterminate(false);
                
                buttonPanel = new JPanel(new BorderLayout(5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(runButton, BorderLayout.EAST);
                buttonPanel.add(progressBar, BorderLayout.CENTER);
                
                // ---------------------------------------------------
                
                RTextScrollPane textScrollPane = createMacroPanel("Tetracam Macro", "Tetracam_Macro_v4.ijm");
                
                setLayout(new BorderLayout(5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(optionsPanel, BorderLayout.NORTH);
                add(textScrollPane, BorderLayout.CENTER);
                add(buttonPanel, BorderLayout.SOUTH);
        }
        
        private final void saveExcelCheckBox_actionPerformed() {
                if(saveExcelCheckBox.isSelected()) {
                        if(saveResultsFile != null) {
                                saveExcelCheckBox.setText(saveResultsFile.getName()+".xlsx");
                        }
                        else {
                                saveExcelCheckBox.setText("");
                        }
                }
                else {
                        saveExcelCheckBox.setText("");
                }
        }
        
        private final void saveCsvCheckBox_actionPerformed() {
                if(saveCsvCheckBox.isSelected()) {
                        if(saveResultsFile != null) {
                                saveCsvCheckBox.setText(saveResultsFile.getName()+".csv");
                        }
                        else {
                                saveCsvCheckBox.setText("");
                        }
                }
                else {
                        saveCsvCheckBox.setText("");
                }
        }
        
        private final void runButton_actionPerformed() {
                if(MosaicFrame.isMacroRunning()) {
                        JOptionPane.showMessageDialog(this, "A macro is already running, please wait until it finishes.", "Macro Already Running", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(batchInputDir == null) {
                        JOptionPane.showMessageDialog(this, "Please select the batch inputs folder.", "Select Batch Inputs", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(saveResultsFile == null) {
                        JOptionPane.showMessageDialog(this, "Please select a results file location.", "Select Results File", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(!saveExcelCheckBox.isSelected() && !saveCsvCheckBox.isSelected()) {
                        JOptionPane.showMessageDialog(this, "Please select a results format type (excel and/or csv).", "Select Results Format", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                Thread macroThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                runButton.setEnabled(false);
                                                progressBar.setIndeterminate(true);
                                                progressBar.setString("Running Macro ...");
                                                MosaicFrame.setMacroRunning(true);
                                        }
                                });
                                
                                String macroCode = macroVars.getMacroCode();
                                IJ.runMacro(macroCode);
                                Map<String,ResultPojo> pojoMap = reformatResultsFile();
                                if(pojoMap != null) {
                                        if(saveCsvCheckBox.isSelected()) {
                                                EventQueue.invokeLater(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                                progressBar.setString("Exporting to CSV ...");
                                                        }
                                                });
                                                
                                                String delimiter = (String)csvDelimiterList.getSelectedItem();
                                                File savedCsvFile = CsvExporter.ExportTetracamResults(pojoMap, saveResultsFile, delimiter);
                                                if(savedCsvFile != null) {
                                                        JOptionPane.showMessageDialog(MultiSpectralMacroPanel.this, "CSV file " + savedCsvFile + " saved", "CSV Saved", JOptionPane.INFORMATION_MESSAGE);
                                                }
                                                else {
                                                        IJ.error("CSV Export Error", "There was an error exporting the results to CSV file \""+saveResultsFile.getName()+"\".csv");
                                                }
                                        }
                                        
                                        if(saveExcelCheckBox.isSelected()) {
                                                EventQueue.invokeLater(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                                progressBar.setString("Exporting to Excel ...");
                                                        }
                                                });
                                                
                                                File savedExcelFile = ExcelExporter.ExportTetracamResults(pojoMap, saveResultsFile);
                                                if(savedExcelFile != null) {
                                                        JOptionPane.showMessageDialog(MultiSpectralMacroPanel.this, "Excel file " + savedExcelFile + " saved", "Excel Saved", JOptionPane.INFORMATION_MESSAGE);
                                                }
                                                else {
                                                        IJ.error("Excel Export Error", "There was an error exporting the results to Excel file \""+saveResultsFile.getName()+"\".xlsx");
                                                }
                                        }
                                }
                                else {
                                        IJ.error("Error Formating Results File", "There was an error while reformatting the results file \""+saveResultsFile.getName()+"\"");
                                }
                                
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                progressBar.setIndeterminate(false);
                                                progressBar.setString("Ready");
                                                runButton.setEnabled(true);
                                                MosaicFrame.setMacroRunning(false);
                                        }
                                });
                        }
                });
                
                macroThread.start();
        }
        
        private final RTextScrollPane createMacroPanel(String panelName, String macroTemplateName) {
                InputStream macroInputStream = MultiSpectralMacroPanel.class.getResourceAsStream("/edu/ub/bioveg/fisioveg/mosaic/ui/macros/multispectral/"+macroTemplateName);
                RTextScrollPane syntaxScrollPane = null;
                if(macroInputStream != null) {
                        RSyntaxTextArea syntaxTextArea = new RSyntaxTextArea(20, 60);
                        syntaxTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
                        syntaxTextArea.setCodeFoldingEnabled(true);
                        
                        String line = null;
                        BufferedReader br = null;
                        StringBuilder sb = null;
                        
                        try {
                                sb = new StringBuilder();
                                br = new BufferedReader(new InputStreamReader(macroInputStream));
                                while((line = br.readLine()) != null) {
                                        sb.append(line).append('\n');
                                }
                                
                                syntaxTextArea.setText(sb.toString());
                                syntaxTextArea.setEditable(false);
                                syntaxScrollPane = new RTextScrollPane(syntaxTextArea);
                                
                                macroVars.setMacroNameKey(panelName);
                                macroVars.setMacroName(panelName);
                                macroVars.setSyntaxTextArea(syntaxTextArea);
                                
                                syntaxTextArea.setCaretPosition(0);
                        }
                        catch(IOException ioe) {
                                ioe.printStackTrace();
                                IJ.error("I/O Error", "There was an error while reading the tetracam macro file.");
                        }
                        finally {
                                if(br != null) {
                                        try {
                                                br.close();
                                        }
                                        catch (IOException e) {}
                                        
                                        br = null;
                                }
                                
                                sb = null;
                                line = null;
                                macroInputStream = null;
                        }
                }
                else {
                        IJ.error("File Not Found", "Could not find the tetracam macro \""+macroTemplateName+"\"");
                }
                
                return syntaxScrollPane;
        }
        
        private final void batchInputButton_actionPerformed() {
                batchInputDir = FileOpen.getFile("Select batch input folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY, "Batch Image Folder", (String[]) null);
                if (batchInputDir != null) {
                        recentDir = batchInputDir.getAbsolutePath();
                        batchInputField.setText(batchInputDir.getAbsolutePath());
                        macroVars.setBatchInputVar(batchInputDir.getAbsolutePath());
                }
        }
        
        private final void resultsFileButton_actionPerformed() {
                saveResultsFile = FileSave.saveFile("Name Results File", (recentDir == null ? new File(System.getProperty("user.dir")) : new File(recentDir)), "Results File", "multispectral_results");
                if(saveResultsFile != null) {
                        recentDir = saveResultsFile.getParentFile().getAbsolutePath();
                        resultsFileField.setText(saveResultsFile.getAbsolutePath());
                        macroVars.setSaveResultsFile(saveResultsFile.getAbsolutePath());
                }
        }
        
        private final Map<String, ResultPojo> reformatResultsFile() {
                TreeMap<String, ResultPojo> pojoMap = null;
                BufferedReader br = null;
                
                try {
                        pojoMap = new TreeMap<String, ResultPojo>();
                        br = new BufferedReader(new FileReader(saveResultsFile));
                        String line1 = null;
                        br.readLine(); //Skip the header
                        String type = null;
                        String filename = null;
                        
                        while(true) {
                                line1 = br.readLine();
                                
                                if(line1 == null) {
                                        break;
                                }
                                
                                String[] arrLine1 = line1.split("\t");
                                type = arrLine1[3];
                                filename = arrLine1[4];
                                
                                if(!pojoMap.containsKey(filename)) {
                                        pojoMap.put(filename, new ResultPojo(filename));
                                }
                                
                                switch(type) {
                                        case "filename": {
                                                pojoMap.get(filename).setFilename(arrLine1[4]);
                                                break;
                                        }
                                        
                                        case "Vegetation_area": {
                                                pojoMap.get(filename).setVegetation_area(arrLine1[2]);
                                                break;
                                        }
                                        
                                        case "R450": {
                                                pojoMap.get(filename).setR450(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R550": {
                                                pojoMap.get(filename).setR550(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R570": {
                                                pojoMap.get(filename).setR570(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R670": {
                                                pojoMap.get(filename).setR670(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R700": {
                                                pojoMap.get(filename).setR700(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R720": {
                                                pojoMap.get(filename).setR720(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R780": {
                                                pojoMap.get(filename).setR780(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R840": {
                                                pojoMap.get(filename).setR840(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R860": {
                                                pojoMap.get(filename).setR860(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R900": {
                                                pojoMap.get(filename).setR900(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R950": {
                                                pojoMap.get(filename).setR950(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R1000": {
                                                pojoMap.get(filename).setR1000(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R450_veg": {
                                                pojoMap.get(filename).setR450_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R550_veg": {
                                                pojoMap.get(filename).setR550_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R570_veg": {
                                                pojoMap.get(filename).setR570_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R670_veg": {
                                                pojoMap.get(filename).setR670_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R700_veg": {
                                                pojoMap.get(filename).setR700_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R720_veg": {
                                                pojoMap.get(filename).setR720_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R780_veg": {
                                                pojoMap.get(filename).setR780_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R840_veg": {
                                                pojoMap.get(filename).setR840_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R860_veg": {
                                                pojoMap.get(filename).setR860_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R900_veg": {
                                                pojoMap.get(filename).setR900_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R950_veg": {
                                                pojoMap.get(filename).setR950_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "R1000_veg": {
                                                pojoMap.get(filename).setR1000_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "NDVI_plot": {
                                                pojoMap.get(filename).setNDVI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "PRI_plot": {
                                                pojoMap.get(filename).setPRI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "SAVI_plot": {
                                                pojoMap.get(filename).setSAVI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "MCARI_plot": {
                                                pojoMap.get(filename).setMCARI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "WBI_plot": {
                                                pojoMap.get(filename).setWBI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "RDVI_plot": {
                                                pojoMap.get(filename).setRDVI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "EVI_plot": {
                                                pojoMap.get(filename).setEVI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "ARI2_plot": {
                                                pojoMap.get(filename).setARI2_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "CRI2_plot": {
                                                pojoMap.get(filename).setCRI2_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "CCI_plot": {
                                                pojoMap.get(filename).setCCI_plot(arrLine1[1]);
                                                break;
                                        }
                                        
                                        case "TCARI_plot": {
                                                pojoMap.get(filename).setTCARI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "OSAVI_plot": {
                                                pojoMap.get(filename).setOSAVI_plot(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "TCARIOSAVI_plot": {
                                                pojoMap.get(filename).setTCARIOSAVI_plot(arrLine1[2]);
                                                break; 
                                        }
                                        
                                        case "NDVI_veg": {
                                                pojoMap.get(filename).setNDVI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "PRI_veg": {
                                                pojoMap.get(filename).setPRI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "SAVI_veg": {
                                                pojoMap.get(filename).setSAVI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "MCARI_veg": {
                                                pojoMap.get(filename).setMCARI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "WBI_veg": {
                                                pojoMap.get(filename).setWBI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "RDVI_veg": {
                                                pojoMap.get(filename).setRDVI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "CCI_veg": {
                                                pojoMap.get(filename).setCCI_veg(arrLine1[1]);
                                                break;
                                        }
                                        
                                        case "EVI_veg": {
                                                pojoMap.get(filename).setEVI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "ARI2_veg": {
                                                pojoMap.get(filename).setARI2_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "CRI2_veg": {
                                                pojoMap.get(filename).setCRI2_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "TCARI_veg": {
                                                pojoMap.get(filename).setTCARI_veg(arrLine1[1]);
                                                break; 
                                        }

                                        case "OSAVI_veg": {
                                                pojoMap.get(filename).setOSAVI_veg(arrLine1[1]);
                                                break; 
                                        }
                                        
                                        case "TCARIOSAVI_veg": {
                                                pojoMap.get(filename).setTCARIOSAVI_veg(arrLine1[2]);
                                                break; 
                                        }
                                        
                                        default: {
                                                IJ.log("Unknown results column type: "+type);
                                                break;
                                        }
                                }
                        }
                        
                        br.close();
                }
                catch (IOException e) {
                        e.printStackTrace();
                }
                finally {
                        if(br != null) {
                                try {
                                        br.close();
                                }
                                catch(IOException e) {}
                                br = null;
                        }
                }
                
                return pojoMap;
        }
}

