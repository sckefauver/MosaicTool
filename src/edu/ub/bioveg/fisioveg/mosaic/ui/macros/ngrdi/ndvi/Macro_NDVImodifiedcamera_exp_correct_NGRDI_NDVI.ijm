//Macro to batch process calculate the NGRDI and NDVI "NIR-R-G" Infrared modified camera multispectral indexes.

var input = "$P{batch_input}";
var ngdriDir = "$P{save_ngrdi_dir}";
var ndviDir = "$P{save_ndvi_dir}";
var saveNgrdi = $P{save_ngrdi_images};
var saveNdvi = $P{save_ndvi_images};
var saveCorrectionFactor = $P{save_correction_factor};
var resultsFile = "$P{save_results_file}";
var list = getFileList(input);

setBatchMode(true);
run("Clear Results");

var fileCount = getImageFileCount(list);
var filenames = newArray(fileCount);
var ngrdis = newArray(fileCount);
var ndvis = newArray(fileCount);
var n = 0;

for (i = 0; i < list.length; i++) {
    if(isImage(list[i])) {
        action(input, list[i]);
    }
}

printResults();

call("java.lang.System.gc");
setBatchMode(false);

function action(input, filename) {
    open(input + filename);
    original = getImageID;
    title = getTitle; 
    imageName = File.nameWithoutExtension;
    
    //Calculate the Normalized Green Red Difference Index (NGRDI) and Normalized Difference Vegetation Index (NDVI)
    //The NGRDI vegetation index from RGB components based on Hunt et al. 2014 and NDVI from Tucker, 1979. 
    
    //Perform RGB threshold classification and make masks
    selectWindow(filename);
    
    //Convert to separate red, green and blue images for processing and applying the correction equations
    run("RGB Stack");
    run("Stack to Images");
    selectWindow("Blue");
    rename("NIR");
    if(saveCorrectionFactor) {
	    run("Macro...", "code=v=11.641*exp(0.015*v)");
	    selectWindow("Red");
	    run("Macro...", "code=v=0.0021*exp(0.0459*v)");
	    selectWindow("Green");
	    run("Macro...", "code=v=10.532*exp(0.0172*v)");
    }
    
    //Use image math calculator plus functions to calculate the NGRDI
    imageCalculator("Add create 32-bit", "Green","Red");
    rename("ResultG+R");
    imageCalculator("Subtract create 32-bit", "Green","Red");
    rename("ResultG-R");
    imageCalculator("Divide create 32-bit", "ResultG-R","ResultG+R");
    rename("NGRDI");
    selectWindow("NGRDI");
    
    //Calculate the mean NGRDI value for the image
    selectWindow("NGRDI");
    run("Set Measurements...", "mean redirect=None decimal=4");
    run("Measure");
    NGRDI = getResult("Mean");
    
    //Use image math calculator plus functions to calculate the NDVI
    imageCalculator("Add create 32-bit", "NIR","Red");
    rename("ResultNIR+R");
    imageCalculator("Subtract create 32-bit", "NIR","Red");
    rename("ResultNIR-R");
    imageCalculator("Divide create 32-bit", "ResultNIR-R","ResultNIR+R");
    rename("NDVI");
    
    //Calculate the mean NDVI value for the image
    selectWindow("NDVI");
    run("Set Measurements...", "mean redirect=None decimal=4");
    run("Measure");
    NDVI = getResult("Mean");

	if(saveNgrdi) {
		selectWindow("NGRDI");
		saveAs("Tiff", ngdriDir + imageName + "_NGRDI");
    }
	
	if(saveNdvi) {
		selectWindow("NDVI");
		saveAs("Tiff", ndviDir + imageName + "_NDVI");
	}
    
    //Clean up after each file
    run("Close All");
    run("Clear Results");
    
    //Consolidate data
    filenames[n] = filename;
    ngrdis[n] = NGRDI;
    ndvis[n] = NDVI;
    n++;
}

function printResults() {
    run("Clear Results");
    for (i = 0; i < fileCount; i++) {
        setResult("File Name", i, filenames[i]);
        setResult("NGRDI", i, ngrdis[i]);
        setResult("NDVI", i, ndvis[i]);
    }

    setOption("ShowRowNumbers", false);
    updateResults();
    selectWindow("Results");
    saveAs("Results", resultsFile);
}

function getImageFileCount(list) {
    count = 0;
    for (i = 0; i < list.length; i++) {
        if(isImage(list[i])) {
            count++;
        }
    }
    return count;
}

function isImage(filename) {
    if(endsWith(toLowerCase(filename), ".tif")||
       endsWith(toLowerCase(filename), ".tiff") ||
       endsWith(toLowerCase(filename), ".jpg") ||
       endsWith(toLowerCase(filename), ".jpeg")) {
       return true
    }
    else {
        return false
    }
};