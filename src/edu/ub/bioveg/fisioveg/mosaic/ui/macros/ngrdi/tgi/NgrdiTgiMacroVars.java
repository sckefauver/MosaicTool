package edu.ub.bioveg.fisioveg.mosaic.ui.macros.ngrdi.tgi;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import edu.ub.bioveg.fisioveg.mosaic.ui.macros.MacroVars;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Feb 18, 2017
 */
public class NgrdiTgiMacroVars extends MacroVars {

        private String batchInputPrev = null;
        private String ngrdiDirPrev = null;
        private String tgiDirPrev = null;
        private String saveNgrdimagesPrev = null;
        private String saveTgiImagesPrev = null;
        private String saveResultsFilePrev = null;
        
        public NgrdiTgiMacroVars() {
                
        }
        
        public final void setBatchInputVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{batch_input}") != -1) {
                        batchInputPrev = "$P{batch_input}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                variableName = variableName + "\\\\";
                
                String newTxt = macroTxt.replace(batchInputPrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                batchInputPrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveNgrdiImagesVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_ngrdi_images}") != -1) {
                        saveNgrdimagesPrev = "saveNgrdi = $P{save_ngrdi_images}";
                }
                
                String newTxt = macroTxt.replace(saveNgrdimagesPrev, "saveNgrdi = "+variableName);
                syntaxTextArea.setText(newTxt);
                
                saveNgrdimagesPrev = new String("saveNgrdi = "+variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveTgiImagesVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_tgi_images}") != -1) {
                        saveTgiImagesPrev = "saveTgi = $P{save_tgi_images}";
                }
                
                String newTxt = macroTxt.replace(saveTgiImagesPrev, "saveTgi = "+variableName);
                syntaxTextArea.setText(newTxt);
                
                saveTgiImagesPrev = new String("saveTgi = "+variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveNgrdiDirVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_ngrdi_dir}") != -1) {
                        ngrdiDirPrev = "$P{save_ngrdi_dir}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                variableName = variableName + "\\\\";
                
                String newTxt = macroTxt.replace(ngrdiDirPrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                ngrdiDirPrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveTgiDirVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_tgi_dir}") != -1) {
                        tgiDirPrev = "$P{save_tgi_dir}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                variableName = variableName + "\\\\";
                
                String newTxt = macroTxt.replace(tgiDirPrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                tgiDirPrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveResultsFile(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_results_file}") != -1) {
                        saveResultsFilePrev = "$P{save_results_file}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                
                String newTxt = macroTxt.replace(saveResultsFilePrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                saveResultsFilePrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
}
