// Macro to batch process MosaicTool output plot level images cut
// from the orthomosaic of any multispectral camera from 1-6 bands

var inputDir = "$P{batch_input}";
var resultsFile = "$P{save_results_file}";
var list = getFileList(inputDir);

setBatchMode(true);
run("Clear Results");

var fileCount = getImageFileCount(list);
var filenames = newArray(fileCount);
var c1s = newArray(fileCount);
var c2s = newArray(fileCount);
var c3s = newArray(fileCount);
var c4s = newArray(fileCount);
var c5s = newArray(fileCount);
var c6s = newArray(fileCount);
var n = 0;

for (i = 0; i < list.length; i++) {
    if(isImage(list[i])) {
        action(inputDir, list[i]);
    }
}

printResults();

call("java.lang.System.gc");
setBatchMode(false);

function action(inputDir, filename) {
    open(inputDir + filename);
    original = getImageID;
	imageId = original;
    title = getTitle;
    imageName = File.nameWithoutExtension;
	bit = bitDepth();
	
	/*
	 Split channels for processing and rename files
	*/
	
	selectImage(imageId);
	channels = nSlices;     //Should be between 1 and 6
	run("Stack to Images");
	
	for(j = 0; j < channels; j++) {
		imageId -= 1;
		selectImage(imageId);
		run("32-bit");
		//adjust bit depth of each band depending on original bit rate of image
		if(bit == 8) { 	
			run("Divide...", "value=256 stack");
		}
		
		if(bit == 16) {
			run("Divide...", "value=65536 stack");;
		}
		
		rename("C"+(j+1));
	};
	
	//Use image math calculator plus functions to calculate the channels mean of the whole plot

	//C1
	selectWindow("C1");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
	run("Measure");
	c1 = getResult("Mean");

	//C2
	if(channels >= 2) {
		selectWindow("C2");
		run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
		run("Measure");
		c2 = getResult("Mean");
	}
	else {
			c2 = "NAN";
	}
	
	//C3
	if(channels >= 3) {
		selectWindow("C3");
		run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
		run("Measure");
		c3 = getResult("Mean");
	}
	else {
		c3 = "NAN";
	}
	
	//C4
	if(channels >= 4) {
		selectWindow("C4");
		run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
		run("Measure");
		c4 = getResult("Mean");
	}
	else {
		c4 = "NAN";
	}
	
	//C5
	if(channels >= 5) {
		selectWindow("C5");
		run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
		run("Measure");
		c5 = getResult("Mean");
	}
	else {
		c5 = "NAN";
	}
	
	//C6
	if(channels >= 6) {
		selectWindow("C6");
		run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
		run("Measure");
		c6 = getResult("Mean");
	}
	else {
		c6 = "NAN";
	}

    //Clean up after each file
    run("Close All");
    run("Clear Results");
	
	//Consolidate data
    filenames[n] = filename;
    c1s[n] = c1;
    c2s[n] = c2;
    c3s[n] = c3;
    c4s[n] = c4;
    c5s[n] = c5;
	c6s[n] = c6;
    n++;
}

function printResults() {
    run("Clear Results");
    for (i = 0; i < fileCount; i++) {
        setResult("filename", i, filenames[i]);
        setResult("c1", i, c1s[i]);
        setResult("c2", i, c2s[i]);
        setResult("c3", i, c3s[i]);
        setResult("c4", i, c4s[i]);
        setResult("c5", i, c5s[i]);
		setResult("c6", i, c6s[i]);
    }

    setOption("ShowRowNumbers", false);
    updateResults();
    selectWindow("Results");
    saveAs("Results", resultsFile);
    run("Close"); 
}

function getImageFileCount(list) {
    count = 0;
    for (i = 0; i < list.length; i++) {
        if(isImage(list[i])) {
            count++;
        }
    }
    return count;
}

function isImage(filename) {
	if(endsWith(toLowerCase(filename), ".tif")||
	   endsWith(toLowerCase(filename), ".tiff") ||
	   endsWith(toLowerCase(filename), ".jpg") ||
	   endsWith(toLowerCase(filename), ".jpeg")) {
	   return true;
	}
	else {
		return false;
	}
}
