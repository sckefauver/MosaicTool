package edu.ub.bioveg.fisioveg.mosaic.ui.macros.thermal;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import edu.ub.bioveg.fisioveg.mosaic.ui.macros.MacroVars;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Jun 29, 2017
 */
public class ThermalImageMacroVars extends MacroVars {

        private String batchInputPrev = null;
        private String outputDirPrev = null;
        private String saveImagesPrev = null;
        private String removeDarkLinePrev = null;
        private String kelvin16bitFormatPrev = null;
        private String threeBandsPrev = null;
        private String saveResultsFilePrev = null;
        
        public ThermalImageMacroVars() {
                
        }
        
        public final void setBatchInputVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{batch_input}") != -1) {
                        batchInputPrev = "$P{batch_input}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                variableName = variableName + "\\\\";
                
                String newTxt = macroTxt.replace(batchInputPrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                batchInputPrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveImagesVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_images}") != -1) {
                        saveImagesPrev = "saveImages = $P{save_images}";
                }
                
                String newTxt = macroTxt.replace(saveImagesPrev, "saveImages = "+variableName);
                syntaxTextArea.setText(newTxt);
                
                saveImagesPrev = new String("saveImages = "+variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setRemoveDarkLineVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{remove_dark_line}") != -1) {
                        removeDarkLinePrev = "removeDarkLine = $P{remove_dark_line}";
                }
                
                String newTxt = macroTxt.replace(removeDarkLinePrev, "removeDarkLine = "+variableName);
                syntaxTextArea.setText(newTxt);
                
                removeDarkLinePrev = new String("removeDarkLine = "+variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setKelvin16BitFormatVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{kelvin_16bit_format}") != -1) {
                        kelvin16bitFormatPrev = "kelvin16bitFormat = $P{kelvin_16bit_format}";
                }
                
                String newTxt = macroTxt.replace(kelvin16bitFormatPrev, "kelvin16bitFormat = "+variableName);
                syntaxTextArea.setText(newTxt);
                
                kelvin16bitFormatPrev = new String("kelvin16bitFormat = "+variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setThreeBandsVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{three_bands}") != -1) {
                        threeBandsPrev = "threeBands = $P{three_bands}";
                }
                
                String newTxt = macroTxt.replace(threeBandsPrev, "threeBands = "+variableName);
                syntaxTextArea.setText(newTxt);
                
                threeBandsPrev = new String("threeBands = "+variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveOutputDirVar(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_output_dir}") != -1) {
                        outputDirPrev = "$P{save_output_dir}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                variableName = variableName + "\\\\";
                
                String newTxt = macroTxt.replace(outputDirPrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                outputDirPrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
        
        public final void setSaveResultsFile(String variableName) {
                String macroTxt = syntaxTextArea.getText();
                
                if(macroTxt.indexOf("$P{save_results_file}") != -1) {
                        saveResultsFilePrev = "$P{save_results_file}";
                }
                
                variableName = variableName.replaceAll("\\\\", "\\\\\\\\");
                
                String newTxt = macroTxt.replace(saveResultsFilePrev, variableName);
                syntaxTextArea.setText(newTxt);
                
                saveResultsFilePrev = new String(variableName);
                syntaxTextArea.setCaretPosition(0);
        }
}
