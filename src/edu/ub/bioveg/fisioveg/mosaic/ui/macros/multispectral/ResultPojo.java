package edu.ub.bioveg.fisioveg.mosaic.ui.macros.multispectral;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Dec 11, 2016
 */
public class ResultPojo {

        private String filename = null;
        private String Vegetation_area = null;
        private String R450 = null;
        private String R550 = null;
        private String R570 = null;
        private String R670 = null;
        private String R700 = null;
        private String R720 = null;
        private String R780 = null;
        private String R780_veg = null;
        private String R840 = null;
        private String R860 = null;
        private String R900 = null;
        private String R950 = null;
        private String R1000  = null;
        private String R450_veg = null;
        private String R550_veg = null;
        private String R570_veg = null;
        private String R670_veg = null;
        private String R700_veg = null;
        private String R720_veg = null;
        private String R840_veg = null;
        private String R860_veg = null;
        private String R900_veg = null;
        private String R950_veg = null;
        private String R1000_veg  = null;
        private String NDVI_plot = null;
        private String PRI_plot = null;
        private String SAVI_plot = null;
        private String MCARI_plot = null;
        private String WBI_plot = null;
        private String RDVI_plot = null;
        private String EVI_plot = null;
        private String ARI2_plot = null;
        private String CRI2_plot = null;
        private String TCARI_plot = null;
        private String OSAVI_plot = null;
        private String TCARIOSAVI_plot = null;
        private String CCI_plot = null;
        private String NDVI_veg = null;
        private String PRI_veg = null;
        private String SAVI_veg = null;
        private String MCARI_veg = null;
        private String WBI_veg = null;
        private String RDVI_veg = null;
        private String EVI_veg = null;
        private String ARI2_veg = null;
        private String CRI2_veg = null;
        private String TCARI_veg = null;
        private String OSAVI_veg = null;
        private String TCARIOSAVI_veg = null;
        private String CCI_veg = null;
        
        public ResultPojo() {
                
        }
        
        public ResultPojo(String filename) {
                this.filename = filename;
        }
        
        public final String getFilename() {
                return filename;
        }
        
        public final void setFilename(String filename) {
                this.filename = filename;
        }
        
        public final String getVegetation_area() {
                return Vegetation_area;
        }
        
        public final void setVegetation_area(String vegetation_area) {
                Vegetation_area = vegetation_area;
        }
        
        public final String getR450() {
                return R450;
        }
        
        public final void setR450(String r450) {
                R450 = r450;
        }
        
        public final String getR550() {
                return R550;
        }
        
        public final void setR550(String r550) {
                R550 = r550;
        }
        
        public final String getR570() {
                return R570;
        }
        
        public final void setR570(String r570) {
                R570 = r570;
        }
        
        public final String getR670() {
                return R670;
        }
        
        public final void setR670(String r670) {
                R670 = r670;
        }
        
        public final String getR700() {
                return R700;
        }
        
        public final void setR700(String r700) {
                R700 = r700;
        }
        
        public final String getR720() {
                return R720;
        }
        
        public final void setR720(String r720) {
                R720 = r720;
        }
        
        public final String getR780() {
                return R780;
        }
        
        public final void setR780(String r780) {
                R780 = r780;
        }
        
        public final String getR780_veg() {
                return R780_veg;
        }
        
        public final void setR780_veg(String r780_veg) {
                R780_veg = r780_veg;
        }
        
        public final String getR840() {
                return R840;
        }
        
        public final void setR840(String r840) {
                R840 = r840;
        }
        
        public final String getR860() {
                return R860;
        }
        
        public final void setR860(String r860) {
                R860 = r860;
        }
        
        public final String getR900() {
                return R900;
        }
        
        public final void setR900(String r900) {
                R900 = r900;
        }
        
        public final String getR950() {
                return R950;
        }
        
        public final void setR950(String r950) {
                R950 = r950;
        }
        
        public final String getR1000() {
                return R1000;
        }
        
        public final void setR1000(String r1000) {
                R1000 = r1000;
        }
        
        public final String getR450_veg() {
                return R450_veg;
        }
        
        public final void setR450_veg(String r450_veg) {
                R450_veg = r450_veg;
        }
        
        public final String getR550_veg() {
                return R550_veg;
        }
        
        public final void setR550_veg(String r550_veg) {
                R550_veg = r550_veg;
        }
        
        public final String getR570_veg() {
                return R570_veg;
        }
        
        public final void setR570_veg(String r570_veg) {
                R570_veg = r570_veg;
        }
        
        public final String getR670_veg() {
                return R670_veg;
        }
        
        public final void setR670_veg(String r670_veg) {
                R670_veg = r670_veg;
        }
        
        public final String getR700_veg() {
                return R700_veg;
        }
        
        public final void setR700_veg(String r700_veg) {
                R700_veg = r700_veg;
        }
        
        public final String getR720_veg() {
                return R720_veg;
        }
        
        public final void setR720_veg(String r720_veg) {
                R720_veg = r720_veg;
        }
        
        public final String getR840_veg() {
                return R840_veg;
        }
        
        public final void setR840_veg(String r840_veg) {
                R840_veg = r840_veg;
        }
        
        public final String getR860_veg() {
                return R860_veg;
        }
        
        public final void setR860_veg(String r860_veg) {
                R860_veg = r860_veg;
        }
        
        public final String getR900_veg() {
                return R900_veg;
        }
        
        public final void setR900_veg(String r900_veg) {
                R900_veg = r900_veg;
        }
        
        public final String getR950_veg() {
                return R950_veg;
        }
        
        public final void setR950_veg(String r950_veg) {
                R950_veg = r950_veg;
        }
        
        public final String getR1000_veg() {
                return R1000_veg;
        }
        
        public final void setR1000_veg(String r1000_veg) {
                R1000_veg = r1000_veg;
        }
        
        public final String getNDVI_plot() {
                return NDVI_plot;
        }
        
        public final void setNDVI_plot(String nDVI_plot) {
                NDVI_plot = nDVI_plot;
        }
        
        public final String getPRI_plot() {
                return PRI_plot;
        }
        
        public final void setPRI_plot(String pRI_plot) {
                PRI_plot = pRI_plot;
        }
        
        public final String getSAVI_plot() {
                return SAVI_plot;
        }
        
        public final void setSAVI_plot(String sAVI_plot) {
                SAVI_plot = sAVI_plot;
        }
        
        public final String getMCARI_plot() {
                return MCARI_plot;
        }
        
        public final void setMCARI_plot(String mCARI_plot) {
                MCARI_plot = mCARI_plot;
        }
        
        public final String getWBI_plot() {
                return WBI_plot;
        }
        
        public final void setWBI_plot(String wBI_plot) {
                WBI_plot = wBI_plot;
        }
        
        public final String getRDVI_plot() {
                return RDVI_plot;
        }
        
        public final void setRDVI_plot(String rDVI_plot) {
                RDVI_plot = rDVI_plot;
        }
        
        public final String getEVI_plot() {
                return EVI_plot;
        }
        
        public final void setEVI_plot(String eVI_plot) {
                EVI_plot = eVI_plot;
        }
        
        public final String getARI2_plot() {
                return ARI2_plot;
        }
        
        public final void setARI2_plot(String aRI2_plot) {
                ARI2_plot = aRI2_plot;
        }
        
        public final String getCRI2_plot() {
                return CRI2_plot;
        }
        
        public final void setCRI2_plot(String cRI2_plot) {
                CRI2_plot = cRI2_plot;
        }
        
        public final String getTCARI_plot() {
                return TCARI_plot;
        }
        
        public final void setTCARI_plot(String tCARI_plot) {
                TCARI_plot = tCARI_plot;
        }
        
        public final String getOSAVI_plot() {
                return OSAVI_plot;
        }
        
        public final void setOSAVI_plot(String oSAVI_plot) {
                OSAVI_plot = oSAVI_plot;
        }
        
        public final String getTCARIOSAVI_plot() {
                return TCARIOSAVI_plot;
        }
        
        public final void setTCARIOSAVI_plot(String tCARIOSAVI_plot) {
                TCARIOSAVI_plot = tCARIOSAVI_plot;
        }
        
        public final String getNDVI_veg() {
                return NDVI_veg;
        }
        
        public final void setNDVI_veg(String nDVI_veg) {
                NDVI_veg = nDVI_veg;
        }
        
        public final String getPRI_veg() {
                return PRI_veg;
        }
        
        public final void setPRI_veg(String pRI_veg) {
                PRI_veg = pRI_veg;
        }
        
        public final String getSAVI_veg() {
                return SAVI_veg;
        }
        
        public final void setSAVI_veg(String sAVI_veg) {
                SAVI_veg = sAVI_veg;
        }
        
        public final String getMCARI_veg() {
                return MCARI_veg;
        }
        
        public final void setMCARI_veg(String mCARI_veg) {
                MCARI_veg = mCARI_veg;
        }
        
        public final String getWBI_veg() {
                return WBI_veg;
        }
        
        public final void setWBI_veg(String wBI_veg) {
                WBI_veg = wBI_veg;
        }
        
        public final String getRDVI_veg() {
                return RDVI_veg;
        }
        
        public final void setRDVI_veg(String rDVI_veg) {
                RDVI_veg = rDVI_veg;
        }
        
        public final String getEVI_veg() {
                return EVI_veg;
        }
        
        public final void setEVI_veg(String eVI_veg) {
                EVI_veg = eVI_veg;
        }
        
        public final String getARI2_veg() {
                return ARI2_veg;
        }
        
        public final void setARI2_veg(String aRI2_veg) {
                ARI2_veg = aRI2_veg;
        }
        
        public final String getCRI2_veg() {
                return CRI2_veg;
        }
        
        public final void setCRI2_veg(String cRI2_veg) {
                CRI2_veg = cRI2_veg;
        }
        
        public final String getTCARI_veg() {
                return TCARI_veg;
        }
        
        public final void setTCARI_veg(String tCARI_veg) {
                TCARI_veg = tCARI_veg;
        }
        
        public final String getOSAVI_veg() {
                return OSAVI_veg;
        }
        
        public final void setOSAVI_veg(String oSAVI_veg) {
                OSAVI_veg = oSAVI_veg;
        }
        
        public final String getTCARIOSAVI_veg() {
                return TCARIOSAVI_veg;
        }
        
        public final void setTCARIOSAVI_veg(String tCARIOSAVI_veg) {
                TCARIOSAVI_veg = tCARIOSAVI_veg;
        }

        public final String getCCI_plot() {
                return CCI_plot;
        }
        
        public final void setCCI_plot(String cCI_plot) {
                CCI_plot = cCI_plot;
        }
        
        public final String getCCI_veg() {
                return CCI_veg;
        }
        
        public final void setCCI_veg(String cCI_veg) {
                CCI_veg = cCI_veg;
        }
        
        public final boolean is12Channel() {
                return R1000 != null;
        }
}
