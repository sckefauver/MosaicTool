package edu.ub.bioveg.fisioveg.mosaic.ui.macros.thermal;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileOpen;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileSave;
import edu.ub.bioveg.fisioveg.mosaic.ui.MosaicFrame;
import ij.IJ;
import layout.TableLayout;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Jun 29, 2017
 */
public class ThermalIMageMacroPanel extends JPanel {
        
        private static final long serialVersionUID = -8659105452063530776L;
        
        private JLabel batchInputLabel = null;
        private JTextField batchInputField = null;
        private JButton batchInputButton = null;
        
        private JCheckBox saveImagesCheckBox = null;
        private JTextField saveImagesField = null;
        private JButton saveImagesButton = null;
        
        private JLabel kelvin16BitFormatLabel = null;
        private JCheckBox kelvin16BitFormatCheckBox = null;
        
        private JLabel removeDarkLineLabel = null;
        private JCheckBox removeDarkLineCheckBox = null;
        
        private JLabel threeBandsLabel = null;
        private JCheckBox threeBandsCheckBox = null;
        
        private JLabel resultsFileLabel = null;
        private JTextField resultsFileField = null;
        private JButton resultsFileButton = null;
        
        private JPanel optionsPanel = null;
        
        private JPanel buttonPanel = null;
        private JButton runButton = null;
        private JProgressBar progressBar = null;
        
        private String recentDir = null;
        private File batchInputDir = null;
        private File saveImagesDir = null;
        private File saveResultsFile = null;
        
        private ThermalImageMacroVars macroVars = null;
        
        public ThermalIMageMacroPanel() {
                macroVars = new ThermalImageMacroVars();
                
                batchInputLabel = new JLabel("Batch Inputs:");
                batchInputLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                batchInputField = new JTextField(20);
                batchInputField.setEditable(false);
                batchInputField.setBackground(Color.WHITE);
                
                batchInputButton = new JButton("...");
                batchInputButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                batchInputButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                saveImagesCheckBox = new JCheckBox("Save Images?", false);
                saveImagesCheckBox.setFocusable(false);
                saveImagesCheckBox.setSelected(false);
                saveImagesCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveImagesCheckBox_actionPerformed();
                        }
                });
                
                saveImagesField = new JTextField(20);
                saveImagesField.setEditable(false);
                saveImagesField.setBackground(null);
                
                saveImagesButton = new JButton("...");
                saveImagesButton.setEnabled(false);
                saveImagesButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveImagesButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                kelvin16BitFormatLabel = new JLabel("Raw 16bit Kelvin?");
                kelvin16BitFormatLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                kelvin16BitFormatCheckBox = new JCheckBox("", false);
                kelvin16BitFormatCheckBox.setFocusable(false);
                kelvin16BitFormatCheckBox.setSelected(false);
                kelvin16BitFormatCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                setKelvin16BitFormatCheckBox_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                removeDarkLineLabel = new JLabel("Remove Dark Line?");
                removeDarkLineLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                removeDarkLineCheckBox = new JCheckBox("", false);
                removeDarkLineCheckBox.setFocusable(false);
                removeDarkLineCheckBox.setSelected(false);
                removeDarkLineCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                setRemoveDarkLineCheckBox_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                threeBandsLabel = new JLabel("Three Bands?");
                threeBandsLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                threeBandsCheckBox = new JCheckBox("", false);
                threeBandsCheckBox.setFocusable(false);
                threeBandsCheckBox.setSelected(false);
                threeBandsCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                setSeparateBandsCheckBox_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                resultsFileLabel = new JLabel("Results File:");
                resultsFileLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                resultsFileField = new JTextField(20);
                resultsFileField.setEditable(false);
                resultsFileField.setBackground(Color.WHITE);
                
                resultsFileButton = new JButton("...");
                resultsFileButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                resultsFileButton_actionPerformed();  
                        }
                });
                
                //----------------------------------------------------------------
                
                double spacer = 5;
                double[][] layoutSize = {
                                //                   0,                        2,                             4
                                {TableLayout.PREFERRED, spacer, TableLayout.FILL, spacer, TableLayout.PREFERRED},
                                {TableLayout.PREFERRED, //0
                                 spacer,
                                 TableLayout.PREFERRED, //2
                                 spacer,
                                 TableLayout.PREFERRED, //4
                                 spacer,
                                 TableLayout.PREFERRED, //6
                                 spacer,
                                 TableLayout.PREFERRED, //8
                                 spacer,
                                 TableLayout.PREFERRED  //10
                                }
                };
                
                optionsPanel = new JPanel();
                optionsPanel.setBorder(BorderFactory.createTitledBorder("Macro Options"));
                optionsPanel.setLayout(new TableLayout(layoutSize));
                optionsPanel.add(batchInputLabel,           "0, 0");
                optionsPanel.add(batchInputField,           "2, 0");
                optionsPanel.add(batchInputButton,          "4, 0");
                optionsPanel.add(saveImagesCheckBox,        "0, 2");
                optionsPanel.add(saveImagesField,           "2, 2");
                optionsPanel.add(saveImagesButton,          "4, 2");
                optionsPanel.add(kelvin16BitFormatLabel,    "0, 4");
                optionsPanel.add(kelvin16BitFormatCheckBox, "2, 4");
                optionsPanel.add(removeDarkLineLabel,       "0, 6");
                optionsPanel.add(removeDarkLineCheckBox,    "2, 6");
                optionsPanel.add(threeBandsLabel,           "0, 8");
                optionsPanel.add(threeBandsCheckBox,        "2, 8");
                optionsPanel.add(resultsFileLabel,          "0, 10");
                optionsPanel.add(resultsFileField,          "2, 10");
                optionsPanel.add(resultsFileButton,         "4, 10");
                
                //----------------------------------------------------------------
                
                runButton = new JButton("Run Macro");
                runButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                runButton_actionPerformed();
                        }
                });
                
                progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
                progressBar.setStringPainted(true);
                progressBar.setString("Ready");
                progressBar.setIndeterminate(false);
                
                buttonPanel = new JPanel(new BorderLayout(5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(runButton, BorderLayout.EAST);
                buttonPanel.add(progressBar, BorderLayout.CENTER);
                
                // ---------------------------------------------------
                
                RTextScrollPane textScrollPane = createMacroPanel("Thermal Macro", "ThermalImageMacroFLIRTau640TEAX-TIR-Kelvin-Celsius-Bimodal_Plots_v1.ijm");
                
                setLayout(new BorderLayout(5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(optionsPanel, BorderLayout.NORTH);
                add(textScrollPane, BorderLayout.CENTER);
                add(buttonPanel, BorderLayout.SOUTH);
        }
        
        private final void runButton_actionPerformed() {
                if(MosaicFrame.isMacroRunning()) {
                        JOptionPane.showMessageDialog(this, "A macro is already running, please wait until it finishes.", "Macro Already Running", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(batchInputDir == null) {
                        JOptionPane.showMessageDialog(this, "Please select the batch inputs folder.", "Select Batch Inputs", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(saveResultsFile == null) {
                        JOptionPane.showMessageDialog(this, "Please select a results file location.", "Select Results File", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                setSeparateBandsCheckBox_actionPerformed();
                setKelvin16BitFormatCheckBox_actionPerformed();
                setRemoveDarkLineCheckBox_actionPerformed();
                saveImagesCheckBox_actionPerformed();
                
                Thread macroThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                runButton.setEnabled(false);
                                                progressBar.setIndeterminate(true);
                                                progressBar.setString("Running Macro ...");
                                                MosaicFrame.setMacroRunning(true);
                                        }
                                });
                                
                                String macroCode = macroVars.getMacroCode();
                                IJ.runMacro(macroCode);
                                
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                progressBar.setIndeterminate(false);
                                                progressBar.setString("Ready");
                                                runButton.setEnabled(true);
                                                MosaicFrame.setMacroRunning(false);
                                        }
                                });
                        }
                });
                
                macroThread.start();
        }
        
        private final RTextScrollPane createMacroPanel(String panelName, String macroTemplateName) {
                InputStream macroInputStream = ThermalIMageMacroPanel.class.getResourceAsStream("/edu/ub/bioveg/fisioveg/mosaic/ui/macros/thermal/"+macroTemplateName);
                RTextScrollPane syntaxScrollPane = null;
                if(macroInputStream != null) {
                        RSyntaxTextArea syntaxTextArea = new RSyntaxTextArea(20, 60);
                        syntaxTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
                        syntaxTextArea.setCodeFoldingEnabled(true);
                        
                        String line = null;
                        BufferedReader br = null;
                        StringBuilder sb = null;
                        
                        try {
                                sb = new StringBuilder();
                                br = new BufferedReader(new InputStreamReader(macroInputStream));
                                while((line = br.readLine()) != null) {
                                        sb.append(line).append('\n');
                                }
                                
                                syntaxTextArea.setText(sb.toString());
                                syntaxTextArea.setEditable(false);
                                syntaxScrollPane = new RTextScrollPane(syntaxTextArea);
                                
                                macroVars.setMacroNameKey(panelName);
                                macroVars.setMacroName(panelName);
                                macroVars.setSyntaxTextArea(syntaxTextArea);
                                
                                syntaxTextArea.setCaretPosition(0);
                        }
                        catch(IOException ioe) {
                                ioe.printStackTrace();
                                IJ.error("I/O Error", "There was an error while reading the macro file.");
                        }
                        finally {
                                if(br != null) {
                                        try {
                                                br.close();
                                        }
                                        catch (IOException e) {}
                                        
                                        br = null;
                                }
                                
                                sb = null;
                                line = null;
                                macroInputStream = null;
                        }
                }
                else {
                        IJ.error("File Not Found", "Could not find macro \""+macroTemplateName+"\"");
                }
                
                return syntaxScrollPane;
        }
        
        private final void batchInputButton_actionPerformed() {
                batchInputDir = FileOpen.getFile("Select batch input folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY, "Batch Image Folder", (String[]) null);
                if (batchInputDir != null) {
                        recentDir = batchInputDir.getAbsolutePath();
                        batchInputField.setText(batchInputDir.getAbsolutePath());
                        macroVars.setBatchInputVar(batchInputDir.getAbsolutePath());
                }
        }
        
        private final void saveImagesCheckBox_actionPerformed() {
                saveImagesButton.setEnabled(saveImagesCheckBox.isSelected());
                String selection = null;
                if(saveImagesCheckBox.isSelected()) {
                        saveImagesField.setBackground(Color.WHITE);
                        selection = "true";
                }
                else {
                        saveImagesField.setBackground(null);
                        selection = "false";
                }
                
                macroVars.setSaveImagesVar(selection);
        }
        
        private final void saveImagesButton_actionPerformed() {
                saveImagesDir = FileOpen.getFile("Select images folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY , "Images Folder", (String[])null);
                if(saveImagesDir != null) {
                        recentDir = saveImagesDir.getAbsolutePath();
                        saveImagesField.setText(saveImagesDir.getAbsolutePath());
                        macroVars.setSaveOutputDirVar(saveImagesDir.getAbsolutePath());
                }
        }
        
        private final void setSeparateBandsCheckBox_actionPerformed() {
                if(threeBandsCheckBox.isSelected()) {
                        macroVars.setThreeBandsVar("true");
                }
                else {
                        macroVars.setThreeBandsVar("false");
                }
        }
        
        private final void setKelvin16BitFormatCheckBox_actionPerformed() {
                if(kelvin16BitFormatCheckBox.isSelected()) {
                        macroVars.setKelvin16BitFormatVar("true");
                }
                else {
                        macroVars.setKelvin16BitFormatVar("false");
                }
        }
        
        private final void setRemoveDarkLineCheckBox_actionPerformed() {
                if(removeDarkLineCheckBox.isSelected()) {
                        macroVars.setRemoveDarkLineVar("true");
                }
                else {
                        macroVars.setRemoveDarkLineVar("false");
                }
        }
        
        private final void resultsFileButton_actionPerformed() {
                saveResultsFile = FileSave.saveFile("Name Results File", (recentDir == null ? new File(System.getProperty("user.dir")) : new File(recentDir)), "Results File", "ThermalImageMacro_FLIR_Tau640_Bimodal_Results.csv");
                if(saveResultsFile != null) {
                        recentDir = saveResultsFile.getParentFile().getAbsolutePath();
                        resultsFileField.setText(saveResultsFile.getAbsolutePath());
                        macroVars.setSaveResultsFile(saveResultsFile.getAbsolutePath());
                }
        }
}
