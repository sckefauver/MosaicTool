package edu.ub.bioveg.fisioveg.mosaic.ui.macros.ngrdi.ndvi;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileOpen;
import edu.ub.bioveg.fisioveg.mosaic.tools.ui.FileSave;
import edu.ub.bioveg.fisioveg.mosaic.ui.MosaicFrame;
import ij.IJ;
import layout.TableLayout;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Feb 18, 2017
 */
public class NgrdiNdviMacroPanel extends JPanel {
        
        private static final long serialVersionUID = -250113643620001119L;
        
        private JLabel batchInputLabel = null;
        private JTextField batchInputField = null;
        private JButton batchInputButton = null;
        
        private JCheckBox saveNgrdiImagesCheckBox = null;
        private JTextField saveNgrdiImagesField = null;
        private JButton saveNgrdiImagesButton = null;
        
        private JCheckBox saveNdviImagesCheckBox = null;
        private JTextField saveNdviImagesField = null;
        private JButton saveNdviImagesButton = null;
        
        private JCheckBox correctionFactorCheckBox = null;
        
        private JLabel resultsFileLabel = null;
        private JTextField resultsFileField = null;
        private JButton resultsFileButton = null;
        
        private JPanel optionsPanel = null;
        
        private JPanel buttonPanel = null;
        private JButton runButton = null;
        private JProgressBar progressBar = null;
        
        private String recentDir = null;
        private File batchInputDir = null;
        private File saveNgrdiDir = null;
        private File saveNdviDir = null;
        private File saveResultsFile = null;
        
        private NgrdiNdviMacroVars macroVars = null;
        
        public NgrdiNdviMacroPanel() {
                macroVars = new NgrdiNdviMacroVars();
                
                batchInputLabel = new JLabel("Batch Inputs:");
                batchInputLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                batchInputField = new JTextField(20);
                batchInputField.setEditable(false);
                batchInputField.setBackground(Color.WHITE);
                
                batchInputButton = new JButton("...");
                batchInputButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                batchInputButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                saveNgrdiImagesCheckBox = new JCheckBox("Save NGRDI Images?", false);
                saveNgrdiImagesCheckBox.setFocusable(false);
                saveNgrdiImagesCheckBox.setSelected(false);
                saveNgrdiImagesCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveNgrdiImagesCheckBox_actionPerformed();
                        }
                });
                
                saveNgrdiImagesField = new JTextField(20);
                saveNgrdiImagesField.setEditable(false);
                saveNgrdiImagesField.setBackground(null);
                
                saveNgrdiImagesButton = new JButton("...");
                saveNgrdiImagesButton.setEnabled(false);
                saveNgrdiImagesButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveNgrdiImagesButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                saveNdviImagesCheckBox = new JCheckBox("Save NDVI Images?", false);
                saveNdviImagesCheckBox.setFocusable(false);
                saveNdviImagesCheckBox.setSelected(false);
                saveNdviImagesCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveNdviImagesCheckBox_actionPerformed();
                        }
                });
                
                saveNdviImagesField = new JTextField(20);
                saveNdviImagesField.setEditable(false);
                saveNdviImagesField.setBackground(null);
                
                saveNdviImagesButton = new JButton("...");
                saveNdviImagesButton.setEnabled(false);
                saveNdviImagesButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                saveNdviImagesButton_actionPerformed();
                        }
                });
                
                //----------------------------------------------------------------
                
                correctionFactorCheckBox = new JCheckBox("Apply Correction Factor?", false);
                correctionFactorCheckBox.setFocusable(false);
                correctionFactorCheckBox.setSelected(false);
                correctionFactorCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                correctionFactorCheckBox_actionPerformed();
                        }
                });
                
                
                //----------------------------------------------------------------
                
                resultsFileLabel = new JLabel("Results File:");
                resultsFileLabel.setHorizontalAlignment(JLabel.RIGHT);
                
                resultsFileField = new JTextField(20);
                resultsFileField.setEditable(false);
                resultsFileField.setBackground(Color.WHITE);
                
                resultsFileButton = new JButton("...");
                resultsFileButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                resultsFileButton_actionPerformed();  
                        }
                });
                
                //----------------------------------------------------------------
                
                double spacer = 5;
                double[][] layoutSize = {
                                //                   0,                        2,                             4
                                {TableLayout.PREFERRED, spacer, TableLayout.FILL, spacer, TableLayout.PREFERRED},
                                {TableLayout.PREFERRED, //0
                                 spacer,
                                 TableLayout.PREFERRED, //2
                                 spacer,
                                 TableLayout.PREFERRED, //4
                                 spacer,
                                 TableLayout.PREFERRED, //6
                                 spacer,
                                 TableLayout.PREFERRED  //8
                                }
                };
                
                optionsPanel = new JPanel();
                optionsPanel.setBorder(BorderFactory.createTitledBorder("Macro Options"));
                optionsPanel.setLayout(new TableLayout(layoutSize));
                optionsPanel.add(batchInputLabel,         "0, 0");
                optionsPanel.add(batchInputField,         "2, 0");
                optionsPanel.add(batchInputButton,        "4, 0");
                optionsPanel.add(saveNgrdiImagesCheckBox, "0, 2");
                optionsPanel.add(saveNgrdiImagesField,    "2, 2");
                optionsPanel.add(saveNgrdiImagesButton,   "4, 2");
                optionsPanel.add(saveNdviImagesCheckBox,  "0, 4");
                optionsPanel.add(saveNdviImagesField,     "2, 4");
                optionsPanel.add(saveNdviImagesButton,    "4, 4");
                optionsPanel.add(correctionFactorCheckBox,"0, 6");
                optionsPanel.add(resultsFileLabel,        "0, 8");
                optionsPanel.add(resultsFileField,        "2, 8");
                optionsPanel.add(resultsFileButton,       "4, 8");
                
                //----------------------------------------------------------------
                
                runButton = new JButton("Run Macro");
                runButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                runButton_actionPerformed();
                        }
                });
                
                progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
                progressBar.setStringPainted(true);
                progressBar.setString("Ready");
                progressBar.setIndeterminate(false);
                
                buttonPanel = new JPanel(new BorderLayout(5, 5));
                buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                buttonPanel.add(runButton, BorderLayout.EAST);
                buttonPanel.add(progressBar, BorderLayout.CENTER);
                
                // ---------------------------------------------------
                
                RTextScrollPane textScrollPane = createMacroPanel("NGRDI NDVI Macro", "Macro_NDVImodifiedcamera_exp_correct_NGRDI_NDVI.ijm");
                
                setLayout(new BorderLayout(5, 5));
                setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(optionsPanel, BorderLayout.NORTH);
                add(textScrollPane, BorderLayout.CENTER);
                add(buttonPanel, BorderLayout.SOUTH);
        }
        
        private final void runButton_actionPerformed() {
                if(MosaicFrame.isMacroRunning()) {
                        JOptionPane.showMessageDialog(this, "A macro is already running, please wait until it finishes.", "Macro Already Running", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(batchInputDir == null) {
                        JOptionPane.showMessageDialog(this, "Please select the batch inputs folder.", "Select Batch Inputs", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                if(saveResultsFile == null) {
                        JOptionPane.showMessageDialog(this, "Please select a results file location.", "Select Results File", JOptionPane.INFORMATION_MESSAGE);
                        return;
                }
                
                saveNdviImagesCheckBox_actionPerformed();
                saveNgrdiImagesCheckBox_actionPerformed();
                correctionFactorCheckBox_actionPerformed();
                
                Thread macroThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                runButton.setEnabled(false);
                                                progressBar.setIndeterminate(true);
                                                progressBar.setString("Running Macro ...");
                                                MosaicFrame.setMacroRunning(true);
                                        }
                                });
                                
                                String macroCode = macroVars.getMacroCode();
                                IJ.runMacro(macroCode);
                                
                                EventQueue.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                                progressBar.setIndeterminate(false);
                                                progressBar.setString("Ready");
                                                runButton.setEnabled(true);
                                                MosaicFrame.setMacroRunning(false);
                                        }
                                });
                        }
                });
                
                macroThread.start();
        }
        
        private final RTextScrollPane createMacroPanel(String panelName, String macroTemplateName) {
                InputStream macroInputStream = NgrdiNdviMacroPanel.class.getResourceAsStream("/edu/ub/bioveg/fisioveg/mosaic/ui/macros/ngrdi/ndvi/"+macroTemplateName);
                RTextScrollPane syntaxScrollPane = null;
                if(macroInputStream != null) {
                        RSyntaxTextArea syntaxTextArea = new RSyntaxTextArea(20, 60);
                        syntaxTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
                        syntaxTextArea.setCodeFoldingEnabled(true);
                        
                        String line = null;
                        BufferedReader br = null;
                        StringBuilder sb = null;
                        
                        try {
                                sb = new StringBuilder();
                                br = new BufferedReader(new InputStreamReader(macroInputStream));
                                while((line = br.readLine()) != null) {
                                        sb.append(line).append('\n');
                                }
                                
                                syntaxTextArea.setText(sb.toString());
                                syntaxTextArea.setEditable(false);
                                syntaxScrollPane = new RTextScrollPane(syntaxTextArea);
                                
                                macroVars.setMacroNameKey(panelName);
                                macroVars.setMacroName(panelName);
                                macroVars.setSyntaxTextArea(syntaxTextArea);
                                
                                syntaxTextArea.setCaretPosition(0);
                        }
                        catch(IOException ioe) {
                                ioe.printStackTrace();
                                IJ.error("I/O Error", "There was an error while reading the macro file.");
                        }
                        finally {
                                if(br != null) {
                                        try {
                                                br.close();
                                        }
                                        catch (IOException e) {}
                                        
                                        br = null;
                                }
                                
                                sb = null;
                                line = null;
                                macroInputStream = null;
                        }
                }
                else {
                        IJ.error("File Not Found", "Could not find macro \""+macroTemplateName+"\"");
                }
                
                return syntaxScrollPane;
        }
        
        private final void batchInputButton_actionPerformed() {
                batchInputDir = FileOpen.getFile("Select batch input folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY, "Batch Image Folder", (String[]) null);
                if (batchInputDir != null) {
                        recentDir = batchInputDir.getAbsolutePath();
                        batchInputField.setText(batchInputDir.getAbsolutePath());
                        macroVars.setBatchInputVar(batchInputDir.getAbsolutePath());
                }
        }
        
        private final void correctionFactorCheckBox_actionPerformed() {
                String selection = null;
                if(correctionFactorCheckBox.isSelected()) {
                        selection = "true";
                }
                else {
                        selection = "false";
                }
                
                macroVars.setCorrectionFactorVar(selection);
        }
        
        private final void saveNgrdiImagesCheckBox_actionPerformed() {
                saveNgrdiImagesButton.setEnabled(saveNgrdiImagesCheckBox.isSelected());
                String selection = null;
                if(saveNgrdiImagesCheckBox.isSelected()) {
                        saveNgrdiImagesField.setBackground(Color.WHITE);
                        selection = "true";
                }
                else {
                        saveNgrdiImagesField.setBackground(null);
                        selection = "false";
                }
                
                macroVars.setSaveNgrdiImagesVar(selection);
        }
        
        private final void saveNgrdiImagesButton_actionPerformed() {
                saveNgrdiDir = FileOpen.getFile("Select NGRDI images folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY , "NGRDI Images Folder", (String[])null);
                if(saveNgrdiDir != null) {
                        recentDir = saveNgrdiDir.getAbsolutePath();
                        saveNgrdiImagesField.setText(saveNgrdiDir.getAbsolutePath());
                        macroVars.setSaveNgrdiDirVar(saveNgrdiDir.getAbsolutePath());
                }
        }
        
        private final void saveNdviImagesCheckBox_actionPerformed() {
                saveNdviImagesButton.setEnabled(saveNdviImagesCheckBox.isSelected());
                String selection = null;
                if(saveNdviImagesCheckBox.isSelected()) {
                        saveNdviImagesField.setBackground(Color.WHITE);
                        selection = "true";
                }
                else {
                        saveNdviImagesField.setBackground(null);
                        selection = "false";
                }
                
                macroVars.setSaveNdviImagesVar(selection);
        }
        
        private final void saveNdviImagesButton_actionPerformed() {
                saveNdviDir = FileOpen.getFile("Select NDVI images folder", (recentDir == null ? System.getProperty("user.dir") : recentDir), JFileChooser.DIRECTORIES_ONLY , "NDVI Images Folder", (String[])null);
                if(saveNdviDir != null) {
                        recentDir = saveNdviDir.getAbsolutePath();
                        saveNdviImagesField.setText(saveNdviDir.getAbsolutePath());
                        macroVars.setSaveNdviDirVar(saveNdviDir.getAbsolutePath());
                }
        }
        
        private final void resultsFileButton_actionPerformed() {
                saveResultsFile = FileSave.saveFile("Name Results File", (recentDir == null ? new File(System.getProperty("user.dir")) : new File(recentDir)), "Results File", "NGRDI_NDVI_results.csv");
                if(saveResultsFile != null) {
                        recentDir = saveResultsFile.getParentFile().getAbsolutePath();
                        resultsFileField.setText(saveResultsFile.getAbsolutePath());
                        macroVars.setSaveResultsFile(saveResultsFile.getAbsolutePath());
                }
        }
}

