// Macro to batch process MosaicTool output plot level images cut from the orthomosaic of a Tetracam micro-MCA11+ILS and provide the following 
// values as outputs at the plot level "_plot" and for vegetation only "veg", using an NDVI threshold of 0.5 to build the soil mask at the pixel level:
//
// Vegetation_area, R450, R550, R570, R670, R700, R720, R780, R780, R840, R860, R900, R950, R1000,
// R450_veg, R550_veg, R570_veg, R670_veg, R700_veg, R720_veg, R780_veg, R780_veg, R840_veg, R860_veg, R900_veg, R950_veg, R1000_veg
// NDVI_plot, SAVI_plot, OSAVI_plot, RDVI_plot, EVI_plot, PRI_plot, MCARI_plot, CCI_plot, TCARI_plot, TCARIOSAVI_plot, ARI2_plot, CRI2_plot, WBI_plot, 
// NDVI_veg, SAVI_veg, OSAVI_veg, RDVI_veg, EVI_veg, PRI_veg, MCARI_veg, CCI_veg, TCARI_veg, TCARIOSAVI_veg, ARI2_veg, CRI2_veg, WBI_veg
//

var input = "$P{batch_input}";
var list = getFileList(input);

setBatchMode(true);
run("Clear Results");

for (i = 0; i < list.length; i++) {
	if(isImage(list[i])) {
		action(list[i]);
	}
};

selectWindow("Results");
saveAs("Results", "$P{save_results_file}");
run("Close");

call("java.lang.System.gc");
setBatchMode(false);

function action(filename) {
    open(input + filename);
    original = getImageID();
    imageId = original;
    title = getTitle(); 
    imageName = File.nameWithoutExtension;

	/*
	 Perform rgb threshold classification and make masks
	 Split channels for processing and rename files
	*/

	selectImage(imageId);
	channels = nSlices;     //Should be either 10 or 12
	run("Stack to Images");
	
	for(j = 0; j < channels; j++) {
		imageId -= 1;
		selectImage(imageId);
		run("32-bit");
		
		//Adjust bit depth of each band depending on original bit rate of image
		if(bit == 8) { 	
			run("Divide...", "value=256 stack");
		}
		
		if(bit == 16) {
			run("Divide...", "value=65536 stack");;
		}
		
		rename("C"+(j+1));
	};
	
	imageCalculator("Subtract create 32-bit", "C8","C4");
	rename("ResultC8-C4");
	imageCalculator("Add create 32-bit", "C8","C4");
	rename("ResultC8+C4");
	imageCalculator("Divide create 32-bit", "ResultC8-C4","ResultC8+C4");
	rename("IMG_NDVI");
	
	//
	// Create soil bed area mask and measure the NDVI of vegetation area
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Select All");
	run("Measure");
	setResult("Type", nResults-1, "Vegetation_area");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	calculateMeanChannel("C1", "R450", filename);
	calculateMeanChannel("C2", "R550", filename);
	calculateMeanChannel("C3", "R570", filename);
	calculateMeanChannel("C4", "R670", filename);
	calculateMeanChannel("C5", "R700", filename);
	calculateMeanChannel("C6", "R720", filename);
	calculateMeanChannel("C7", "R780", filename);
	calculateMeanChannel("C8", "R840", filename);
	calculateMeanChannel("C9", "R860", filename);
	calculateMeanChannel("C10", "R900", filename);
	
	if(channels >= 11) {
		calculateMeanChannel("C11", "R950", filename);
	}
	
	if(channels == 12) {
		calculateMeanChannel("C12", "R1000", filename);
	}
	
	calculateMeanChannelVeg("C1", "R450_veg", filename);
	calculateMeanChannelVeg("C2", "R550_veg", filename);
	calculateMeanChannelVeg("C3", "R570_veg", filename);
	calculateMeanChannelVeg("C4", "R670_veg", filename);
	calculateMeanChannelVeg("C5", "R700_veg", filename);
	calculateMeanChannelVeg("C6", "R720_veg", filename);
	calculateMeanChannelVeg("C7", "R780_veg", filename);
	calculateMeanChannelVeg("C8", "R840_veg", filename);
	calculateMeanChannelVeg("C9", "R860_veg", filename);
	calculateMeanChannelVeg("C10", "R900_veg", filename);
	
	if(channels >= 11) {
		calculateMeanChannelVeg("C11", "R950_veg", filename);
	}
	
	if(channels == 12) {
		calculateMeanChannelVeg("C12", "R1000_veg", filename);
	}
	
	//
	// Use image math calculator plus functions to calculate the NDVI of the whole plot
	//
	selectWindow("IMG_NDVI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "NDVI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator plus functions to calculate the SAVI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C8","C4");
	rename("ResultC8-C4");
	imageCalculator("Add create 32-bit", "C8","C4");
	rename("ResultC8+C4");
	selectWindow("ResultC8+C4");
	run("Add...", "value=0.5");
	rename("ResultC8+C4+05");
	imageCalculator("Divide create 32-bit", "ResultC8-C4","ResultC8+C4+05");
	rename("ResultC8-C4/C8+C4+05");
	selectWindow("ResultC8-C4/C8+C4+05");
	run("Multiply...", "value=1.5");
	rename("IMG_SAVI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "SAVI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator plus functions to calculate the OSAVI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C8","C4");
	rename("ResultC8-C4");
	selectWindow("ResultC8-C4");
	run("Multiply...", "value=1.16");
	rename("Result116C8-C4");
	imageCalculator("Add create 32-bit", "C8","C4");
	rename("ResultC8+C4");
	selectWindow("ResultC8+C4");
	run("Add...", "value=0.16");
	rename("ResultC8+C4+016");
	imageCalculator("Divide create 32-bit", "Result116C8-C4","ResultC8+C4+016");
	rename("IMG_OSAVI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "OSAVI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	OSAVI_plot = getResult("Mean");
	
	//
	// Use image math calculator plus functions to calculate the RDVI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C8","C4");
	rename("ResultC8-C4");
	imageCalculator("Add create 32-bit", "C8","C4");
	rename("ResultC8+C4");
	selectWindow("ResultC8+C4");
	run("Square Root");
	rename("ResultC8+C4^1/2");
	imageCalculator("Divide create 32-bit", "ResultC8-C4","ResultC8+C4^1/2");
	rename("IMG_RDVI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "RDVI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator plus functions to calculate the EVI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C8","C4");
	rename("ResultC8-C4");
	selectWindow("C4");
	run("Duplicate...", "title=2C4");
	selectWindow("2C4");
	run("Multiply...", "value=6");
	rename("Result6*C4");
	selectWindow("C1");
	run("Duplicate...", "title=2C1");
	selectWindow("2C1");
	run("Multiply...", "value=7.5");
	rename("Result75*C1");
	imageCalculator("Add create 32-bit", "C8","Result6*C4");
	rename("ResultC8+6*C4");
	imageCalculator("Subtract create 32-bit", "ResultC8+6*C4","Result75*C1");
	rename("ResultC8+6*C4-75*C1");
	selectWindow("ResultC8+6*C4-75*C1");
	run("Add...", "value=1");
	rename("ResultC8+6*C4-75*C1+1");
	imageCalculator("Divide create 32-bit", "ResultC8-C4","ResultC8+6*C4-75*C1+1");
	rename("ResultC8-C4/C8+6*C4-75*C1+1");
	selectWindow("ResultC8-C4/C8+6*C4-75*C1+1");
	run("Multiply...", "value=2.5");
	rename("IMG_EVI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "EVI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator plus functions to calculate the PRI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C2","C3");
	rename("ResultC2-C3");
	imageCalculator("Add create 32-bit", "C2","C3");
	rename("ResultC2+C3");
	imageCalculator("Divide create 32-bit", "ResultC2-C3","ResultC2+C3");
	rename("IMG_PRI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "PRI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator subtract functions to calculate the MCARI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C5","C4");
	rename("ResultC5-C4");
	imageCalculator("Subtract create 32-bit", "C5","C2");
	rename("ResultC5-C2");
	selectWindow("ResultC5-C2");
	run("Multiply...", "value=0.2");
	rename("Result0.2*C5-C2");
	imageCalculator("Subtract create 32-bit", "ResultC5-C4","Result0.2*C5-C2");
	rename("ResultC5-C4-0.2*C5-C2");
	imageCalculator("Divide create 32-bit", "C5","C4");
	rename("ResultC5/C4");
	imageCalculator("Multiply create 32-bit", "ResultC5-C4-0.2*C5-C2","ResultC5/C4");
	rename("IMG_MCARI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "MCARI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator subtract functions to calculate the CCI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C11","C1");
	rename("ResultC11-C1");
	imageCalculator("Add create 32-bit", "C1","C11");
	rename("ResultC11+C1");
	imageCalculator("Divide create 32-bit", "ResultC11-C1","ResultC11+C1");
	rename("IMG_CCI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=2");
	run("Measure");
	setResult("Type", nResults-1, "CCI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator subtract functions to calculate the TCARI of the whole plot
	//
	imageCalculator("Subtract create 32-bit", "C5","C4");
	rename("ResultC5-C4");
	selectWindow("ResultC5-C4");
	run("Multiply...", "value=3");
	rename("Result3C5C4");
	imageCalculator("Subtract create 32-bit", "C5","C2");
	rename("ResultC5-C2");
	imageCalculator("Divide create 32-bit", "C5","C4");
	rename("ResultC5/C4");
	imageCalculator("Multiply create 32-bit", "ResultC5-C2","ResultC5/C4");
	rename("ResultC5C2C5C4");
	selectWindow("ResultC5C2C5C4");
	run("Multiply...", "value=0.2");
	rename("Result2");
	imageCalculator("Subtract create 32-bit", "Result3C5C4","Result2");
	rename("IMG_TCARI");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "TCARI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	TCARI_plot = getResult("Mean");
	
	//
	// Use image math calculator plus functions to calculate the TCARIOSAVI of the whole plot
	//
	TCARIOSAVI_plot = TCARI_plot / OSAVI_plot;
	setResult("%Area", nResults, TCARIOSAVI_plot);
	setResult("Type", nResults-1, "TCARIOSAVI_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator plus functions to calculate the ARI2 of the whole plot
	//
	selectWindow("C2");
	run("Duplicate...", "title=2C2");
	selectWindow("2C2");
	run("Reciprocal");
	rename("Result1/C2");
	selectWindow("C5");
	run("Duplicate...", "title=2C5");
	selectWindow("2C5");
	run("Reciprocal");
	rename("Result1/C5");
	imageCalculator("Subtract create 32-bit", "Result1/C2","Result1/C5");
	rename("Result1/C2-1/C5");
	imageCalculator("Multiply create 32-bit", "Result1/C2-1/C5","C8");
	rename("IMG_ARI2");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "ARI2_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Use image math calculator plus functions to calculate the CRI2 of the whole plot
	//
	selectWindow("C2");
	run("Duplicate...", "title=2C2");
	selectWindow("2C2");
	run("Reciprocal");
	rename("Result1/C2");
	selectWindow("C5");
	run("Duplicate...", "title=2C5");
	selectWindow("2C5");
	run("Reciprocal");
	rename("Result1/C5");
	imageCalculator("Subtract create 32-bit", "Result1/C2","Result1/C5");
	rename("IMG_CRI2");
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, "CRI2_plot");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	if(channels >= 11) {
		//	
		// Use image math calculator plus functions to calculate the WBI of the whole plot
		//
		imageCalculator("Divide create 32-bit", "C10","C11");
		rename("IMG_WBI");
		run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
		run("Measure");
		setResult("Type", nResults-1, "WBI_plot");
		setResult("File Name", nResults-1, filename);
		updateResults();
	}

	
	selectWindow("Soil_mask");
	run("Duplicate...", "title=Soil_mask-save");
	selectWindow("Soil_mask-save");
	run("Set Measurements...", "mean area_fraction redirect=IMG_NDVI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "NDVI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the SAVI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_SAVI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "SAVI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the TCARI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_OSAVI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "OSAVI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	OSAVI_veg = getResult("Mean");
	
	//
	// Create soil bed area mask and measure the RDVI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_RDVI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "RDVI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the EVI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_EVI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "EVI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the PRI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_PRI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "PRI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the MCARI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_MCARI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "MCARI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	//Create soil bed area mask and measure the CCI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_CCI decimal=2");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "CCI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the TCARI of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_TCARI decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "TCARI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	TCARI_veg = getResult("Mean");
	
	TCARIOSAVI_veg = TCARI_veg / OSAVI_veg;
	setResult("%Area", nResults, TCARIOSAVI_veg);
	setResult("Type", nResults-1, "TCARIOSAVI_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the ARI2 of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_2NDVI");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_ARI2 decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "ARI2_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	//
	// Create soil bed area mask and measure the CRI2 of vegetation
	//
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect=IMG_CRI2 decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, "CRI2_veg");
	setResult("File Name", nResults-1, filename);
	updateResults();
	
	if(channels >= 11) {
		//
		// Create soil bed area mask and measure the WBI of vegetation
		//
		selectWindow("IMG_NDVI");
		run("Duplicate...", "title=IMG_NDVI2");
		selectWindow("IMG_NDVI2");
		rename("IMG_NDVI_soil");
		selectWindow("IMG_NDVI_soil");
		setThreshold(0,50);
		setOption("BlackBackground", false);
		setAutoThreshold("Default");
		setAutoThreshold("Default dark");
		run("Convert to Mask");
		run("Despeckle");
		run("Invert");
		rename("Soil_mask");
		run("Set Measurements...", "mean area_fraction redirect=IMG_WBI decimal=4");
		run("Create Selection");
		run("Make Inverse");
		run("Measure");
		setResult("Type", nResults-1, "WBI_veg");
		setResult("File Name", nResults-1, filename);
		updateResults();
	}
	
	run("Close All");
};

function calculateMeanChannel(window, col, filename) {
	selectWindow(window);
	run("Set Measurements...", "mean area_fraction redirect=None decimal=4");
	run("Measure");
	setResult("Type", nResults-1, col);
	setResult("File Name", nResults-1, filename);
	updateResults();
};

function calculateMeanChannelVeg(window, col, filename) {
	selectWindow("IMG_NDVI");
	run("Duplicate...", "title=IMG_NDVI2");
	selectWindow("IMG_NDVI2");
	rename("IMG_NDVI_soil");
	selectWindow("IMG_NDVI_soil");
	setThreshold(0,50);
	setOption("BlackBackground", false);
	setAutoThreshold("Default");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Despeckle");
	run("Invert");
	rename("Soil_mask");
	run("Set Measurements...", "mean area_fraction redirect="+window+" decimal=4");
	run("Create Selection");
	run("Make Inverse");
	run("Measure");
	setResult("Type", nResults-1, col);
	setResult("File Name", nResults-1, filename);
	updateResults();
};

function isImage(filename) {
	if(endsWith(toLowerCase(filename), ".tif")||
	   endsWith(toLowerCase(filename), ".tiff") ||
	   endsWith(toLowerCase(filename), ".jpg") ||
	   endsWith(toLowerCase(filename), ".jpeg")) {
	   return true;
	}
	else {
		return false;
	}
};