//Macro to batch process calculate the NGRDI and TGI RGB "multispectral" style vegetation indexes.

var input = "$P{batch_input}";
var ngrdiDir = "$P{save_ngrdi_dir}";
var tgiDir = "$P{save_tgi_dir}";
var saveNgrdi = $P{save_ngrdi_images};
var saveTgi = $P{save_tgi_images};
var resultsFile = "$P{save_results_file}";
var list = getFileList(input);

setBatchMode(true);
run("Clear Results");

var fileCount = getImageFileCount(list);
var filenames = newArray(fileCount);
var ngrdis = newArray(fileCount);
var ngrdiVegs = newArray(fileCount);
var tgis = newArray(fileCount);
var tgiVegs = newArray(fileCount);
var n = 0;

for (i = 0; i < list.length; i++) {
    if(isImage(list[i])) {
        action(input, list[i]);
    }
}

printResults();

call("java.lang.System.gc");
setBatchMode(false);

function action(input, filename) {
    open(input + filename);
    original = getImageID;
    title = getTitle; 
    imageName = File.nameWithoutExtension;

    //Calculate the Normalized Green Red Difference Index (NGRDI) and Triangular Greeness (TGI) for the leaf area
    //These are vegetation indexes from RGB components based on Hunt et al. 2014. 

    //Perform RGB threshold classification and make masks
    selectWindow(filename);
    
    //Convert to separate red, green and blue images for processing
    run("Split Channels");
    selectWindow(filename + " (red)");
    rename("IMG_(red)");
    selectWindow(filename + " (green)");
    rename("IMG_(green)");
    selectWindow(filename + " (blue)");
    rename("IMG_(blue)");
    
    //Use image math calculator plus functions to calculate the NGRDI
    imageCalculator("Add create 32-bit", "IMG_(green)","IMG_(red)");
    rename("ResultG+R");
    imageCalculator("Subtract create 32-bit", "IMG_(green)","IMG_(red)");
    rename("ResultG-R");
    imageCalculator("Divide create 32-bit", "ResultG-R","ResultG+R");
    rename("IMG_NGRDI");

    //Calculate the mean NGRDI value for the total of the leaf area
    selectWindow("IMG_NGRDI");
    run("Set Measurements...", "mean redirect=None decimal=4");
	run("Select All");
    run("Measure");
    NGRDI = getResult("Mean");
	setThreshold(0, 1);
	run("Create Selection");
    run("Measure");
	NGRDIveg = getResult("Mean");	
	
    //Use image math calculator plus functions to calculate the TGI
	imageCalculator("Subtract create 32-bit", "IMG_(red)","IMG_(blue)");
	rename("ResultR-B");
	imageCalculator("Subtract create 32-bit", "IMG_(red)","IMG_(green)");
	rename("ResultR-G");
	selectWindow("ResultR-G");
	run("Multiply...", "value=190");
	selectWindow("ResultR-B");
	run("Multiply...", "value=120");
	run("Calculator Plus", "i1=ResultR-G i2=ResultR-B operation=[Subtract: i2 = (i1-i2) x k1 + k2] k1=-0.5 k2=0 create");
	rename("IMG_TGI");
    
    //Calculate the mean TGI value for the total of the leaf area
    selectWindow("IMG_TGI");
    run("Set Measurements...", "mean redirect=None decimal=4");
    run("Measure");
    TGI = getResult("Mean");
	run("Restore Selection");
	run("Measure");
	
	TGIveg = getResult("Mean");
	
	if(saveNgrdi) {
	    selectWindow("IMG_NGRDI");
        saveAs("Tiff", ngrdiDir + imageName + "_NGRDI");
    }
	
	if(saveTgi) {
		selectWindow("IMG_TGI");
		saveAs("Tiff", tgiDir + imageName + "_TGI");
	}
	
    //clean up after each file
    run("Close All");
    run("Clear Results");

    //Consolidate data
    filenames[n] = filename;
    ngrdis[n] = NGRDI;
	ngrdiVegs[n] = NGRDIveg;
    tgis[n] = TGI;
	tgiVegs[n] = TGIveg;
    n++;
}

function printResults() {
    run("Clear Results");
    for (i = 0; i < fileCount; i++) {
        setResult("File Name", i, filenames[i]);
        setResult("NGRDI", i, ngrdis[i]);
		setResult("NGRDIveg", i, ngrdiVegs[i]);
        setResult("TGI", i, tgis[i]);
		setResult("TGIveg", i, tgiVegs[i]);
    }

    setOption("ShowRowNumbers", false);
    updateResults();
    selectWindow("Results");
    saveAs("Results", resultsFile);
}

function getImageFileCount(list) {
    count = 0;
    for (i = 0; i < list.length; i++) {
        if(isImage(list[i])) {
            count++;
        }
    }
    return count;
}

function isImage(filename) {
    if(endsWith(toLowerCase(filename), ".tif")||
       endsWith(toLowerCase(filename), ".tiff") ||
       endsWith(toLowerCase(filename), ".jpg") ||
       endsWith(toLowerCase(filename), ".jpeg")) {
       return true;
    }
    else {
        return false;
    }
};