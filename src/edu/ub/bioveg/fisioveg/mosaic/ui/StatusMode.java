package edu.ub.bioveg.fisioveg.mosaic.ui;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 4, 2017
 * 
 */
public enum StatusMode {

        EDIT("Edit", new Color(255,242,204), Color.BLACK, new Color(214,182,86)),
        ERROR("Error", new Color(248, 206, 204), Color.BLACK, new Color(184,84,80)),
        VIEW("View", new Color(213,232,212), Color.BLACK, new Color(130,179,102));
        
        private Color statusBackground;
        private Color statusForeground;
        private Color statusBorder;
        private String status;
        
        StatusMode(String status, Color statusBackground, Color statusForeground, Color statusBorder) {
                this.status = status;
                this.statusBackground = statusBackground;
                this.statusForeground = statusForeground;
                this.statusBorder = statusBorder;
        }
        
        public Color getStatusBackground() {
                return statusBackground;
        }
        
        public Color getStatusForeground() {
                return statusForeground;
        }
        
        public Color getStatusBorder() {
                return statusBorder;
        }
        
        public String getStatus() {
                return status;
        }
}
