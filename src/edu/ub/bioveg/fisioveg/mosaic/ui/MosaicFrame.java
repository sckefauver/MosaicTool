package edu.ub.bioveg.fisioveg.mosaic.ui;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import edu.ub.bioveg.fisioveg.mosaic.MosaicTool;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.multispectral.MultiSpectralMacroPanel;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.multispectral.generic.MultispectralGenericMacroPanel;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.ngrdi.ndvi.NgrdiNdviMacroPanel;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.ngrdi.tgi.NgrdiTgiMacroPanel;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.thermal.ThermalIMageMacroPanel;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: May 1, 2016
 * 
 */
public class MosaicFrame extends JFrame {
        
        private static final long serialVersionUID = 3294320332657869704L;
        
        private JTabbedPane tabbedPane = null;
        private MosaicPanel mosaicPanel = null;
        private BreedPixPanel breedPixPanel = null;
        private MultiSpectralMacroPanel multiSpectralMacroPanel = null;
        private MultispectralGenericMacroPanel multiSpectralGenericMacroPanel = null;
        private NgrdiNdviMacroPanel ngrdiNdviMacroPanel = null;
        private NgrdiTgiMacroPanel ngrdiTgiMacroPanel = null;
        private ThermalIMageMacroPanel thermalMacroPanel = null;
        
        private static boolean breedPixRunning = false;
        private static boolean macrosRunning = false;
        
        public MosaicFrame() {
                super();
                init();
        }
        
        private final void init() {
                mosaicPanel = new MosaicPanel();
                breedPixPanel = new BreedPixPanel();
                multiSpectralMacroPanel = new MultiSpectralMacroPanel();
                multiSpectralGenericMacroPanel = new MultispectralGenericMacroPanel();
                ngrdiNdviMacroPanel = new NgrdiNdviMacroPanel();
                ngrdiTgiMacroPanel = new NgrdiTgiMacroPanel();
                thermalMacroPanel = new ThermalIMageMacroPanel();

                tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
                tabbedPane.addTab("Mosaic Tool", mosaicPanel);
                tabbedPane.addTab("BreedPix", breedPixPanel);
                tabbedPane.addTab("MultiSpectral Macro", multiSpectralMacroPanel);
                tabbedPane.addTab("MutliSpectral Generic Macro", multiSpectralGenericMacroPanel);
                tabbedPane.addTab("NGRDI NDVI Macro", ngrdiNdviMacroPanel);
                tabbedPane.addTab("NGRDI TGI Macro", ngrdiTgiMacroPanel);
                tabbedPane.addTab("Thermal Macro", thermalMacroPanel);

                // ---------------------------------------------------

                setTitle("Mosaic Tool - " + MosaicTool.VERSION);
                setLayout(new BorderLayout(5, 5));
                add(tabbedPane, BorderLayout.CENTER);

                setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                                exit();
                        }
                });
                
                pack();
        }
        
        public static final void setBreedPixRunning(boolean running) {
                breedPixRunning = running;
        }
        
        public static final void setMacroRunning(boolean running) {
                macrosRunning = running;
        }
        
        public static final boolean isMacroRunning() {
                return macrosRunning;
        }
        
        public static final boolean isBreedPixRunning() {
                return breedPixRunning;
        }
        
        private final void exit() {
                
                //TODO code this
//                StringBuilder sb = new StringBuilder();
//                
//                if(scannerRunning || macrosRunning) {
//                        sb.append("There are still tasks running such as");
//                        if(scannerRunning) {
//                                sb.append(" \"Scanner\"");
//                        }
//                        
//                        if(macrosRunning) {
//                                if(scannerRunning) {
//                                        sb.append(" and");
//                                }
//                                
//                                sb.append(" \"Canopy Macros\"");
//                        }
//                        
//                        if(breedPixRunning) {
//                                if(macrosRunning || scannerRunning) {
//                                        sb.append(" and");
//                                }
//                                
//                                sb.append(" \"BreedPix\"");
//                        }
//                        
//                        sb.append(". Exiting now may leave results unfinished, still exit ?");
//                }
//                else {
//                        sb.append("Exit the plugin ?");
//                }
//                
//                String msg = sb.toString();
//                
//                int choice = JOptionPane.showConfirmDialog(this, msg, "Exit Plugin ?", JOptionPane.YES_NO_CANCEL_OPTION);
//                if(JOptionPane.YES_OPTION == choice) {
//                        setVisible(false);
//                        dispose();
//                        System.gc();
//                }
                
                
                String msg = null;
                if(breedPixRunning || macrosRunning) {
                        msg = "There are still tasks running such as \"BreedPix\" or \"Multispectral Macro\" Exiting now may leave results unfinished, still exit?";
                }
                else {
                        msg = "Exit the plugin?";
                }

                int choice = JOptionPane.showConfirmDialog(this, msg, "Exit Plugin ?", JOptionPane.YES_NO_CANCEL_OPTION);
                if(JOptionPane.YES_OPTION == choice) {
                        setVisible(false);
                        mosaicPanel.close();
                        dispose();
                        System.gc();
                }
        }
}
