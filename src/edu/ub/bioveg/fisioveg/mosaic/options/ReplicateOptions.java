package edu.ub.bioveg.fisioveg.mosaic.options;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 31, 2016
 *
 */
public class ReplicateOptions {

        private int noReps = -1;
        private int repSpacing = -1;

        public ReplicateOptions() {

        }

        public ReplicateOptions(int noReps, int repSpacing) {
                super();
                this.noReps = noReps;
                this.repSpacing = repSpacing;
        }

        public final int getNoReps() {
                return noReps;
        }

        public final void setNoReps(int noReps) {
                this.noReps = noReps;
        }

        public final void setNoReps(String noReps) {
                this.noReps = Integer.parseInt(noReps);
        }

        public final int getRepSpacing() {
                return repSpacing;
        }

        public final void setRepSpacing(int repSpacing) {
                this.repSpacing = repSpacing;
        }

        public final void setRepSpacing(String repSpacing) {
                this.repSpacing = Integer.parseInt(repSpacing);
        }
}
