package edu.ub.bioveg.fisioveg.mosaic.options;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 29, 2016
 *
 */
public class PlotOptions {

        private double plotHeight = 0;
        private double plotVerticalSpacing = 0;
        private int plotNumbering = 0;
        private boolean topDownNumbering = true;
        private int bufferX = 0;
        private int bufferY = 0;
        
        public PlotOptions() {
                
        }

        public final double getPlotHeight() {
                return plotHeight;
        }

        public final void setPlotHeight(double plotHeight) {
                this.plotHeight = plotHeight;
        }
        
        public final void setPlotHeight(String plotHeight) {
                this.plotHeight = Double.parseDouble(plotHeight);
        }

        public final double getPlotVerticalSpacing() {
                return plotVerticalSpacing;
        }

        public final void setPlotVerticalSpacing(double plotVerticalSpacing) {
                this.plotVerticalSpacing = plotVerticalSpacing;
        }
        
        public final void setPlotVerticalSpacing(String plotVerticalSpacing) {
                this.plotVerticalSpacing = Double.parseDouble(plotVerticalSpacing);
        }
        
        public final int getPlotNumbering() {
                return plotNumbering;
        }
        
        public final void setPlotNumbering(int plotNumbering) {
                this.plotNumbering = plotNumbering;
        }
        
        public final boolean isTopDownNumbering() {
                return topDownNumbering;
        }
        
        public final void setTopDownNumbering(boolean topDownNumbering) {
                this.topDownNumbering = topDownNumbering;
        }
        
        public final int getBufferX() {
                return bufferX;
        }

        public final void setBufferX(int bufferX) {
                this.bufferX = bufferX;
        }
        
        public final void setBufferX(String bufferX) {
                if(!bufferX.isEmpty()) {
                        this.bufferX = Integer.parseInt(bufferX);
                }
        }
        
        public final int getBufferY() {
                return bufferY;
        }
        
        public final void setBufferY(String bufferY) {
                if(!bufferY.isEmpty()) {
                        this.bufferY = Integer.parseInt(bufferY);
                }
        }

        public final void setBufferY(int bufferY) {
                this.bufferY = bufferY;
        }
        
        public final boolean hasBuffers() {
                return bufferX > 0 || bufferY > 0;
        }
}
