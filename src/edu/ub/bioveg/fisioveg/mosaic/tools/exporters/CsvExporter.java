package edu.ub.bioveg.fisioveg.mosaic.tools.exporters;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.multispectral.ResultPojo;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Dec 12, 2016
 * 
 */
public final class CsvExporter {

        private CsvExporter() {

        }

        public final static File ExportTetracamResults(Map<String,ResultPojo> pojoMap, File saveResultsFile, String delimiter) {
                File csvResultsFile = null;
                BufferedWriter bw = null;
                char delim = setDelimiter(delimiter);
                
                try {
                        //Small hack to extract 12 channel flag.
                        //The csv header should be constructed before arriving to this method
                        Iterator<ResultPojo> tmpIter = pojoMap.values().iterator();
                        boolean is12Channel = tmpIter.next().is12Channel();
                        tmpIter = null;
                        
                        csvResultsFile = new File(saveResultsFile.getAbsolutePath() + ".csv");
                        bw = new BufferedWriter(new FileWriter(csvResultsFile, false));
                        bw.write("filename");
                        bw.write(delim);
                        bw.write("Vegetation_area");
                        bw.write(delim);
                        bw.write("R450");
                        bw.write(delim);
                        bw.write("R550");
                        bw.write(delim);
                        bw.write("R570");
                        bw.write(delim);
                        bw.write("R670");
                        bw.write(delim);
                        bw.write("R700");
                        bw.write(delim);
                        bw.write("R720");
                        bw.write(delim);
                        bw.write("R780");
                        bw.write(delim);
                        bw.write("R840");
                        bw.write(delim);
                        bw.write("R860");
                        bw.write(delim);
                        bw.write("R900");
                        bw.write(delim);
                        bw.write("R950");
                        
                        if(is12Channel) {
                                bw.write(delim);
                                bw.write("R1000");
                        }
                        
                        bw.write(delim);
                        bw.write("R450_veg");
                        bw.write(delim);
                        bw.write("R550_veg");
                        bw.write(delim);
                        bw.write("R570_veg");
                        bw.write(delim);
                        bw.write("R670_veg");
                        bw.write(delim);
                        bw.write("R700_veg");
                        bw.write(delim);
                        bw.write("R720_veg");
                        bw.write(delim);
                        bw.write("R780_veg");
                        bw.write(delim);
                        bw.write("R840_veg");
                        bw.write(delim);
                        bw.write("R860_veg");
                        bw.write(delim);
                        bw.write("R900_veg");
                        bw.write(delim);
                        bw.write("R950_veg");
                        
                        if(is12Channel) {
                                bw.write(delim);
                                bw.write("R1000_veg");
                        }
                        
                        bw.write(delim);
                        bw.write("NDVI_plot");
                        bw.write(delim);
                        bw.write("SAVI_plot");
                        bw.write(delim);
                        bw.write("OSAVI_plot");
                        bw.write(delim);
                        bw.write("RDVI_plot");
                        bw.write(delim);
                        bw.write("EVI_plot");
                        bw.write(delim);
                        bw.write("PRI_plot");
                        bw.write(delim);
                        bw.write("MCARI_plot");
                        bw.write(delim);
                        bw.write("CCI_plot");
                        bw.write(delim);
                        bw.write("TCARI_plot");
                        bw.write(delim);
                        bw.write("TCARIOSAVI_plot");
                        bw.write(delim);
                        bw.write("ARI2_plot");
                        bw.write(delim);
                        bw.write("CRI2_plot");
                        bw.write(delim);
                        bw.write("WBI_plot");
                        bw.write(delim);
                        bw.write("NDVI_veg");
                        bw.write(delim);
                        bw.write("SAVI_veg");
                        bw.write(delim);
                        bw.write("OSAVI_veg");
                        bw.write(delim);
                        bw.write("RDVI_veg");
                        bw.write(delim);
                        bw.write("EVI_veg");
                        bw.write(delim);
                        bw.write("PRI_veg");
                        bw.write(delim);
                        bw.write("MCARI_veg");
                        bw.write(delim);
                        bw.write("CCI_veg");
                        bw.write(delim);
                        bw.write("TCARI_veg");
                        bw.write(delim);
                        bw.write("TCARIOSAVI_veg");
                        bw.write(delim);
                        bw.write("ARI2_veg");
                        bw.write(delim);
                        bw.write("CRI2_veg");
                        bw.write(delim);
                        bw.write("WBI_veg");
                        bw.write(delim);
                        bw.write(System.getProperty("line.separator"));
                        bw.flush();
                        
                        Iterator<ResultPojo> iter = pojoMap.values().iterator();
                        while(iter.hasNext()) {
                                ResultPojo pojo = iter.next();
                                bw.write(pojo.getFilename());
                                bw.write(delim);
                                bw.write(pojo.getVegetation_area());
                                bw.write(delim);
                                bw.write(pojo.getR450());
                                bw.write(delim);
                                bw.write(pojo.getR550());
                                bw.write(delim);
                                bw.write(pojo.getR570());
                                bw.write(delim);
                                bw.write(pojo.getR670());
                                bw.write(delim);
                                bw.write(pojo.getR700());
                                bw.write(delim);
                                bw.write(pojo.getR720());
                                bw.write(delim);
                                bw.write(pojo.getR780());
                                bw.write(delim);
                                bw.write(pojo.getR840());
                                bw.write(delim);
                                bw.write(pojo.getR860());
                                bw.write(delim);
                                bw.write(pojo.getR900());
                                bw.write(delim);
                                bw.write(pojo.getR950());
                                
                                if(is12Channel) {
                                        bw.write(delim);
                                        bw.write(pojo.getR1000());
                                }

                                bw.write(delim);
                                bw.write(pojo.getR450_veg());
                                bw.write(delim);
                                bw.write(pojo.getR550_veg());
                                bw.write(delim);
                                bw.write(pojo.getR570_veg());
                                bw.write(delim);
                                bw.write(pojo.getR670_veg());
                                bw.write(delim);
                                bw.write(pojo.getR700_veg());
                                bw.write(delim);
                                bw.write(pojo.getR720_veg());
                                bw.write(delim);
                                bw.write(pojo.getR780_veg());
                                bw.write(delim);
                                bw.write(pojo.getR840_veg());
                                bw.write(delim);
                                bw.write(pojo.getR860_veg());
                                bw.write(delim);
                                bw.write(pojo.getR900_veg());
                                bw.write(delim);
                                bw.write(pojo.getR950_veg());
                                
                                if(is12Channel) {
                                        bw.write(delim);
                                        bw.write(pojo.getR1000_veg());
                                }
                                
                                bw.write(delim);
                                bw.write(pojo.getNDVI_plot());
                                bw.write(delim);
                                bw.write(pojo.getSAVI_plot());
                                bw.write(delim);
                                bw.write(pojo.getOSAVI_plot());
                                bw.write(delim);
                                bw.write(pojo.getRDVI_plot());
                                bw.write(delim);
                                bw.write(pojo.getEVI_plot());
                                bw.write(delim);
                                bw.write(pojo.getPRI_plot());
                                bw.write(delim);
                                bw.write(pojo.getMCARI_plot());
                                bw.write(delim);
                                bw.write(pojo.getCCI_plot());
                                bw.write(delim);
                                bw.write(pojo.getTCARI_plot());
                                bw.write(delim);
                                bw.write(pojo.getTCARIOSAVI_plot());
                                bw.write(delim);
                                bw.write(pojo.getARI2_plot());
                                bw.write(delim);
                                bw.write(pojo.getCRI2_plot());
                                bw.write(delim);
                                bw.write(pojo.getWBI_plot());
                                bw.write(delim);
                                bw.write(pojo.getNDVI_veg());
                                bw.write(delim);
                                bw.write(pojo.getSAVI_veg());
                                bw.write(delim);
                                bw.write(pojo.getOSAVI_veg());
                                bw.write(delim);
                                bw.write(pojo.getRDVI_veg());
                                bw.write(delim);
                                bw.write(pojo.getEVI_veg());
                                bw.write(delim);
                                bw.write(pojo.getPRI_veg());
                                bw.write(delim);
                                bw.write(pojo.getMCARI_veg());
                                bw.write(delim);
                                bw.write(pojo.getCCI_veg());
                                bw.write(delim);
                                bw.write(pojo.getTCARI_veg());
                                bw.write(delim);
                                bw.write(pojo.getTCARIOSAVI_veg());
                                bw.write(delim);
                                bw.write(pojo.getARI2_veg());
                                bw.write(delim);
                                bw.write(pojo.getCRI2_veg());
                                bw.write(delim);
                                bw.write(pojo.getWBI_veg());
                                bw.write(System.getProperty("line.separator"));
                                bw.flush();
                        }
                }
                catch(IOException ex) {
                        ex.printStackTrace();
                }
                finally {
                        if(bw != null) {
                                try {
                                        bw.close();
                                }
                                catch(IOException e) {}
                                bw = null;
                        }
                }
                
                return csvResultsFile;
        }
        
        private static final char setDelimiter(String delimiter) {
                char delim = '\0';
                switch(delimiter) {
                        case "Comma": {
                                delim = ',';
                                break;
                        }
                        
                        case "Space": {
                                delim = ' ';
                                break;
                        }
                        
                        case "Tab": {
                                delim = '\t';
                                break;
                        }
                        
                        case "Pipe": {
                                delim = '|';
                                break;
                        }
                        
                        case "Semi-Colon": {
                                delim = ';';
                                break;
                        }
                        
                        default: {
                                delim = ',';
                                break;
                        }
                }
                
                return delim;
        }
}
