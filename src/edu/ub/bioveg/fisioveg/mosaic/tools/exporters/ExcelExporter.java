package edu.ub.bioveg.fisioveg.mosaic.tools.exporters;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import edu.ub.bioveg.fisioveg.mosaic.ui.macros.multispectral.ResultPojo;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Dec 12, 2016
 * 
 */
public final class ExcelExporter {

        private ExcelExporter() {
                
        }
        
        public final static File ExportTetracamResults(Map<String,ResultPojo> pojoMap, File saveResultsFile) {
                final String[] headers = {
                                "Filename",
                                "Vegetation_area",
                                "R450",
                                "R550",
                                "R570",
                                "R670",
                                "R700",
                                "R720",
                                "R780",
                                "R780_veg",
                                "R840",
                                "R860",
                                "R900",
                                "R950",
                                "R1000",
                                "R450_veg",
                                "R550_veg",
                                "R570_veg",
                                "R670_veg",
                                "R700_veg",
                                "R720_veg",
                                "R840_veg",
                                "R860_veg",
                                "R900_veg",
                                "R950_veg",
                                "R1000_veg",
                                "NDVI_plot",
                                "PRI_plot",
                                "SAVI_plot",
                                "MCARI_plot",
                                "WBI_plot",
                                "RDVI_plot",
                                "EVI_plot",
                                "ARI2_plot",
                                "CRI2_plot",
                                "TCARI_plot",
                                "OSAVI_plot",
                                "TCARIOSAVI_plot",
                                "NDVI_veg",
                                "PRI_veg",
                                "SAVI_veg",
                                "MCARI_veg",
                                "WBI_veg",
                                "RDVI_veg",
                                "EVI_veg",
                                "ARI2_veg",
                                "CRI2_veg",
                                "TCARI_veg",
                                "OSAVI_veg",
                                "TCARIOSAVI_veg"
                };
                
                File excelResultsFile = null;
                XSSFWorkbook wb = null;
                
                try {
                        excelResultsFile = new File(saveResultsFile.getAbsolutePath() + ".xlsx");
                        wb = new XSSFWorkbook();
                        Sheet sheet = wb.createSheet("Tetracam Macro Results");
                        Row headerRow = sheet.createRow(0);
                        for(int i=0; i < headers.length; i++) {
                                headerRow.createCell(i).setCellValue(headers[i]);
                        }
                        
                        int rowIndex = 0;
                        int colIndex = 0;
                        Iterator<ResultPojo> iter = pojoMap.values().iterator();
                        while(iter.hasNext()) {
                                rowIndex++;
                                colIndex = 0;
                                ResultPojo pojo = iter.next();
                                Row row = sheet.createRow(rowIndex);
                                row.createCell(colIndex++).setCellValue(pojo.getFilename());
                                row.createCell(colIndex++).setCellValue(pojo.getVegetation_area());
                                row.createCell(colIndex++).setCellValue(pojo.getR450());
                                row.createCell(colIndex++).setCellValue(pojo.getR550());
                                row.createCell(colIndex++).setCellValue(pojo.getR570());
                                row.createCell(colIndex++).setCellValue(pojo.getR670());
                                row.createCell(colIndex++).setCellValue(pojo.getR700());
                                row.createCell(colIndex++).setCellValue(pojo.getR720());
                                row.createCell(colIndex++).setCellValue(pojo.getR780());
                                row.createCell(colIndex++).setCellValue(pojo.getR780_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR840());
                                row.createCell(colIndex++).setCellValue(pojo.getR860());
                                row.createCell(colIndex++).setCellValue(pojo.getR900());
                                row.createCell(colIndex++).setCellValue(pojo.getR950());
                                row.createCell(colIndex++).setCellValue(pojo.getR1000());
                                row.createCell(colIndex++).setCellValue(pojo.getR450_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR550_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR570_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR670_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR700_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR720_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR840_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR860_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR900_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR950_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getR1000_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getNDVI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getPRI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getSAVI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getMCARI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getWBI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getRDVI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getEVI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getARI2_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getCRI2_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getTCARI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getOSAVI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getTCARIOSAVI_plot());
                                row.createCell(colIndex++).setCellValue(pojo.getNDVI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getPRI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getSAVI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getMCARI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getWBI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getRDVI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getEVI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getARI2_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getCRI2_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getTCARI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getOSAVI_veg());
                                row.createCell(colIndex++).setCellValue(pojo.getTCARIOSAVI_veg());
                        }
                        
                        FileOutputStream fileOut = new FileOutputStream(excelResultsFile);
                        wb.write(fileOut);
                        fileOut.close();
                }
                catch(Exception ex) {
                        ex.printStackTrace();
                }
                finally {
                        if(wb != null) {
                                try {
                                        wb.close();
                                }
                                catch(IOException e) {}
                                wb = null;
                        }
                }
                
                return excelResultsFile;
        }
}
