package edu.ub.bioveg.fisioveg.mosaic.tools.ui;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Dialog;
import java.awt.FileDialog;
import java.io.File;
import java.io.FilenameFilter;
import javax.swing.JFileChooser;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 20, 2015
 *
 */
public final class FileSave {
        
        private static final boolean IS_MAC = System.getProperty("os.name").indexOf("mac") >= 0;
        
        private FileSave() {

        }
        
        public static final File saveFile(String title, File startDirectory, final String fileDesc) {
                return saveFile(title, startDirectory, fileDesc, null);
        }

        public static final File saveFile(String title, File startDirectory, final String fileDesc, String defaultFileName) {
                File path = null;
                if(IS_MAC) {
                        path = saveFileMac(title, startDirectory, fileDesc, defaultFileName);
                }
                else {
                        path = saveFileOther(title, startDirectory, fileDesc, defaultFileName);
                }
                
                return path;
        }
        
        public static final File saveFileOther(String title, File startDirectory, final String fileDesc, String defaultFileName) {
                JFileChooser chooser = new JFileChooser();
                chooser.setDialogTitle(title);
                
                File path = null;
                
                if(defaultFileName != null) {
                        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                        chooser.setSelectedFile(new File(chooser.getCurrentDirectory().getAbsolutePath()+File.separator+defaultFileName));
                }
                else {
                        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                }

                if (startDirectory != null) {
                        chooser.setCurrentDirectory(startDirectory);
                }
                else {
                        chooser.setCurrentDirectory(new File(System.getProperties().getProperty("user.home")));
                }

                chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
                        @Override
                        public boolean accept(File f) {
                                return f.isDirectory() || f.isFile();
                        }

                        @Override
                        public String getDescription() {
                                return fileDesc;
                        }
                });

                int r = chooser.showSaveDialog(null);

                if (r == JFileChooser.APPROVE_OPTION) {
                        path = new File(chooser.getSelectedFile().getAbsolutePath());
                }
                
                return path;
        }
        
        public static final File saveFileMac(String title, File startDirectory, final String fileDesc, String defaultFileName) {
                File path = null;
                
                System.setProperty("apple.awt.fileDialogForDirectories", "true");
                FileDialog d = new FileDialog((Dialog)null, title, FileDialog.SAVE);
                
                if(defaultFileName != null) {
                        d.setFile(defaultFileName);  
                }
                
                d.setFilenameFilter(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                                return dir.isDirectory() || dir.isFile();
                        }
                });
                
                d.setVisible(true);
                String tmp = d.getFile();
                
                if(tmp != null) {
                        path = new File(tmp);
                }
                
                return path;
        }
}
