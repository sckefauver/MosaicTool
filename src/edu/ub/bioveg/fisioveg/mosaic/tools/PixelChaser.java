package edu.ub.bioveg.fisioveg.mosaic.tools;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import edu.ub.bioveg.fisioveg.mosaic.options.PlotOptions;
import edu.ub.bioveg.fisioveg.mosaic.tools.Corner.Sign;
import ij.gui.PolygonRoi;
import ij.process.ImageProcessor;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 29, 2016
 *
 */
public final class PixelChaser {
        
        private PlotOptions plotOpts = null;
        
        public PixelChaser() {
                
        }
        
        public final void setPlotOptions(PlotOptions plotOpts) {
                this.plotOpts = plotOpts;
        }
        
        public final List<Plot> drawPlots(ImageProcessor ip, PolygonRoi replicatePolygon) throws IndexOutOfBoundsException {
                drawReplicate(ip, replicatePolygon);
                Corners corners = PolygonTool.getCorners2(replicatePolygon.getPolygon());
                List<Plot> plots = null;
                plots = drawPlotsInternal(ip, corners);
                applyBuffers(plots);
                return plots;
        }
        
        private final List<Plot> drawPlotsInternal(ImageProcessor ip, Corners corners) throws IndexOutOfBoundsException {
                List<Plot> plots = new ArrayList<Plot>();
                List<Corner> leftCorners = new ArrayList<Corner>();
                List<Corner> rightCorners = new ArrayList<Corner>();
                
                Corner upperLeft = corners.getUpperLeft();
                Corner lowerLeft = corners.getLowerLeft();
                
                int x = upperLeft.x;
                int y = upperLeft.y;
                int bottom = lowerLeft.y;
                int nextCorner = y;
                int cornerCount = 1;
                
                while(true) {
                        if(isMagenta(ip.get(x, y))) {
                                if(y >= bottom) {
                                        //We reach the bottom of the polygon
                                        if(cornerCount > 1) {
                                                int px = x;
                                                int py = y;
                                                leftCorners.add(new Corner(px, py));
                                                cornerCount = 1;
                                        }
                                        
                                        break;
                                }
                                else if(y == nextCorner) {
                                        int px = x;
                                        int py = y;
                                        leftCorners.add(new Corner(px, py));
                                        if(cornerCount == 1) {
                                                nextCorner = y + (int)plotOpts.getPlotHeight();
                                                cornerCount++;
                                        }
                                        else {
                                                nextCorner = y + (int)plotOpts.getPlotVerticalSpacing();
                                                cornerCount = 1;
                                        }
                                        
                                        y++;
                                }
                                else {
                                      //move 1 pixel down
                                      y++;
                                }
                        }
                        else {
                                if(isMagenta(ip.get(x+1, y))) {
                                        x++;
                                        //go right
                                }
                                else if(isMagenta(ip.get(x-1, y))) {
                                        x--;
                                        //go left
                                }
                                else {
                                        int opt = JOptionPane.showConfirmDialog(null, "Cannot go left or right (1) Continue ?", "Pixel Chaser", JOptionPane.YES_NO_CANCEL_OPTION);
                                        if(JOptionPane.NO_OPTION == opt) {
                                                break;
                                        }
                                }
                        }
                }
                
                //--------------------------------------------------------------------------------
                
                Corner upperRight = corners.getUpperRight();
                Corner lowerRight = corners.getLowerRight();
                
                x = upperRight.x;
                y = upperRight.y;
                bottom = lowerRight.y;
                nextCorner = y;
                cornerCount = 1;
                
                while(true) {
                        if(isMagenta(ip.get(x, y))) {
                                if(y >= bottom) {
                                        //We reach the bottom of the polygon
                                        if(cornerCount > 1) {
                                                int px = x;
                                                int py = y;
                                                rightCorners.add(new Corner(px, py));
                                                cornerCount = 1;
                                        }
                                        
                                        break;
                                }
                                else if(y == nextCorner) {
                                        int px = x;
                                        int py = y;
                                        rightCorners.add(new Corner(px, py));
                                        if(cornerCount == 1) {
                                                nextCorner = y + (int)plotOpts.getPlotHeight();
                                                cornerCount++;
                                        }
                                        else {
                                                nextCorner = y + (int)plotOpts.getPlotVerticalSpacing();
                                                cornerCount = 1;
                                        }
                                        
                                        y++;
                                }
                                else {
                                      //move 1 pixel down
                                      y++;
                                }
                        }
                        else {
                                if(isMagenta(ip.get(x+1, y))) {
                                        x++;
                                        //go right
                                }
                                else if(isMagenta(ip.get(x-1, y))) {
                                        x--;
                                        //go left
                                }
                                else {
                                        int opt = JOptionPane.showConfirmDialog(null, "Cannot go left or right (2) Continue ?", "Pixel Chaser", JOptionPane.YES_NO_CANCEL_OPTION);
                                        if(JOptionPane.NO_OPTION == opt) {
                                                break;
                                        }
                                }
                        }
                }
                
                //--------------------------------------------------------------------------------
                
                //IJ.log("Plot List Sizes = "+leftCorners.size()+" & "+rightCorners.size());
                
                int count = 0;
                Plot plot = null;
                for(int i=0; i < leftCorners.size(); i++) {
                        if(count == 0) {
                                Corner leftCorner = leftCorners.get(i);
                                Corner rightCorner = rightCorners.get(i);
                                plot = new Plot();
                                plot.setUpperLeft(leftCorner);
                                plot.setUpperRight(rightCorner);
                                count++;
                        }
                        else {
                                Corner leftCorner = leftCorners.get(i);
                                Corner rightCorner = rightCorners.get(i);
                                if(plot != null) {
                                        plot.setLowerLeft(leftCorner);
                                        plot.setLowerRight(rightCorner);
                                        plots.add(plot);
                                }
                                count = 0;
                        }
                }
                
                return plots;
        }
        
        private final void applyBuffers(List<Plot> plots) {
                if(plotOpts.hasBuffers()) {
                        int buffX = plotOpts.getBufferX();
                        int buffY = plotOpts.getBufferY();
                        
                        for(int i=0; i < plots.size(); i++) {
                                Plot plot = plots.get(i);
                                plot.getUpperLeft().addXBuffer(buffX, Sign.POSITIVE);
                                plot.getUpperLeft().addYBuffer(buffY, Sign.POSITIVE);
                                
                                plot.getLowerLeft().addXBuffer(buffX, Sign.POSITIVE);
                                plot.getLowerLeft().addYBuffer(buffY, Sign.NEGATIVE);
                                
                                plot.getUpperRight().addXBuffer(buffX, Sign.NEGATIVE);
                                plot.getUpperRight().addYBuffer(buffY, Sign.POSITIVE);
                                
                                plot.getLowerRight().addXBuffer(buffX, Sign.NEGATIVE);
                                plot.getLowerRight().addYBuffer(buffY, Sign.NEGATIVE);
                        }
                }
        }
        
        private static final int MAGENTA = 0xFFFF00FF;
        private final boolean isMagenta(int pixel) {
                //MAGENTA   FF00FF
                //BLACK = FF000000
                return MAGENTA == pixel;
        }
        
        private final void drawReplicate(ImageProcessor ip, PolygonRoi replicatePolygon) {
                ip.setColor(Color.MAGENTA);
                replicatePolygon.drawPixels(ip);  
        }
}
