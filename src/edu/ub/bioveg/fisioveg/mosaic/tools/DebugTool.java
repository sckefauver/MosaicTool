package edu.ub.bioveg.fisioveg.mosaic.tools;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Enumeration;
import javax.swing.tree.DefaultMutableTreeNode;
import ij.IJ;
import ij.gui.PolygonRoi;

public final class DebugTool {

        private DebugTool() {
                
        }
        
        public static final void printTree(DefaultMutableTreeNode rootNode) {
                @SuppressWarnings("unchecked")
                Enumeration<DefaultMutableTreeNode> enu = rootNode.preorderEnumeration();
                while(enu.hasMoreElements()) {
                        DefaultMutableTreeNode node = enu.nextElement();
                        Object obj = node.getUserObject();
                        IJ.log(obj.toString());
                }
        }
        
        public static final void printPolygon(PolygonRoi polyRoi) {
                int[] x = polyRoi.getPolygon().xpoints;
                int[] y = polyRoi.getPolygon().ypoints;
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                sb.append(' ');
                
                for(int a=0; a < x.length; a++) {
                        sb.append('(');
                        sb.append(x[a]);
                        sb.append(',');
                        sb.append(y[a]);
                        sb.append(')');
                        sb.append(' ');
                }
                
                sb.append(']');
                IJ.log(sb.toString());
        }
        
        public static final void printArray(int[] arr) {
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                
                for(int i=0; i < arr.length; i++) {
                        sb.append(arr[i]);
                        sb.append(',');
                }
                
                sb.deleteCharAt(sb.length()-1);
                sb.append(']');
                IJ.log(sb.toString());
        }
        
        public static final void printArray(double[] arr) {
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                
                for(int i=0; i < arr.length; i++) {
                        sb.append(arr[i]);
                        sb.append(',');
                }
                
                sb.deleteCharAt(sb.length()-1);
                sb.append(']');
                IJ.log(sb.toString());
        }
        
        public static final void printArray(float[] arr) {
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                
                for(int i=0; i < arr.length; i++) {
                        sb.append(arr[i]);
                        sb.append(',');
                }
                
                sb.deleteCharAt(sb.length()-1);
                sb.append(']');
                IJ.log(sb.toString());
        }
        
        public static final void printArray(short[] arr) {
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                
                for(int i=0; i < arr.length; i++) {
                        sb.append(arr[i]);
                        sb.append(',');
                }
                
                sb.deleteCharAt(sb.length()-1);
                sb.append(']');
                IJ.log(sb.toString());
        }
        
        public static final void printArray(long[] arr) {
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                
                for(int i=0; i < arr.length; i++) {
                        sb.append(arr[i]);
                        sb.append(',');
                }
                
                sb.deleteCharAt(sb.length()-1);
                sb.append(']');
                IJ.log(sb.toString());
        }
        
        public static final void printArray(String[] arr) {
                StringBuilder sb = new StringBuilder();
                sb.append('[');
                
                for(int i=0; i < arr.length; i++) {
                        sb.append(arr[i]);
                        sb.append(',');
                }
                
                sb.deleteCharAt(sb.length()-1);
                sb.append(']');
                IJ.log(sb.toString());
        }
}
