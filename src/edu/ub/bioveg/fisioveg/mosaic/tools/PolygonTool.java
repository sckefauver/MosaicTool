package edu.ub.bioveg.fisioveg.mosaic.tools;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.Arrays;
import edu.ub.bioveg.fisioveg.mosaic.ui.panels.CloneDirection;
import ij.gui.PolygonRoi;
import ij.gui.Roi;

/**
 * 
 * @author George - george.dma@gmail.com
 * Created on: Mar 31, 2016
 *
 */
public final class PolygonTool {

        private PolygonTool() {
                
        }
        
        public static final PolygonRoi clonePolygonRoi(PolygonRoi polyRoi, int offset, CloneDirection direction) {
                Polygon polygon = polyRoi.getPolygon();
                
                int[] xPoints = polygon.xpoints;
                int[] yPoints = polygon.ypoints;
                
                int[] _xPoints = new int[xPoints.length];
                int[] _yPoints = Arrays.copyOf(yPoints, yPoints.length);
                
                //Get distance between upper left and upper right corners
                int xDistance = xPoints[3] - xPoints[0];
                int yDistance = _yPoints[3] - yPoints[0];
                
                //Apply the distance and specified offset to all the X points
                
                for(int i=0; i < xPoints.length; i++) {
                        switch(direction) {
                                case RIGHT: {
                                        _xPoints[i] = xPoints[i] + xDistance + offset;
                                        break;
                                }
                        
                                case LEFT: {
                                        _xPoints[i] = xPoints[i] - xDistance - offset;
                                        break;
                                }
                                
                                default: {
                                        _xPoints[i] = xPoints[i] + xDistance + offset;
                                        break;
                                }
                        }
                }
                
                //Apply the distance only to the Y points
                
                for(int i=0; i < _yPoints.length; i++) {
                        switch(direction) {
                                case RIGHT: {
                                        _yPoints[i] = _yPoints[i] + yDistance;
                                        break;
                                }
                                
                                case LEFT: {
                                        _yPoints[i] = _yPoints[i] - yDistance;
                                        break;
                                }
                                
                                default: {
                                        _yPoints[i] = _yPoints[i] + yDistance;
                                        break;
                                }
                        }
                }
                
                PolygonRoi polyRoiClone = new PolygonRoi(new Polygon(_xPoints, _yPoints, 4), Roi.POLYGON);
                return polyRoiClone;
        }
        
        public static final Corners getCorners2(Polygon polygon) {
                int[] xpoints = polygon.xpoints;
                int[] ypoints = polygon.ypoints;
                if(4 == polygon.npoints) {
                        Corners corners = new Corners();
                        corners.setUpperLeft(new Corner(xpoints[0], ypoints[0]));
                        corners.setLowerLeft(new Corner(xpoints[1], ypoints[1]));
                        corners.setLowerRight(new Corner(xpoints[2], ypoints[2]));
                        corners.setUpperRight(new Corner(xpoints[3], ypoints[3]));
                        return corners;
                }
                
                return null;
        }
        
        /** 
         * Given a polygon, it will return the 4 corners as {@link Point}s.
         * <p>
         * We are assuming that the polygon is basically a rectangle
         * with many sides. We do not know the coordinates of the corners
         * so this method will find them and sort them for us.
         * 
         * @param polygon - with arbitrary number of sides and 4 unknown corner coordinates
         * @return {@link Corners}
         */
        public static final Corners getCorners(Polygon polygon) {
                ArrayList<double[]> areaPoints = new ArrayList<double[]>();
                ArrayList<Line2D.Double> areaSegments = new ArrayList<Line2D.Double>();
                double[] coords = new double[6];
                
                for (PathIterator pi = polygon.getPathIterator(null); !pi.isDone(); pi.next()) {
                    int type = pi.currentSegment(coords);
                    double[] pathIteratorCoords = {type, coords[0], coords[1]};
                    areaPoints.add(pathIteratorCoords);
                }
                
                double[] start = new double[3];

                for (int i = 0; i < areaPoints.size(); i++) {
                    double[] currentElement = areaPoints.get(i);
                    double[] nextElement = {-1, -1, -1};
                    
                    if (i < areaPoints.size() - 1) {
                        nextElement = areaPoints.get(i + 1);
                    }

                    if (currentElement[0] == PathIterator.SEG_MOVETO) {
                        start = currentElement;
                    } 

                    if (nextElement[0] == PathIterator.SEG_LINETO) {
                        areaSegments.add(new Line2D.Double(currentElement[1], currentElement[2],nextElement[1], nextElement[2]));
                    }
                    else if (nextElement[0] == PathIterator.SEG_CLOSE) {
                        areaSegments.add(new Line2D.Double(currentElement[1], currentElement[2],start[1], start[2]));
                    }
                }
                
                Corner[] _corners = new Corner[4];
                int cor = 0;
                
                int j = 0;
                int k = j+1;
                for(int i=0; i < polygon.npoints; i++) {
                        Line2D.Double line1 = areaSegments.get(j);
                        Line2D.Double line2 = areaSegments.get(k);
                        
                        double angle = getAngle(line1, line2);
                        if((angle > 79 && angle < 101) || (angle > 259 && angle < 281)) {
                                _corners[cor] = new Corner(line1.x1, line1.y1);
                                cor++;
                        }
                        
                        j++;
                        if(j == areaSegments.size()-1) {
                                k = 0;
                        }
                        else {
                                k = j+1;   
                        }
                }
                
                /*
                 * Sort corners in the order of
                 * 1. Upper Left
                 * 2. Lower Left
                 * 3. Upper Right
                 * 4. Lower Right
                 */
                Arrays.sort(_corners, new CornerComparator());
                
                Corners corners = new Corners();
                corners.setUpperLeft(_corners[0]);
                corners.setLowerLeft(_corners[1]);
                corners.setUpperRight(_corners[2]);
                corners.setLowerRight(_corners[3]);
                
                return corners;
        }
        
        /**
         * Returns the angle in degrees between 2 lines
         * 
         * @param line1 - the first line
         * @param line2 - the second line
         * @return the angle in degrees
         */
        public static final double getAngle(Line2D line1, Line2D line2) {
                double angle1 = Math.atan2(line1.getY1() - line1.getY2(), line1.getX1() - line1.getX2());
                double angle2 = Math.atan2(line2.getY1() - line2.getY2(), line2.getX1() - line2.getX2());
                return Math.toDegrees(Math.abs(angle1-angle2));
        }
        
        public static final double getAngleRotation(Polygon replicateRoi) throws NullPointerException {
                if(replicateRoi == null) {
                        throw new NullPointerException("replicateRoi cannot be null");
                }
                
                return getAngleRotation(getCorners2(replicateRoi));
        }
        
        public static final double getAngleRotation(Corners replicateCorners) throws NullPointerException {
                if(replicateCorners == null) {
                        throw new NullPointerException("replicateCorners cannot be null");
                }
                
                Corner upperLeft = replicateCorners.getUpperLeft();
                Corner lowerLeft = replicateCorners.getLowerLeft();
                Corner upperRight = replicateCorners.getUpperRight();
                Corner lowerRight = replicateCorners.getLowerRight();
                
                double a1 = getAngle(new Line2D.Double(upperLeft.x, upperLeft.y, lowerLeft.x, lowerLeft.y),
                                     new Line2D.Double(lowerLeft.x, lowerLeft.y, lowerLeft.x-10, lowerLeft.y));
                
                double a2 = getAngle(new Line2D.Double(lowerRight.x, lowerRight.y, upperRight.x, upperRight.y),
                                     new Line2D.Double(upperRight.x, upperRight.y, upperRight.x+10, upperRight.y));
                
                double angle = 90 - (a1+a2)/2;
                
//                System.out.println("Angle a1 = "+a1);
//                System.out.println("Angle a2 = "+a2);
//                System.out.println("Calc rotation = "+angle);
                
                if(angle < 0) {
                        return Math.abs(angle);
                }
                
                return angle*-1;
        }
        
//        public static void main(String ...args) {
////                upperLeft = 2384,432
////                lowerLeft = 1008,5224
////                lowerRight = 1696,5440
////                upperRight = 3104,600
//                  angle of rotation should be 15 or 16
//                
//                Corners corners = new Corners();
//                corners.setUpperLeft(new Corner(2384,432));
//                corners.setLowerLeft(new Corner(1008,5224));
//                corners.setLowerRight(new Corner(1696,5440));
//                corners.setUpperRight(new Corner(3104,600));
//                
//                PolygonTool.getAngleRotation(corners);
//        }
}
