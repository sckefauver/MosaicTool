package edu.ub.bioveg.fisioveg.mosaic.tools;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Apr 10, 2016
 *
 */
public class Corner {

        public int x = -1;
        public int y = -1;

        public enum Sign {
                NEGATIVE,
                POSITIVE
        }

        public Corner(double x, double y) {
                this.x = (int)x;
                this.y = (int)y;
        }

        public Corner(int x, int y) {
                this.x = x;
                this.y = y;
        }

        public void addXBuffer(int pixels, Sign sign) {
                if(Sign.NEGATIVE.equals(sign)) {
                        x = x - pixels;
                }
                else {
                        x = x + pixels;
                }
        }

        public void addYBuffer(int pixels, Sign sign) {
                if(Sign.NEGATIVE.equals(sign)) {
                        y = y - pixels;
                }
                else {
                        y = y + pixels;
                }
        }

        public static Corner parse(String xyStr) throws NumberFormatException {
                if(xyStr != null) {
                        if(!xyStr.isEmpty()) {
                                int x = Integer.parseInt(xyStr.substring(0, xyStr.indexOf(',')));
                                int y = Integer.parseInt(xyStr.substring(xyStr.indexOf(',') + 1));
                                return new Corner(x, y);
                        }
                        
                        throw new NumberFormatException("String is empty");
                }
                
                throw new NullPointerException("String is null");
        }
        
        public static Corner valueOf(int x, int y) {
                return new Corner(x, y);
        }

        @Override
        public String toString() {
                return x + "," + y;
        }
}
