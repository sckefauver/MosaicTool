package edu.ub.bioveg.fisioveg.mosaic.tools;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 31, 2016
 *
 */
public class Corners {

        private Corner upperLeft = null;
        private Corner lowerLeft = null;
        private Corner upperRight = null;
        private Corner lowerRight = null;
        
        public Corners() {
                
        }

        public final void setUpperLeft(Corner upperLeft) {
                this.upperLeft = upperLeft;
        }

        public final void setLowerLeft(Corner lowerLeft) {
                this.lowerLeft = lowerLeft;
        }

        public final void setUpperRight(Corner upperRight) {
                this.upperRight = upperRight;
        }

        public final void setLowerRight(Corner lowerRight) {
                this.lowerRight = lowerRight;
        }

        public final Corner getUpperLeft() {
                return upperLeft;
        }

        public final Corner getLowerLeft() {
                return lowerLeft;
        }

        public final Corner getUpperRight() {
                return upperRight;
        }

        public final Corner getLowerRight() {
                return lowerRight;
        }
        
        @Override
        public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("Corners [");
                sb.append(upperLeft.toString());
                sb.append(' ');
                sb.append(lowerLeft.toString());
                sb.append(' ');
                sb.append(upperRight.toString());
                sb.append(' ');
                sb.append(lowerRight.toString());
                sb.append(']');
                return sb.toString();
        }
}
