package edu.ub.bioveg.fisioveg.mosaic.tools;

/*
 * MosaicTool
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Comparator;

/**
 * 
 * @author George - george.dma@gmail.com
 * <br>
 * Created on: Mar 31, 2016
 *
 */
public class CornerComparator implements Comparator<Corner> {

        @Override
        public int compare(Corner o1, Corner o2) {
                if(o1.x > o2.x) {
                        return 1;
                }
                else if(o1.x < o2.x) {
                        return -1;
                }
                else {
                        if(o1.y > o2.y) {
                                return 1;
                        }
                        else if(o1.y < o2.y) {
                                return -1;
                        }
                        else {
                                return 0;
                        }
                }
        }
}
