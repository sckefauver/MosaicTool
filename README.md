# MosaicTool

## Already Have FIJI Installed ?

Already having [FIJI](http://fiji.sc/) installed is great and will lower the time spent getting started.

Go directly to the __Install MosaicTool Plugin__ section

## Install Java, FIJI and Plugin

This section is for those users who need to install everything in order to get started.

## Install Java

The very first step that needs to be done is to make sure that the latest version of Java is installed. Follow the steps below to install the latest version of Java.

* Download & Install Java [Download Here](https://www.java.com/en/download/)
* Make sure it was installed correctly
  * Visit Oracle's [Verify Java Version Page](https://www.java.com/en/download/installed.jsp)
  * If your browser blocks the Java web-plugin visit the Oracle's [Manual Java Check Page](https://java.com/en/download/help/version_manual.xml)

Go to __Install Fiji__ section
  
## Install Fiji

Now that the latest version of Java is installed we can proceed with installing [FIJI](http://fiji.sc/).

1. Download the latest version of FIJI [Here](http://fiji.sc/#download)
   * Make sure to download the "No JRE" version

## Install MosaicTool Plugin

TODO

### Verify Integrity of the downloaded files (optional)

Provided are 3 hash codes to check the integrity of the file *Mosaic_Tool.jar*

* md5: xxx
* sha1: xxx
* sha256: xxx

An online check tool can be used like [Online MD5](http://onlinemd5.com/)

# Authors

- Dr. Shawn Kefauver
   - Project Principal Investigator, [University of Barcelona](http://www.ub.edu/)
- Adrian Gracia-Romero, [University of Barcelona](http://www.ub.edu/)
- Prof. José Luis Araus, [University of Barcelona](http://www.ub.edu/)
- George El-Haddad
   - Software Engineer, [Postlight](https://postlight.com)

# Organizations

- Administered by [University of Barcelona](http://www.ub.edu/) / [Dept. B.E.E.C.A.](https://www.ub.edu/portal/web/dp-beeca/fisiologia-vegeta) / [Integrative Crop Ecophysiology Group](https://integrativecropecophysiology.com/)
- Funded in part by Spanish MINECO Project AGL2016-76527-R and Juan de la Cierva IJCI-2014-20595

# Library Dependencies

The MosaicTool has the following dependencies external to Fiji

- Apache Poi [https://poi.apache.org/](https://poi.apache.org/)
   - Licensed under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
- TableLayout [http://www.oracle.com/technetwork/java/tablelayout-141489.html](http://www.oracle.com/technetwork/java/tablelayout-141489.html)
   -  Public Domain
   -  Author: [Daniel Barbalace](dbarbalace@clearthought.info)

# License

Copyright 2018 Shawn Carlisle Kefauver

Licensed under the General Public License version 3.0

- [http://www.gnu.org/licenses/gpl-3.0.en.html](http://www.gnu.org/licenses/gpl-3.0.en.html)
- [https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))
